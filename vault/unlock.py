#!/usr/bin/python
###############################################################################
# Name: unlock.py
# Desc:
#   This script unseals a hashicorp vault instance. 
#   By default, any needed unseal keys and root tokens are obtained 
#   from /etc/vault/corphashicorp_vault_keys.json, but this file may
#   be overridden using the '-keyfile=' option.
#
# Inputs:
#     1.  path to keyfile (containing unseal keys and root token if needed)
#
# Logic:
#     1. Invoke "vault status" to learn state of vault.
#        if rc=0 (vault is already running and unsealed) - nothing to do, exit 0)
#        if rc=1 (vault is not running) try to restart it - invoke restart routine
#        if rc=2 (vault is running, but sealed), invoke unseal routine...
#
#     2. Restart routine
#        Run "systemctl start vault"
#        Run "systemctl status vault"
#        if rc is 3, the service is not started.  If cannot start, exit.
#        
#     3. Unseal vault
#        a. Grab keys from keyfile. 
#           basic verifications...
#           
#        b. Invoke "vault status", and parse it's output for
#           the "Theshold" and "Total Shares" values...
#            ---------- example ---------
#           [root@chu3l9ap417 vault]# vault status
#           Key                Value
#           ---                -----
#           Seal Type          shamir
#           Initialized        true
#           Sealed             true
#           Total Shares       3
#           Threshold          2
#           Unseal Progress    0/2
#           Unseal Nonce       n/a
#           Version            1.2.0
#           HA Enabled         true
#            ---------- example ---------
#
#           If the number of keys found in step a is less than the
#            "Threshold" value, exit with error: 
#               Error: Not enough keys available to unseal the vault.
#           Else if the number of keys found in step a is less than "Total Shares",
#             emit a warning, "WARN: missing keys, but enough to unseal."
#             continue to next step
#           Else...keys found match Total Shares, continue...
#           If the count is at least "Threshold", but less than "Total Shares",
#           Warning: Missing keys, but enough to unseal.
#           else, continue to next step...      
#
#        b. for each key found, Invoke "vault operator unseal", using pexpect
#           Instruct pexpect to provide the key when it sees the following prompt...
#           [root@chu3l9ap417 vault]# vault operator unseal
#           Unseal Key (will be hidden):
#           <key typed here>
#
#           if rc=0 (from vault operator unseal), key was accepted.
#           else
#              key was bad, unseal failed.
#              Emit warning, keep going...
#
#        c.  Invoke 'vault status' again
#            if vault unsealed ok (verified by rc=0 or other indicators in output)
#               emit message "Vault unlocked successfully", exit rc=0
#            else
#               emit message "Error cannot unlock.", exit rc=1
#            endif
#
#        DONE
#
# Change History:
#     11/22/2019 - P. Rupp - Initial Implementation
#
##############################################################################

import os
import sys
import time
import argparse
import pexpect
import platform
import socket
import string
import subprocess

PGM=os.path.basename(sys.argv[0])
HOSTNAME=socket.gethostname()

class results():
   """
   The 'results' class is used to instantiate an object representing return 
   results from the shell function.  An object instaniated from
   this class stores instance variabales representing the command 
   executed (self.cmd), the return code (self.rc), and the stderr/stdout
   streams captured as a string (self.stderr, self.stdout).
   """

   def  __init__(self,rc=0,cmd=None,stdout=None,stderr=None):
      self.rc=rc
      self.cmd=cmd
      self.stdout=stdout
      self.stderr=stderr
   def __repr__(self):
      r (self.rc, self.cmd, self.stdout, self.stderr)

   def __str__(self):
      work=(str(self.rc),str(self.cmd),str(self.stdout),str(self.stderr))
      return "%s"%(work)

   def getrc(self):
      return self.rc

   def setrc(self,rc):
      self.rc=rc

   def getcmd(self):
      return self.cmd

   def setcmd(self,cmd):
      self.cmd=cmd

   def getstdout(self):
      return self.stdout

   def setstdout(self,stdout):
      self.stdout=stdout

   def getstderr(self):
      return self.stderr

   def setstderr(self,stderr):
      self.stderr=stderr

def shell(cmd):
   """
   The shell function is a convenient wrapper to execute (bash) 
   commands, and collect the return code and output in an easy way.
   The command returns a 'results' object.
   """
   rob=results()
   rob.setcmd(cmd)
   proc=subprocess.Popen(cmd,
                  shell=True,
                  stderr=subprocess.PIPE,
                  stdout=subprocess.PIPE,
                  universal_newlines=True)
   o=proc.stdout.read() 
   rob.setstdout(o)
   e=proc.stderr.read() 
   rob.setstderr(e)
   rc=proc.wait() 
   rob.setrc(rc)

   return rob 


def getparms(args=sys.argv):
  d="""
This program unseals a hashicorp (tm) Vault instance.  It takes one 
option, which is the path to a file containing unseal keys (and a root token).
If a file is not provided, it defaults to /etc/vault/hashicorp_vault_keys.json. 
"""
  p=argparse.ArgumentParser(description=d)
  p.add_argument('-keyfile',nargs=1,required=False,help="/path/to/key-file",metavar="<path>",dest="keyfile")
  #
  # create "c" as a "Namespace" Object to hold input variables
  # i.e, "class c():

  c=p.parse_args(args[1:])

  if (c.keyfile  == None ):
     c.keyfile=["/etc/vault/hashicorp_vault_keys.json"] 
  elif (not os.path.isfile(c.keyfile[0])):
     p.error("-keyfile is not a file or missing.")
  
  return c

def log(tag,msg):

   TIMESTAMP=time.strftime("%Y.%m.%d.%H:%M:%S", time.localtime())
   msg=msg.strip()
   tag=tag.strip()
   if (tag == ""): 
      tag="INFO"
    
   # where fac should be "INFO", "WARN", "FAIL", or "ABND"
   if (msg.strip() == ''): 
      print "%s %s [%s] %s"%(TIMESTAMP, PGM, tag, "")
   else:
      for i in (msg.splitlines()):
          print "%s %s [%s] %s"%(TIMESTAMP, PGM, tag, i)

def unseal(key):
   
  log("","Unsealing key %s"%key)
  cmd="vault operator unseal"
  proc=pexpect.spawn(cmd) 
  proc.expect('Unseal Key',timeout=1)
  proc.sendline(key)
  proc.expect(pexpect.EOF)
  proc.close
  rob=shell("vault status")
  log("","# vault status")
  log("",rob.stdout)
  log("",rob.stderr)
  log("","rc=%s"%str(rob.rc))
  log("","")

def getkeys(keyfile):
  """
    getkeys extracts the Vault unseal keys from a root-protected
    key file, and returns them as a (python) list. 

    Looks for any non-commented fields containing
    the value 'Unseal Key'...
     e.g,
      Unseal Key 1: +io098jkla0yF4DtZchVOstPWscvZXZHKNmYk5Ld/t0K
      Unseal Key 2: j99ajajjaljZqZsq7k3n1BShxt5G5SD7lSw0y/+6CVwo
      Unseal Key 3: 6TMPJcarpdogcatfwVj1Y2wWCwZVXMbTaPLjF5WTZjB8
   
    Some basic checks are:
      - if no keys were found (key count=0) Exit w/error.
      - if keys found were less than the Vault Theshold ? Exit w/error
      - if keys found are less than the Vault "Total Shares"? 
              Warning (missing keys, but continue)

      (Vault "Threshold" and "Total Shares" values are obtained 
       from the Vault command, as follows:
         ---------- example ---------
         [root@chu3l9ap417 vault]# vault status
         Key                Value
         ---                -----
         Seal Type          shamir
         Initialized        true
         Sealed             true
         Total Shares       3
         Threshold          2
         Unseal Progress    0/2
         Unseal Nonce       n/a
         Version            1.2.0
         HA Enabled         true
          ---------- example ---------

    Note Well - in the future this routine can obtain the keys
    from other secure sources.
  """

  try:
    # Get vault 'Total Shares' value
    rob=shell("vault status | grep -i 'Total' | awk '{print $3}'")
    total_shares=int(rob.getstdout().lstrip().rstrip())

    # Get vault 'Threshold' value
    rob=shell("vault status | grep -i 'Thres' | awk '{print $2}'")
    threshold=int(rob.getstdout().lstrip().rstrip())

  except Exception as e:
    print("Exception: in getkeys(%s)"%(e))
    log("ERROR","Cannot extract vault 'Total Shares' or 'Threshold' values!")
    sys.exit(1)
  
  keys=[]
  cmd="grep 'Unseal Key' "+str(keyfile)+"|egrep -v '^[[:space:]]*\#'|awk '{print $4}'"
  rob=shell(cmd)
  for i in (rob.stdout.splitlines()):
     keys.append(i)

  log("INFO","Verifying Unseal Keys")
  log("INFO","Vault 'Total Shares' found:  %s"%total_shares)
  log("INFO","Vault 'Threshold' found:     %s"%threshold)
  log("INFO","Vault unseal keys found:     %s"%len(keys))
   
  if (len(keys)==0):
     log("ERROR","No unseal keys found. Check keyfile %s, exiting."%keyfile)
     sys.exit(1)

  if (len(keys) < threshold):
     log("ERROR","Number of unseal keys found is less than threshold Check keyfile %s, exiting."%keyfile)
     sys.exit(1)
     
  if (len(keys) < total_shares):
     log("WARN","Number of unseal keys found is less than Total Shares  Check keyfile %s"%keyfile)

  return keys

def main(c):

  log("","Starting %s"%PGM)
  log("","")

  rc=shell("vault status").getrc()
  if ( rc == 0 ):
     log("INFO","Vault is already unsealed.  Nothing to do, exiting.")
     sys.exit(0)

  if ( rc == 1 ):
     log("ERROR","Vault server is not running. Please restart and rerun this program.")
     sys.exit(1)

  keys=getkeys(c.keyfile[0])
  for i in keys:
     time.sleep(2)
     unseal(i)

if (__name__=="__main__"):
   try:
      c=getparms()
      main(c)
   except KeyboardInterrupt, e:
      log("INFO","Aborted...")
      sys.exit(1)

