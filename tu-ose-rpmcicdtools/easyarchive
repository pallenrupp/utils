#!/usr/bin/bash
#
# Archives a source-code directory (a linux directory, or a git repo)
# as a source to the rpmbuild process. A "standard" rpmbuild practice is
# to provide the source distribution as a compressed tar archive, where
# the name, and it's parent directory are named after a simple product
# name, or product-version name.  Likewise, the archive also should
# include a parent directory of the same name.  This script builds
# such an archive automatically for you.
#
# At a minimum, a source directory is required, but options are available
# to specify a destination directory (to place the archive), and the type
# of archive needed. (Reasonable defaults are provided for the latter).
# Also, the user may provide a "prefix", which serves a dual purpose: first
# it adds a parent directory of the same name within the archive, and second
# is used to name the archive. 
#
# Logic:
#    The script sets default values for dest (/tmp), archive type (tgz),
#    name 
#
# Usage:
#   <pgm>    [-s |--source]         </path/to/source> 
#            [-f |--archive-file]   <archive file>
#            [-p |--archive-prefix] <parent directory>
#            [-f |--archive-format] <tar|*tgz*|tbz|cpio|cgz|cbz>
#            [-c |--commit]         <branch|tag|COMMITID|>
#            [-v |--verbose]
#            [-h |--help ]
#
# Notes:  
#            --source is path to a directory or disk-based git repo
#            --archive-file is the archive file you want to create
#            --commit is a commit point within the git repo
#                     and may be specified as a branch, tag, or
#                     commit id (hex); Is ignored if not a git repo
#            --archive-prefix is a directory path you want prepended to
#                     your source directory within the archive.  
#            --format Is the type of archive file, such as "tar", "tgz"
#                     (gzipped tar file).
#
# Return Codes:
#
#    0 All parameters are sane and archive file is written successfully 
#    1 Error, causing exit 
#

#set -o nounset
PGM=$(basename $0)

usage() {

  title="Usage: $PGM"
  indent="${#title}"

  printf "%-${indent}s %s\n" "$title" " -s |--archive-source  </path/to/source/directory>"
  printf "%-${indent}s %s\n" "    " " -o |--archive-out       </path/to/archive-file>"
  printf "%-${indent}s %s\n" "    " "[-p |--archive-prefix    <./path/ inserted in archive>]"
  printf "%-${indent}s %s\n" "    " "[-t |--archive-type      <tar|*tgz*|tbz|cpio|cpgz|cbz>]"
  printf "%-${indent}s %s\n" "    " "[-c |--git-commit        <git commit id>]"
  printf "%-${indent}s %s\n" "    " "[-v |-vv | -vvv]         debug/verbosity level"
  printf "%-${indent}s %s\n" "    " "[-h |--help]"
  printf "Defaults\n" 
  printf " --git-commit:      master\n"
  printf " --archive-prefix:  None\n" 
  printf " --archive-format:  tgz\n"
  printf "\n"
}

#<trap handling code>
 
# The following code is used to cleanup debris created by this
# program, and to exit with the correct exit status of any
# command or function at the time of exit.  Certain signals
# can also terminate a script, (refered to here as "abends")
# and these always return a 129 exit code. 
 
set -e
set -o nounset

trap cleanup EXIT
trap abend   SIGHUP SIGTERM SIGABRT SIGKILL SIGQUIT SIGINT 

abend() {
   #if abend called if an unusual situation (not normal exit)
   #exit with a failed error code. POSIX says anything > 128
   #Usually, 128+signal_no
   echo "abend code initiated..." >&2
   echo "LINENO=$LINENO, BASH_COMMAND=$BASH_COMMAND"
   exit 129
}

declare -a clean_list[0]="echo 'cleaning up...'"

cleanup() {
   exit_status=$?
   echo "cleanup code initiated..." 2>&2
   echo "LINENO=$LINENO, BASH_COMMAND=$BASH_COMMAND"

   for i in "${clean_list[@]}"
   do
      echo "Executing-->$i"
      eval $i
   done
   exit "$exit_status"
}

addclean() {
  local n=${#clean_list[*]}
  clean_list[$n]="$*"
}


#
#</trap handling code>
#

msg() {
   echo "$@"
   return 0
}
msginfo() {
  msg "$PGM [INFO] $@"
  return 0
}
msgwarn() {
  echo "$PGM [WARN] $@" >&2
  return 0
}
msgerr() {
  echo "$PGM [ERROR] $@" >&2
  return 0
}

msgbug() {
  #
  # Used to selectively print debug messages, whereby optionally,
  # the first argument is a number that represents the minimum
  # debug-level in order to print.  If the first argument is a number
  # it is assumed to specify which debug level to print.  If the
  # first argument is not a number (NaN), the message will print
  # if the 'debug_level' is greater than 0 (zero).  Setting the
  # debug-level to zero disables the function.
  # Example:  
  #  debug 3 "print this when debug_level is 3 or greater"
  #     
  #     debug 1 "print this when debug_level is 1 or greater"
  #
  # DEBUG_LEVEL=${DEBUG_LEVEL:-0}
  #
  if [[ ${DEBUG_LEVEL} -gt 0 ]]; then
     if [[ $1 =~ ^[-+]?[0-9]*\.?[0-9]+$ ]]; then
        # $1 is a number
        if [[ $1 -le ${DEBUG_LEVEL} ]]; then
           shift
           echo "$PGM [DEBUG] $@" >&2
        fi
     else
        echo "$PGM [DEBUG] $@" >&2
     fi
  fi
}

die() {
  local rc=0
  if [ $# -eq 0 ]; then
     rc=0
  elif [ $# -eq 1 ]; then
     rc=$1
  else
     rc=$1
     shift
     echo "PGM [FATAL] $@" 
  fi
   
  exit $rc
}



isopt() {
  # isopt is used by getparms to identify an argument as
  # a known option. Returns 0 (true) if argument $1 is
  # a valid option, or 1 (false) if not. 
  local rc=0
  case "$1" in
    -s|--source) ;;
    -d|--dest) ;;
    -c|--git-commit) ;;
    -n|--archive-name) ;;
    -p|--archive-prefix) ;;
    -f|--archive-format) ;;
    -v|--verbose) ;;
    -h|--help) ;;
    *)
      rc=1 ;;
  esac
  return $rc
}

getParms() { 

  #
  # Set default values 
  #
  ARCHIVE_SOURCE=""
  DEST=""
  ARCHIVE_OUT=""
  ARCHIVE_PREFIX=${ARCHIVE_PREFIX:-""}
  ARCHIVE_TYPE=${ARCHIVE_TYPE:-"tgz"}
  GIT_COMMIT=${GIT_COMMIT:-"master"}
  DEBUG_LEVEL=0

  rc=0

  if [ $# -eq 0 ]; then
     usage
     exit 0
  fi

  while [ "$#" -gt 0 ]; do
    case "$1" in
      -s|--archive-source)
        if [ $# -lt 2 ]; then
           msgerr "$1 missing argument"; rc=$((rc+1))
           shift 1
        else
           if isopt $2; then
              msgerr "$1 missing argument"; rc=$((rc+1))
           else
              ARCHIVE_SOURCE="$2"
              shift 2
           fi
        fi
        ;;
      --archive-source=*) ARCHIVE_SOURCE="${1#*=}"; shift 1;;


      -o|--archive-out)
        if [ $# -lt 2 ]; then
           msgerr "$1 missing argument";usage;exit 1
        else
           if isopt $2; then
              msgerr "$1 missing argument";usage;exit 1
           else
              ARCHIVE_OUT="$2"
              shift 2
           fi
        fi
        ;;
      --archive-out=*) ARCHIVE_OUT="${1#*=}"; shift 1;;


      -p|--archive-prefix)
        if [ $# -lt 2 ]; then
           msgerr "$1 missing argument";usage;exit 1
           shift 1
        else
           if isopt $2; then
              msgerr "$1 missing argument";usage;exit 1
           else
              ARCHIVE_PREFIX="$2"
              shift 2
           fi
        fi
        ;;
      --archive-prefix=*) ARCHIVE_PREFIX="${1#*=}"; shift 1;;


      -t|--archive-type)
        if [ $# -lt 2 ]; then
           msgerr "$1 missing argument";usage;exit 1
           shift 1
        else
           if isopt $2; then
              msgerr "$1 missing argument";usage;exit 1
           else
              ARCHIVE_TYPE="$2"
              shift 2
           fi
        fi
        ;;
      --archive-type=*) ARCHIVE_TYPE="${1#*=}"; shift 1;;

      -c|--git-commit)
        if [ $# -lt 2 ]; then
           msgerr "$1 missing argument";usage;exit 1
           shift 1
        else
           if isopt $2; then
              msgerr "$1 missing argument";usage;exit 1
           else
              GIT_COMMIT="$2"
              shift 2
           fi
        fi
        ;;
      --git-commit=*) GIT_COMMIT="${1#*=}"; shift 1;;

      -v)
        DEBUG_LEVEL=1
        shift
        ;;

      -vv)
        DEBUG_LEVEL=2
        shift
        ;;

      -vvv)
        DEBUG_LEVEL=3
        shift
        ;;

      -h|--help) usage;exit 0
        ;;
      -*|--*) 
         msgerr "$1 unknown option: $1";usage;exit 1
         ;;
      *)  
         msgerr "$1 unknown argument: $1";usage;exit 1
         ;;
    esac

  done

  # REQUIRED PARMS and ARGS 

  if [ -z $ARCHIVE_SOURCE ]; then
     usage
     msgerr "--source missing,  required."; usage;exit 1
  elif [ ! -d $ARCHIVE_SOURCE ]; then
     msgerr "$ARCHIVE_SOURCE is not a directory, exiting..."; usage; exit 1
  fi

  # Resolve source to exact directory
  ARCHIVE_SOURCE=$(readlink -f $ARCHIVE_SOURCE)

  if [ -z $ARCHIVE_OUT ]; then
     msgerr "-f|--archive-file is missing, required.";usage;exit 1
  elif [ -d $ARCHIVE_OUT ]; then
     msgerr "$ARCHIVE_OUT cannot be a directory, exiting..."; usage; exit 1
  fi

  #
  # Make sure our --format options are valid
  #
  HAYSTACK="tar tgz tbz cpio cgz cbz"
  HAYSTACK=${HAYSTACK,,}  # Lower case
  match=0
  for needle in ${HAYSTACK}; do
    needle=${needle,,}
    if [ "${ARCHIVE_TYPE}" == $needle ]; then
       match=$((match+1))
    fi
  done
  if [ $match -eq 0 ]; then
     msgerr "--archive-format ($ARCHIVE_TYPE) is invalid. Valid choices are: $HAYSTACK"
     usage
     exit
  fi
  
  ARCHIVE_OUT=$(realpath $ARCHIVE_OUT)
  ARCHIVE_SOURCE=$(realpath $ARCHIVE_SOURCE)

  # 
  #
  #
  msginfo "Parameters look good...."
  msginfo "#############################################################"
  msginfo "#"
  msginfo "#  $PGM parameters:"
  msginfo "#  -s|--source=$ARCHIVE_SOURCE"
  msginfo "#  -o|--archive-file=$ARCHIVE_OUT"
  msginfo "#  -p|--archive-prefix=$ARCHIVE_PREFIX"
  msginfo "#  -t|--archive-type=$ARCHIVE_TYPE"
  msginfo "#  --git-commit=$GIT_COMMIT"
  msginfo "#  -v|-vv|-vvv=$DEBUG_LEVEL"
  msginfo "#  "
  msginfo "#############################################################"

  return $rc 

}

archive_git() {
   #
   # archive_git is called to handle the extra steps needed
   # to prepare and archive a git repo.  The extra steps needed
   # are to save the original branch state, switch to a requested
   # commit point ("master" is the default), invoke a routine to
   # archive the repo's current working directory, then finally
   # restoring the repo's original branch state.
   #
   # psuedo code:
   #    1. save the repo's original branch state
   #    2. change to the requested commit point (could be a tag,
   #       branch, or commit-ID
   #    3. call function 'archive_dir' to archive the repo's working
   #       director
   #    4. restore the repo's original branch state
   #
   #    Finis
   #

   local rc=0

   if cd ${ARCHIVE_SOURCE}; then
     #
     # Save the COMMIT point
     #
     WORK=$(git branch|grep '^*' | sed 's/[\(\)]//g')
     NEEDLE=$(echo "$WORK" | awk '{print $2}')
     if [[ "${NEEDLE}" == "detached" ]]; then
        GIT_COMMIT_SAVE=$(echo "$WORK" | awk '{print $4}')
     else
        GIT_COMMIT_SAVE="${NEEDLE}"
     fi

     if [ -n "$GIT_COMMIT_SAVE" ]; then
        msginfo "Saved original GIT commit: $GIT_COMMIT_SAVE"
        addclean "(cd $ARCHIVE_SOURCE; git checkout $GIT_COMMIT_SAVE >/dev/null 2>&1)"
        # Checkout requested COMMIT point
        msginfo  "Checking out requested GIT commit: ${GIT_COMMIT}..."
        if cd $ARCHIVE_SOURCE; git checkout $GIT_COMMIT; then
           msginfo "Git checkout of ${GIT_COMMIT} is Successful."
           msginfo "Starting the archive..."
           if archive_dir $ARCHIVE_SOURCE $DEST $ARCHIVE_PREFIX $ARCHIVE_OUT; then
              msginfo "Archive ${ARCHIVE_OUT} completed - successful."
              msginfo  "Restoring original GIT commit: ${GIT_COMMIT_SAVE}..."
              if cd $ARCHIVE_SOURCE;git checkout ${GIT_COMMIT_SAVE}; then
                 msginfo "Git checkout is Successful."
              else
                 msgwarn "Git checkout failed."
              rc=$((rc+1))
              fi
           else
              msgwarn "A problem in archive was detected..."
              rc=$((rc+1))
           fi
        else
           msgerr "Could NOT checkout GIT commit $GIT_COMMIT!! Exiting."
           rc=$((rc+1))
        fi
     else # could not find original GIT branch/commit!
       msgerr "Could not find original GIT branch or commit! Exiting."
       rc=$((rc+1))
     fi
   else
     msgerr "Could not change to source directory $ARCHIVE_SOURCE. Exiting."
     rc=$((rc+1))
   fi

   return $rc
}

archive_dir() {
   # archive_dir does all the work needed to archive a local directory
   # adcording to the global variables $ARCHIVE_SOURCE, $DEST, $ARCHIVE_TYPE,
   # $ARCHIVE_OUT, and $ARCHIVE_PREFIX.    
   # Note well: If the directory is also a git repo, it will blindly archive
   # whatever the current branch and commit points are. If you need 
   # to choose a specific GIT branch, tag or commit point, please use 
   # the 'archive_git' function, which allows you to archive a specific
   # branch or commit  point. A specific branch is selected using the
   # '--git-commit' option and defaults to "master".
   #

   local rc=0

   #
   # Cleanup and simplify the prefix path, by resolving redundant relative directories
   # into their simpler form.  The reason we do this, we must re-create the prefix
   # directory structure in a temporary directory, terminating with a symbolic link
   # to the user's source directory.   If se used the PREFIX raw, and it contained
   # relative components, we could create them outside of our temporary directory! 
   # This routine ensures the PREFIX path is simplified, and made relative so this
   # cannot occur.
   #
   # This is done using the 'realpath -m' command 
   # i/e.
   #  User specified a prefix of ".././dogs/are/green/../cats/eat/dogs"
   #  would be cleaned to --> "./dogs/are/cats/eat" 
   #

   prefix=${ARCHIVE_PREFIX:-default}
   prefix="."$(cd /; realpath -m "$prefix")
   prefix_target=$(echo $prefix| awk -F/ '{print $2}')
   work_dir="/tmp/${PGM}_${USER}$(date '+%Y%m%d%H%M%S%N')"
   prefix_dir=$(dirname ${prefix})
   prefix_link=$(basename ${prefix})
   #
   # from the PREFIX variable, extract the highest level path segment, and use
   # this as the archive target.
   # 
   msgbug "prefix=$prefix"
   msgbug "work_dir=$work_dir"
   msgbug "prefix_dir=$prefix_dir"
   msgbug "prefix_link=$prefix_link"
   msgbug "prefix_target=$prefix_target"
   #
   #  create temp directory --> /tmp/YYYYMMDDSS_easyarchive_<user>/<prefix directory/
   #  create link --> /tmp/YYYYMMDDSS_easyarchive_<user>/prefix_dir/link --> <source dir>
   # 

   CMD="mkdir -p ${work_dir}/${prefix_dir}"
   msgbug "Runnning CMD=$CMD" 
   if eval $CMD; then
      msgbug "Successful. RC=$?" 
      CMD="ln -s ${ARCHIVE_SOURCE} ${work_dir}/${prefix_dir}/${prefix_link}"
      msgbug "Running CMD=$CMD"
      if eval $CMD; then 
          msgbug "Successful. RC=$?"
          #
          # Add temporary directories and link to cleanup-on-exit function
          #
          addclean "(cd ${work_dir}/${prefix_dir}; rm ${prefix_link};set +x)"
          addclean "(rm -rf ${work_dir})"
      else
          die 2 "Could not create temporary work directory ${work_dir}/${prefix_dir}"
      fi
   else 
     die 2 "Could not create temporary work directory ${work_dir}/${prefix_dir}"
   fi
   
   CMD=""
   if  [[ -n $ARCHIVE_PREFIX ]]; then
      case ${ARCHIVE_TYPE} in
      tar)
        CMD="(cd $work_dir; tar --dereference -cvf - $prefix_target > $ARCHIVE_OUT)"
        ;;
      tgz)
        CMD="(cd $work_dir; tar --dereference -cvf - $prefix_target | gzip > $ARCHIVE_OUT)"
        ;;
      tbz)
        CMD="(cd $work_dir; tar --dereference -cvf - $prefix_target | bzip2 > $ARCHIVE_OUT)"
        ;;
      cpio)
        CMD="(cd $work_dir; find -L $prefix_target | cpio -ovc > $ARCHIVE_OUT)"
        ;;
      cgz)
        CMD="(cd $work_dir; find -L $prefix_target | cpio -ovc | gzip >$ARCHIVE_OUT)"
        ;;
      cbz)
        CMD="(cd $work_dir; find -L $prefix_target | cpio -ovc | gzip >$ARCHIVE_OUT)"
        ;;
      esac
   else
     case ${ARCHIVE_TYPE} in
     tar)
        CMD="(cd ${work_dir}/${prefix_target}; tar -cvf - > $ARCHIVE_OUT)"
        ;;
     tgz)
        CMD="(cd ${work_dir}/${prefix_target}; tar -cvf - . | gzip > $ARCHIVE_OUT)"
        ;;
     tbz)
        CMD="(cd ${work_dir}/${prefix_target}; tar -cvf - . | bzip2 > $ARCHIVE_OUT)"
        ;;
     cpio)
        CMD="(cd ${work_dir}/${prefix_target}; find . | cpio -ovc > $ARCHIVE_OUT)"
        ;;
     cgz)
        CMD="(cd ${work_dir}/${prefix_target}; find . | cpio -ovc | gzip > $ARCHIVE_OUT)"
        ;;
      cbz)
         CMD="(cd ${work_dir}/${prefix_target}; find . | cpio -ovc | bzip2 > $ARCHIVE_OUT)"
         ;;
      esac
   fi

   msgbug 1 "ARCHIVE CMD=$CMD"
   msginfo "Creating Archive...."
   msginfo "CMD=$CMD"
   eval $CMD || rc=$((rc+1))
   return $rc 
}


main() {

   addclean msginfo "Program exit on $(date). Cleaning up..." 
   msginfo "$PGM starting on $(date)"   

   local rc=0
   cd $ARCHIVE_SOURCE
   
   if git status > /dev/null 2>&1; then
      msginfo "Source $ARCHIVE_SOURCE is a git repo."
      if archive_git; then
         msginfo "Git repo archived successfully! Done."
      else
         msgerr "Git repo archive FAILED!."
         rc=$((rc+1))
      fi
   else
      archive_dir || rc=$((rc+1))
   fi 
   return $rc 
}

msginfo "$PGM starting on $(date)"
getParms $@ 
main
msginfo "$PGM finished on $(date)"
