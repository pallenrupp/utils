#!/usr/bin/env bash
# 
# Description:
#   buildmyrpm is the driver of TU OSE's standard RPM build process
#   configuration information is kept in a $0.env file and
#   sourced in, and contains variables such as the RPM name, version,
#   git branch, etc.  Next, the product source files are archived and
#   placed into the user's default RPM SOURCES directory.  At this
#   point, rpmbuild is called using the 'default.spec' file located in
#   the packaging directory, then the build process starts.  The RPM's
#   are currently built in the user's RPMS and SRPMS build areas.
#   Finally, the RPM's are extracted and sent to artifactory.
#   
# Basic logic:
#   1. Source configuration file with configuration variables
#   2. Create the user's default RPM build directory structure
#   3. Archive the product source files in the RPM build source directory
#   4. invoke 'rpmbuild' pointing to the default.spec file
#   5. the .rpm files are retrieved and uploaded to artifactory
#
set -o nounset
PGM=$(basename $0) 
CONFIG=${PGM}.env

usage() {
  echo "Usage:"
  echo "$PGM [-c|--config] </path/to/config/file>"
  echo "$PGM [-h|--help]"
  echo ""
}


die() {

  rc=0
  if [ $# -eq 0 ]; then
     rc=0
  elif [ $# -eq 1 ]; then
     rc=$1
  else
     rc=$1
     shift
     echo "$@"
  fi
  exit $rc

}

isopt() {
  # isopt is used by getparms() to identify known options.
  # Returns 0 (true) if a  known argument; false (1) if not.

  rc=0
  case "$1" in
    -c|--config) ;;
   *)
      rc=1 ;;
  esac
  return $rc
}


get_parms() {
  # getparms validates input parameters and arguments according to
  # the usage statement.

  CONFIG=""
  rc=0
  while [ "$#" -gt 0 ]; do
     case "$1" in
      -c|--config)
        if [ $# -lt 2 ]; then
           echo "Error: $1 missing argument";exit 1
           shift 1
        else
           if isopt $2; then
              echo "Error: $1 missing argument";exit 1
           else
              CONFIG="$2"
              shift 2
           fi
        fi
        ;;
        --config=*) 
            CONFIG="${1#*=}"
            shift 1
            ;;

        -h|--help) 
            usage
            exit 0
            ;;
         -*|--*) echo "unknown option: $1" >&2
            usage;
            exit 1
            ;;
         *) shift 1
            ;;
      esac
   done

  if [ -z $CONFIG ]; then
     echo "Error: -c|--config missing, required."
     usage
     exit 1
  elif [ ! -f $CONFIG ]; then
     echo "Error: config file $CONFIG is not a file or is empty." 
     usage
     exit 1
  fi
  if [ $(dirname $CONFIG) == '.' ]; then
     CONFIG="./$CONFIG"
  fi
    
  echo "#############################################################"
  echo "#"
  echo "#  $PGM parameters:"
  echo "#  --config=$CONFIG"
  echo "#  "
  echo "#############################################################"

}


get_config() {
   rc=0

   if [ -s $CONFIG ]; then
      . $CONFIG

      # As of this writing, all config variables must be defined
      # even if null, within the config file

      if [ -v RPM_SOURCE ] && \
         [ -v RPM_NAME ] && \
         [ -v RPM_VERSION ] && \
         [ -v RPM_SPEC ] && \
         [ -v GIT_COMMIT ] ; then

         if [ -z $RPM_SOURCE ] || [ ! -d $RPM_SOURCE ]; then
            die 2 "Error: RPM_SOURCE $RPM_SOURCE is empty or not a directory!"
         fi
         if [ -z $RPM_SOURCE ]; then 
            die 2 "Error: RPM_NAME $RPM_NAME is empty - required!"
         fi
         if [ -z $RPM_VERSION ]; then 
            die 2 "Error: RPM_VERSION $RPM_VERSION is empty - required!"
         fi
         RPM_SPEC=${RPM_SPEC:-"$RPM_SOURCE/packaging/default.spec"}
         GIT_COMMIT=${GIT_COMMIT:-"master"}
   
         echo "Config file $CONFIG has needed parameters, OK."
         echo "<$CONFIG>"
         echo "RPM_SOURCE=$RPM_SOURCE"
         echo "RPM_NAME=$RPM_NAME"
         echo "RPM_VERSION=$RPM_VERSION"
         echo "RPM_SPEC=$RPM_SPEC"
         echo "GIT_COMMIT=$GIT_COMMIT"
         echo "</$CONFIG>"
      else
         rc=$((rc+1))
         echo "Error: Config file $CONFIG is missing required variables!"
         echo "Valid variables are:"
         for i in RPM_SOURCE RPM_NAME RPM_VERSION RPM_SPEC GIT_COMMIT
         do
           echo $i
         done
      fi
   else
      rc=$((rc+1))
      echo "Error: Config file $CONFIG not found!"
   fi

   return $rc
    
}

create_build_tree() {
  # 
  # Create_build_tree does the following... 
  # 1. Queries rpm for the user's default build directories, then
  #    sets the as shell variables of the same name.
  #    (Obtained via 'rpm --showrc | grep topdir | awk '{print $2}')
  #
  #    variables:
  #      _topdir
  #      _builddir
  #      _buildrootdir
  #      _rpmdir
  #      _sourcedir
  #      _specdir
  #      _srcrpmdir
  #
  # 2. Create's the user's default RPM build tree
  #    e.g, mkdir  -p $HOME/rpmbuild/${BUILD,RPMS,SOURCES,SPECS,SRPMS}
  #
  #
  #  Obtained via 'rpm --showrc | grep topdir | awk '{print $2}'
  #
  rc=0

  for i in $(rpm --showrc|grep topdir|awk '{print $2}')
  do
     var="%{$i}"
     value=$(rpm --eval $var)
     set -x
     export $i=$value
     set +x
     if [ -z $i ]; then
        rc=$((rc+1))
        echo "Error: create_build_tree tree variable "$i" is null!  Cannot proceed."
     else
        mkdir -p $value 
     fi
  done

  return $rc

}

create_rpm_source() {
   #
   # description:
   # This routine uses 'getrpmsrc.bash' to extract the product source 
   # into the user's RPM build source directory as found via
   #  $(rpm --eval '%{_sourcedir}'.  The directory (and tar.gz file) are
   # named using the standard format <rpm name>-<rpm version> 
   # If the source directory is GIT, we specify the master
   #
   rc=0
   set -x 
   easyarchive -s ${RPM_SOURCE} \
               -o ${_sourcedir}/${RPM_NAME}-${RPM_VERSION}.tar.gz \
               -p ${RPM_NAME}-${RPM_VERSION} \
               -t tgz \
               -c ${GIT_COMMIT} | tee $PGM.log 
   set +x
   if [ $? -ne 0 ]; then
      rc=$((rc+1))
   fi
   return $rc
} 

build_rpm() {
 
   rc=0

   
   env | grep -i rpm

   if rpmbuild -ba \
      --define "name $RPM_NAME"  \
      --define "version $RPM_VERSION" \
      ${RPM_SPEC}; then
      echo "$PGM: rpmbuild successful!"
   else
      echo "$PGM: Error: rpmbuild failed!"
      rc=$((rc+1))
   fi
   return $rc

}

main() {

   rc=0
   if get_parms $@; then
      if create_build_tree; then
         echo "$PGM: create rpm build tree - SUCCESS."
         if get_config; then
            echo "$PGM: Load the config file - SUCCESS."
            if create_rpm_source; then
               echo "$PGM: create rpm build source - SUCCESS."
               if build_rpm; then
                  echo "$PGM: create rpm - SUCCESS."
               else
                  rc=$((rc+1))
                  die $rc "$PGM: create rpm - FAILED"
               fi
            else
               rc=$((rc+1))
               die $rc "$PGM: create rpm build source - FAILED."
            fi
         else
             rc=$((rc+1))
             die $rc "$PGM: create rpm build tree - FAILED."
         fi
      else  
         rc=$((rc+1))
         die $rc "$PGM: Load the config file - FAILED"
      fi
   else
      rc=$((rc+1))
      die $rc "$PGM: Get input parms - FAILED"
   fi

   return $rc

}

main "$@"
exit $?
