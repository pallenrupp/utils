#!/usr/bin/env python

class results():

   def __str__(self):
      a=str(self.rc)
      b=str(self.msg)
      c=str(self.payload)
      d=(a,b,c)
      d=str(d)
      return "%s"%(d)

   def __repr__(self):
      a=str(self.rc)
      b=str(self.msg)
      c=str(self.payload)
      d=(a,b,c)
      d=str(d)
      return "%s"%(d)

   def  __init__(self,rc=True,msg=None,payload=None):
      self.rc=rc
      self.msg=msg
      self.payload=payload

   def clone(self,other):
      if isinstance(other,results):
         self.rc=other.rc
         self.msg=other.msg
         self.payload=other.payload
      else:
         raise TypeError, "must be results type"

   def get(self):
      return (self.rc, self.msg, self.payload)

   def getrc(self):
      return self.rc

   def setrc(self,rc):
      self.rc=rc

   def getmsg(self):
      return self.msg

   def setmsg(self,msg):
      self.msg=msg

   def getpay(self):
      return self.payload

   def setpay(self,payload):
      self.payload=payload

