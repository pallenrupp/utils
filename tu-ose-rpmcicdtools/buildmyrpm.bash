#!/usr/bin/env bash
#
# Build a RPM, using the user's standard ~/rpmbuild directory
# structure, a path to the distribution's source, and a spec file.
# 
# Things get complicated when the path-to-the-distro-source is a git
# repo.  How do we determine the version, and what branch/tag/ref do
# we use?

# Helpful RPM commands to separate out NVRA components from an RPM name
# See  http://rpm.org/user_doc/query_format.html
#     I
# rpm --querytags
# rpm -qa --queryformat \
#     "\nNAME=%NAME\nVERSION=%{VERSION}\nRELEASE=%{release}\nEPOCH=%{EPOCH}\n" \
#     "*"

PATHTOSOURCE=/u/prupp/git/tu-ose-bootnet
PATHTOSPEC=/u/prupp/git/tu-ose-bootnet/rpm.spec

PGM=$(basename $0)

emsg() {
  echo "ERROR -" $@
}
wmsg() {
  echo "WARN -" $@
}
imsg() {
  echo "INFO -" $@
}

iamroot() { 
  if [[ $(id -un) == "root" ]]; then return 0; else return 1; fi
}
      
doit() {
  # VERIFY INPUTS
  if iamroot; then
     emsg "cannot run builds as the root user, exiting." 
     exit 1
  fi
  if [[ ! -d $PATHTOSOURCE ]]; then
     emsg "PATHTOSOURCE $PATHTOSOURCE does not exist! Exiting."
     exit 1
  fi
  if [[ ! -f $PATHTOSPEC ]]; then
     emsg "PATHTOSPEC $PATHTOSPEC does not exist! Exiting."
     exit 1
  fi 

  imsg "PATHTOSOURCE=$PATHTOSOURCE"
  imsg "PATHTOSPEC=$PATHTOSPEC"

  #
  # Setup User's rpmbuild directories
  #

  imsg "1) Setup the user's rpmbuild environment..."
  set -x
  PATHTORPMTOPDIR=$(rpm --eval '%_topdir')
  RPM_SOURCE_DIR=$(rpm --eval '%_sourcedir')
  mkdir -p $PATHTORPMTOPDIR/{BUILD,RPMS,SOURCES,SPECS,SRPMS}

  #	
  # If git repo, check out master branch, exit if problem.
  #
  imsg "2) is $PATHTOSOURCE a Git repo?"
  cd $PATHTOSOURCE
  if git status > /dev/null 2>&1; then
     imsg "Yes..."
     imsg "Checkout master branch..."
     if git checkout master; then
        imsg "checkout Ok, continuing...."
     else
        imsg "checkout failed, exiting."
        exit 1
     fi
   else
      imsg "No. Not a git repo, continuing..."
   fi

   #
   # The rpm name is extracted from the .spec file
   #

   imsg "3)  extract RPM name from .spec file"
   RPMNAME=$(grep Name $PATHTOSPEC|awk '{print $2}')
   #RPM_SOURCE_DIR=$(rpm --eval '%_sourcedir')
   RPM_SOURCE_DIR=$(rpm --define "_topdir $PATHTORPMTOPDIR" --eval '%_sourcedir')
   MYRPM_SOURCE_DIR=$RPM_SOURCE_DIR/${RPMNAME}
   imsg "RPM_SOURCE_DIR=$RPM_SOURCE_DIR"
   imsg "RPMNAME=$RPMNAME"
   imsg "MYRPM_SOURCE_DIR=$MYRPM_SOURCE_DIR"

   if [[ -z $RPMNAME ]]; then
      emsg "   Failed.  RPM Name not found. Exiting."
      exit 1
   else
      imsg "   Successful. RPMNAME=$RPMNAME."
   fi
 
   if [[ -z $RPM_SOURCE_DIR ]]; then
     emsg "   Failed.  RPM_SOURCE_DIR not found. Exiting."
     exit 1
   else
     imsg "   Successful. RPM_SOURCE_DIR=$RPM_SOURCE_DIR."
   fi



   imsg "PWD=$(pwd)"

   mkdir -p $MYRPM_SOURCE_DIR

   imsg "4) Copy distro source to rpmbuild SOURCES directory $MYRPM_SOURCE_DIR..."
   tar -cvf - . | (cd ${MYRPM_SOURCE_DIR}; tar -xf - )

   imsg "5) Create a tar.gz archive  of the product directory"
   imsg "PWD=$(pwd)"
   SOURCE_ARCHIVE="${RPM_SOURCE_DIR}/${RPMNAME}.tar.gz"
   imsg "   Creating: ${SOURCE_ARCHIVE}"
	
   if cd ${RPM_SOURCE_DIR}; then
      pwd
      rm -f ${SOURCE_ARCHIVE} 
      if tar zcf ${SOURCE_ARCHIVE} ${RPMNAME}; then
         echo "   Successful, continuing..."
         ls -ld ${SOURCE_ARCHIVE} 
      else
         echo "   Failed. Exiting. Exiting."
      fi
   fi
   cd -
   echo "PWD=$(pwd)"
   echo "6) Build the RPM, using the SOURCES archive tar.gz file..."
   set  -x
   pwd
   rpmbuild -ba ./rpm.spec
   set +x
}
doit $@
