#version=RHEL9

timesource --ntp-server=time.ornl.gov
timesource --ntp-server=time-alt.ornl.gov
# System timezone
timezone America/New_York --utc

# Use text install
text

# Use network installation
url --url http://rhn6.ornl.gov/pulp/repos/Oak_Ridge_National_Lab/Library/content/dist/rhel9/9.0/x86_64/baseos/kickstart/

repo --name="AppStream" --baseurl=http://rhn6.ornl.gov/pulp/repos/Oak_Ridge_National_Lab/Library/content/dist/rhel9/9.0/x86_64/appstream/kickstart/

selinux --permissive

# Keyboard layouts
keyboard --vckeymap=us --xlayouts='us'
# System language
lang en_US.UTF-8

# Network information
network  --bootproto=dhcp --device=link --ipv6=auto --activate

# Root password "welcome1"
rootpw --iscrypted $6$rjgoGV7ZhJxpnC.d$jFWMJkteQr8kWQnid0W.AKVTc777/leL6KHdOLxYuiaHg4H3aGfsPNzd.LltOv8ifTgVwetFnnVJKFEf9qOJo1
%addon com_redhat_kdump --enable --reserve-mb='auto'
%end

# Run the Setup Agent on first boot
firstboot --disable

%include /tmp/part-include

# System services
services --enabled="chronyd"

# Intended system purpose
syspurpose --role="Red Hat Enterprise Linux Workstation" --sla="Self-Support"

%packages
@^workstation-product-environment

# We need these for p1/p2 scripts:
wget
net-tools
iproute
bind-utils
perl-interpreter

# The insights-client puts messages in motd telling you how to
# configure it.  Since it sends stuff to Red Hat directly, we don't
# want to encourage its use at this time.
-insights-client
-rhc

%end

%addon com_redhat_kdump --disable --reserve-mb='auto'
%end

%pre
exec < /dev/tty3 > /dev/tty3 2>&1
chvt 3
lsblk
read -t 60 -p "Please enter fully qualified  root drive device (defaut is /dev/sda) " ROOTDRIVE

if [[ "$ROOTDRIVE" == "" ]]; then
   ROOTDRIVE=/dev/sda
fi
echo "ROOTDRIVE=$ROOTDRIVE"
cat << EOF > /tmp/part-include
# Run the Setup Agent on first boot
ignoredisk --only-use=$ROOTDRIVE
# System bootloader configuration
bootloader --append="crashkernel=auto" --location=mbr --boot-drive=$ROOTDRIVE
zerombr
clearpart --drives=$ROOTDRIVE  --all --initlabel
part /boot --fstype="xfs" --ondisk=$ROOTDRIVE --size=1024
part pv.2815 --fstype="lvmpv" --ondisk=$ROOTDRIVE --grow --size=1
volgroup vg00 --pesize=32768  pv.2815
logvol swap --name=swap --vgname=vg00 --size=8000
logvol / --fstype=xfs --name=root --vgname=vg00 --size=40000
logvol /home --fstype="xfs" --size=10240 --name=home --vgname=vg00
EOF
chvt 1
%end

%post  --nochroot --log=/mnt/sysimage/root/ks-post.log
#  IMPORTANT!
#  Mount points during %post installation,
#  RHEL 7.x DVD: /run/install/repo
#  RHEL 7.x OS:  /mnt/sysimage
#  RHEL 6.x DVD: /mnt/source
#  RHEL 6.x OS:  /mnt/sysimage
#

set -x
PATH=$PATH:/sbin:/usr/sbin:/usr/bin:/bin
export PATH

# Add tmpfs entry to fstab
echo "tmpfs /tmp tmpfs defaults,noatime,nosuid,nodev,noexec,mode=1777,size=1024M 0 0" >> /mnt/sysimage/etc/fstab

# Backup /etc
cp -pr /mnt/sysimage/etc /mnt/sysimage/etc.orig

# enable Network Manager (required for RHEL9)
systemctl --root=/mnt/sysimage enable NetworkManager

# Enable root ssh login
echo "PermitRootLogin yes" > /mnt/sysimage/etc/ssh/sshd_config.d/01-permitrootlogin.conf

cat << EOF > /mnt/sysimage/root/rhel9finish-kickstart.sh
#!/bin/bash

# run p1 script
curl -s -S http://160.91.1.117/pub/p1 | bash

# register this RHEL installation with rhn6.ornl.gov
subscription-manager register --force --org="Oak_Ridge_National_Lab" \
        --activationkey="rhel9-ws"

# run p2 script
curl -s -S http://160.91.1.117/pub/p2 | bash

# ensure openssh is installed and running
/usr/bin/yum -y install openssh-server
/bin/systemctl enable sshd.service

# install ORNL specific packages
echo -e "\e[1m\e[31mDownloading and installing ORNL specific packages.\e[21m\e[0m"
/usr/bin/yum -y install wsadduser wspasswd
/usr/bin/yum -y install wscfengine

# configure repositories before big update - this will take a few
# minutes for cfengine to do all of the stuff that needs doing
echo -e "\e[1m\e[31mRunning wscfengine.\e[21m\e[0m"
/bin/rm -f /var/wscfengine3/inputs/cf_md5sum
/etc/cron.hourly/wscfengine
/var/wscfengine3/bin/cf-agent -K
/var/wscfengine3/bin/cf-agent -K

# patch the host
echo -e "\e[1m\e[31mPatching from local ORNL repositories.\e[21m\e[0m"
sleep 10
/usr/bin/yum clean all
/usr/bin/yum -y update
EOF

cat << EOF > /mnt/sysimage/root/get_rdsd_rhel9finish.sh
#!/bin/bash
curl -o rhel9finish.sh http://ipxe.ornl.gov/scripts/rhel9finish.sh
EOF

%end
