# ORNL Kickstart file
#version=RHEL7

# Purpose of this KS it to allow user custom partitioning.
# Unlike Suse, the RH text installer does not support configuring partition layout, so graphical mode
# is required if you wish to manually partition.

# https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/6/html/Installation_Guide/ch-guimode-x86.html#idp45410064

# http://comments.gmane.org/gmane.linux.installation.cobbler/8504

# Further, Cobbler adds 'text' by default as a kernel option. Use tab at the Cobbler boot screen to remove this, else
# text mode will be forced, even if graphical is specified here.
text

# include the partitioning logic from the pre section.
%include /tmp/part-include

%pre
exec < /dev/tty3 > /dev/tty3 2>&1
chvt 3

# pre section
#----- partitioning logic below--------------
# pick the first drive that is not removable and is over MINSIZE
DIR="/sys/block"

# minimum size of hard drive needed specified in GIGABYTES
MINSIZE=10

read -t 10 -p "Please enter root drive: " ROOTDRIVE

if [ -n $ROOTDRIVE ]; then

   ROOTDRIVE=""

   # /sys/block/*/size is in 512 byte chunks

   for DEV in md126 sda sdb sdc sdd hda hdb vda md127; do
     if [ -d $DIR/$DEV ]; then
       REMOVABLE=`cat $DIR/$DEV/removable`
       if [[ $REMOVABLE -eq 0 ]]; then
         echo $DEV
         SIZE=`cat $DIR/$DEV/size`
         GB=$(($SIZE/2**21))
         if [[ $GB -gt $MINSIZE ]]; then
           echo "$(($SIZE/2**21))"
           if [ -z $ROOTDRIVE ]; then
             ROOTDRIVE=$DEV
           fi
         fi
       fi
     fi
   done

fi

echo "ROOTDRIVE=$ROOTDRIVE"

#Disk partitioning information
cat << EOF > /tmp/part-include
zerombr
clearpart --all --drives=$ROOTDRIVE --initlabel

part /boot/efi --fstype=efi --grow --maxsize=200 --size=20 --ondisk=$ROOTDRIVE
part /boot --fstype=xfs --size=512 --ondisk=$ROOTDRIVE --asprimary
part pv.01 --size=1 --grow --ondisk=$ROOTDRIVE --asprimary --fstype=ext4
volgroup VolGroup00 pv.01 
logvol swap --fstype=swap --name=swapvol --vgname=VolGroup00 --recommended
logvol / --fstype=ext4 --name=rootvol --vgname=VolGroup00 --size=100 --grow


bootloader --location=mbr --driveorder=$ROOTDRIVE --append="crashkernel=auto rhgb quiet"
EOF

chvt 1
%end

install
nfs --server=160.91.1.107 --dir=/redhat/ws7-x86_64

lang en_US.UTF-8
keyboard us

network --device eth0 --bootproto dhcp --onboot yes

rootpw  sup3rf4ntast1c
firewall --service=ssh
authconfig --enableshadow --passalgo=sha512
selinux --permissive
timezone America/New_York --isUtc --ntpservers=time.ornl.gov
firstboot --disable



repo --name="repoman-epel-main" --baseurl=http://repoman.ornl.gov/epel/7/x86_64/

# Installation logging level
logging --level=info

%packages
@core
perl
wget
chrony
fprintd-pam
nfs-utils
%end


%post >/root/ks-post-anaconda.log 2>&1

wget -O- http://160.91.1.107/pub/p1 | bash

rpm -U http://160.91.1.107/pub/rhn-org-trusted-ssl-cert-1.0-2.noarch.rpm
ntpdate -sb time.ornl.gov

perl -p -i.orig -e 's/xmlrpc\.rhn\.redhat\.com/rhn\.ornl\.gov/g;' \
     -e 's/enter.your.server.url.here/rhn\.ornl\.gov/g;' \
     -e 's/RHNS-CA-CERT/RHN-ORG-TRUSTED-SSL-CERT/g' /etc/sysconfig/rhn/up2date

# RHEL 7 is always x86_64
# Note:  this is the ungrouped activation key for RHEL 7 Workstation x86_64
rhnreg_ks --activationkey="1-35956bf96d14fd026c0773c8a70954ba" --force

# RHEL 5 moved the key -- look for it there first
if [ -f /etc/pki/rpm-gpg/RPM-GPG-KEY-redhat-release ]; then
    rpm --import /etc/pki/rpm-gpg/RPM-GPG-KEY-redhat-release
fi

cat <<'PREPO3' > /etc/yum.repos.d/repoman-epel-main.repo

[repoman-epel-main]
name=repoman-epel-main
baseurl=http://repoman.ornl.gov/epel/7/x86_64/
enabled=true
gpgcheck=true
gpgkey=http://repoman.ornl.gov/epel/RPM-GPG-KEY-EPEL-7

PREPO3

cat <<'PREP04' > /etc/yum.repos.d/ornl-int.repo

[ornl-int]
name=ornl-int
baseurl=http://repo-int.ornl.gov/ORNL/centos/7/x86_64/
enabled=true
gpgcheck=true
gpgkey=http://repo-int.ornl.gov/ORNL/ORNL-RPM-GPG-KEY

PREP04

yum update -y

cat << EOF > /etc/openldap/ldap.conf
BASE    dc=xcams,dc=ornl,dc=gov
URI     ldaps://ldapx.ornl.gov
TLS_REQCERT  demand
TLS_CACERTDIR /etc/openldap/cacerts
LDAP_Protocol_Version 3
LDAP_StartTLS On
EOF

yum -y install wsadduser wspasswd wscfengine
sudo wspasswd -a || true

/var/wscfengine3/bin/cf-agent --bootstrap lp-cfsrv01.ornl.gov
/var/wscfengine3/bin/cf-agent -f /var/wscfengine3/inputs/update.cf
/var/wscfengine3/bin/cf-agent -I -K

wget -O- http://160.91.1.107/pub/p2 | bash

exit 0
%end
