#!/usr/bin/env python3 
import argparse
import calendar
import json
import os
import re
import requests
import subprocess
import sys
import time
import smtplib
from os.path import basename
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.utils import COMMASPACE, formatdate

class namespace():
   pass
globals = namespace() 

# the following variables maybe changed by the user to
# specify which Rundeck project & job definitions are included in this
# report 

globals.query_parms = \
    ( \
      ({"p":"Auto_Patch_Linux", "j":"GREY[0-9]"}),
      ({"p":"Auto_Patch_Linux", "j":"BLACK[0-9]"}),
      ({"p":"Auto_Patch_Linux", "j":"GOLD[0-9]"}),
      ({"p":"Auto_Patch_Linux", "j":"RED[0-9]"})
    )
#  Append PGM directory to PATH

dirname = os.path.realpath(os.path.dirname(sys.argv[0]))
os.environ['PATH']=os.environ['PATH']+":"+dirname
globals.PATH  = os.environ['PATH']
globals.PGM = os.path.basename(sys.argv[0])

class results():
   """
   The 'results' class is used to instantiate an object representing return
   results from the shell function.  An object instaniated from
   this class stores instance variabales representing the command
   executed (self.cmd), the return code (self.rc), and the stderr/stdout
   streams captured as a string (self.stderr, self.stdout).
   """

   def  __init__(self,rc=0,cmd=None,stdout=None,stderr=None):
      self.rc=rc
      self.cmd=cmd
      self.stdout=stdout
      self.stderr=stderr
   def __repr__(self):
      r (self.rc, self.cmd, self.stdout, self.stderr)

   def __str__(self):
      work=(str(self.rc),str(self.cmd),str(self.stdout),str(self.stderr))
      return "%s"%(work)

   def getrc(self):
      return self.rc

   def setrc(self,rc):
      self.rc=rc

   def getcmd(self):
      return self.cmd

   def setcmd(self,cmd):
      self.cmd=cmd

   def getstdout(self):
      return self.stdout

   def setstdout(self,stdout):
      self.stdout=stdout

   def getstderr(self):
      return self.stderr

   def setstderr(self,stderr):
      self.stderr=stderr

def shell(cmd):
   """
   The shell function is a convenient wrapper to execute (bash)
   commands, and collect the return code and output in an easy way.
   The command returns a 'results' object.
   """
   rob=results()
   rob.setcmd(cmd)
   proc=subprocess.Popen(cmd,
                         shell=True,
                         stderr=subprocess.PIPE,
                         stdout=subprocess.PIPE,
                         universal_newlines=True)
   o=proc.stdout.read()
   rob.setstdout(o)
   e=proc.stderr.read()
   rob.setstderr(e)
   rc=proc.wait()
   rob.setrc(rc)

   return rob

def log(msg):
    print("%s: %s"%(globals.PGM,msg),file = sys.stderr)
    return 0

def getArgs():
   parser = argparse.ArgumentParser()
   parser.add_argument('-e', required=False, type = str, default = None, help = "optional email addresses, comma separated")
   parser.add_argument('-m', required=True, type = str, default = None, help = "month as MM/YYYY")
   parser.add_argument('-x',  required=False, action='store_true', default = False, help = "extract task output and attach")

   globals.args = parser.parse_args() 
   
   m =  globals.args.m
   if m:
      try:
         s = time.strptime(m,"%m/%Y")
         tm_mlastday = calendar.monthrange(s.tm_year, s.tm_mon)[1]
         globals.args.d = "%d/%d/%d-%d/%d/%d"%(s.tm_mon,s.tm_mday,s.tm_year,s.tm_mon,tm_mlastday,s.tm_year)
      except Exception as e:
         log("Exception in month/date %s (%s). Please specify month in MM/YYYY format.  Exiting."%(m,e))
         sys.exit(1)
        
def report(p, j):
   # report:  function is given a project and jobname search/regex
   # then makes a call to 'rdquery' to get aset of tasks in .json format.
   # The json is parsed and printed.  
   d = globals.args.d # date range of supplied MM/YYYY
   project = ""
   jobname = ""
   taskid = 0
   goodcnt = 0
   goodpct = 0
   goodcnttot = 0
   badcnt = 0
   badpct = 0
   badcnttot = 0
   start_date_unixtime_utc = 0
   start_date_human_local = ""
   workfile = globals.PGM+".json"
 
   # extract rundeck tasks per  project (-p), and job (-j) filters, whithin a date range (-d)
   # and with extended output (-x) into a .json file.  We consume the .json to produce
   # a jreport body, followed by the extended output (as an attachement)

   
   if globals.args.x: # if user requested to attach output
      script = "rm -f %s; rdquery -x -p \"%s\" -j \"%s\" -d \"%s\" -o \"%s\""%(workfile, p, j, globals.args.d, workfile)
   else:
      script = "rm -f %s; rdquery    -p \"%s\" -j \"%s\" -d \"%s\" -o \"%s\""%(workfile, p, j, globals.args.d, workfile)
 
   body = ""
   attachments = [] 
 
   rob = shell(script) 
   if rob.rc == 0:
      try:
         f = open(workfile,'r')
      except Exception as e:
         log("problem loading (json) input file (%s); Valid .json? Exiting."%(workfile))
         sys.exit(1)
      tasklist = json.load(f)
      # sort in ascending timestamp order, so oldest first, latest last.
      tasklist.sort(key = lambda x: x['date-started']['unixtime'])
 
      body += "%s,%s runs between %s\n"%(p,j,d)
 
      if len(tasklist) == 0:
         body += "%s,%s started on: %s\n"%(p,j,"NO DATA FOUND")
      else:
         for i in tasklist:
             tattach = ""
             jobname = i['job']['name']
             project = i['job']['project']
             taskid  = i['id']
             permalink = i['permalink']
             goodlist = i.get('successfulNodes',[])
             badlist = i.get('failedNodes',[])
             goodcnt = len(goodlist)
             badcnt = len(badlist)
             if (goodcnt + badcnt) == 0:
                goodpct = 0.0
                badpct = 0.0
             else:
                goodpct = (goodcnt/(goodcnt+badcnt))*100
                badpct = (badcnt/(goodcnt+badcnt))*100
             goodcnttot += goodcnt
             badcnttot += badcnt
             start_date_unixtime_utc = i['date-started']['unixtime']/1000
             start_date_human_local  = time.ctime(start_date_unixtime_utc)
    
             body += "\n"
             body += "   %s, %s, %s started on: %s\n"%(project, jobname, str(taskid), start_date_human_local)
             body += "   %s\n"%(permalink)
             body += "      Success:   %6d  (%3.1f%%)\n"%(goodcnt,goodpct)
             body += "      Failed:    %6d  (%3.1f%%)\n"%(badcnt,badpct)
             for j in badlist:
                 body += "               %s\n"%(j)

             # Next, get extended output (STDOUT) from task on target host(s)
 
             outdict = i.get('output',{})
             for j in outdict.keys():
                tattach += "%s %s %s"%("-"*30,j,"-"*30)
                tattach += "\n"
                tattach += outdict[j]
                tattach += "\n"
 
             tag = "%s_%s.txt"%(jobname,str(taskid))
             entry = (tag, tattach)
             attachments.append(entry)
          
         goodpcttot = (goodcnttot/(goodcnttot+badcnttot))*100
         badpcttot = (badcnttot/(goodcnttot+badcnttot))*100
         body += "\n"
         body += "   Totals:\n"
         body += "      Success:   %6d  (%3.1f%%)\n"%(goodcnttot, goodpcttot)
         body += "      Failed:    %6d  (%3.1f%%)\n"%(badcnttot, badpcttot)
         body += "\n"

      body += "-"*80+"\n"

   return (body, attachments) 

def create_attachment_files(attachlist):
   # takes a list of two-tuples
   # [{str(filename/tag), str(STDOUT), (,) ...)
   # returns a list of names
   namelist = []
   for i in attachlist:
       name = i[0]
       output = i[1]
       f = open(name,'w')
       f.write(output)
       f.close()
       namelist.append(name)
       print("Creating attachment file %s..."%(name))

   return namelist

def send_mail(send_from, send_to, subject, text, files=None, server="127.0.0.1"):
   msg = MIMEMultipart()
   msg['From'] = send_from
   #msg['To'] = COMMASPACE.join(send_to)
   msg['To'] = send_to
   msg['Date'] = formatdate(localtime=True)
   msg['Subject'] = subject

   msg.attach(MIMEText(text))

   for f in files or []:
       with open(f, "rb") as fil:
           part = MIMEApplication(
               fil.read(),
               Name=basename(f)
           )
       # After the file is closed
       part['Content-Disposition'] = 'attachment; filename="%s"' % basename(f)
       msg.attach(part)

   smtp = smtplib.SMTP(server)
   smtp.sendmail(send_from, send_to, msg.as_string())
   smtp.close()

if __name__ == "__main__":

   getArgs() 

   attachment_file_list = []
   body = ""
   body += "VUMC IT Linux Monthly Patching Report for %s\n"%(globals.args.m)
   body += "Report Date: %s\n"%(time.asctime())
   body += "\n"

   attach_filename_list = []
   for i in globals.query_parms:
      a, b = report(i['p'],i['j'])  
      body += a
      if globals.args.x:
         worklist = create_attachment_files(b)
         attach_filename_list.extend(worklist)

   bodyfile = 'body.txt'
   log("Writing body to file %s"%(bodyfile))
   f = open(bodyfile,'w')
   f.write(body)
   f.close()
   print(body)

   if globals.args.e:
      log("sending email to %s"%(globals.args.e))
      send_from="noreply@vumc.org"
      send_to = globals.args.e
      subject = "VUMC IT Linux Monthly Patching Report for %s\n"%(globals.args.m)
      send_mail(send_from, send_to, subject, body, files=attach_filename_list, server="127.0.0.1")

   log("done on %s"%(time.ctime())) 
