#!/usr/bin/env bash
#
# This script uses a personal UCAM's id to download ARM globus certs 
# via ssh/scp from OLCF's linux systems.
# For instructions on how to obtain a personal OLCF account, please
# see README in this directory.
#


# Change to HOME directory of user
cd $HOME

# backup user's globus x509 and .pem files
cp -pr .globus .globus.$(date  '+%Y%m%d')
cp -pr gridcerts gridcerts.$(date  '+%Y%m%d')

# Remove globus files
rm -f .globus/certificates/*  gridcerts/certificates/*

# get "open" x509 certs...
scp  rupppa@opendtn.ccs.ornl.gov:/etc/grid-security/certificates/*   $HOME/.globus/certificates/. 
 
# get "moderate"  x509 certs ..
scp  rupppa@dtn.ccs.ornl.gov:/etc/grid-security/certificates/*   $HOME/.globus/certificates/.

# get "open" .pem file...
scp rupppa@opendtn.ccs.ornl.gov:/ccs/gridcerts/dm-cli120.pem   $HOME/gridcerts/dm-cli120_open.pem

# get "moderate" .pem file...
scp rupppa@dtn.ccs.ornl.gov:/ccs/gridcerts/dm-cli120.pem  $HOME/gridcerts/dm-cli120_moderate.pem

# Change the ownership to your current user's uname and primary group....
chown -R $USER .globus 
chown -R $USER gridcerts

# DONE!
