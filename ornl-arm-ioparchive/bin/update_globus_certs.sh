#!/usr/bin/env bash

# Examples for /var/tmp/Check_MK-globus-cert-update.status.....
# 2 Globus-Certs-Update fail=1:0:0:: [20211117-100521] Failed to sync .globus/certificates: Failed to get dm-cli120_open.pem cert
# 2 Globus-Certs-Update fail=1:0:0:: [20211118-100514] Failed to sync .globus/certificates: Failed to get dm-cli120_open.pem cert
# 0 Globus-Certs-Update fail=0:0:0:: [20211119-085010] Success
# 0 Globus-Certs-Update fail=0:0:0:: [20211122-100707] Success
# </FILE: /var/tmp/Check_MK-globus-cert-update.status>

STATFILE="/var/tmp/Check_MK-globus-cert-update.status"
touch $STATFILE
chmod 777 $STATFILE

CERTS_DIR=$HOME/.globus/certificates/
PEMS_DIR=$HOME/gridcerts/
mkdir -p $CERTS_DIR 
mkdir -p $PEMS_DIR

rcall=0
rclist=""
msglist=""

#
# Get open certs...
#
set -x
globus-url-copy -sync -sync-level 3 -cred $HOME/gridcerts/dm-cli120_open.pem gsiftp://opengridftp.ccs.ornl.gov/etc/grid-security/certificates/ file:///$CERTS_DIR/
rc=$?; rcall=$((rcall+rc)); rclist="$rclist:$rc"
set +x
if [[ $rc -ne 0 ]]; then msglist="$msglist: Failed to sync open .globus/certificates "; fi


#
# Get moderate certs...
#
set -x
globus-url-copy -sync -sync-level 3 -cred $HOME/gridcerts/dm-cli120_moderate.pem gsiftp://gridftp.ccs.ornl.gov/etc/grid-security/certificates/ file:///$CERTS_DIR/
rc=$?; rcall=$((rcall+rc)); rclist="$rclist:$rc"
set +x
if [[ $rc -ne 0 ]]; then msglist="$msglist: Failed to sync moderate .globus/certificates "; fi


#
# Get open pem...
#
set -x
globus-url-copy -sync -sync-level 3 -cred $HOME/gridcerts/dm-cli120_open.pem gsiftp://opengridftp.ccs.ornl.gov/ccsopen/gridcerts/dm-cli120.pem file:///$PEMS_DIR/dm-cli120_open.pem
rc=$?; rcall=$((rcall+rc)); rclist="$rclist:$rc"
set +x
if [[ $rc -ne 0 ]]; then msglist="$msglist: Failed to get dm-cli120_open.pem"; fi

#
# Get moderate pem
#
set -x
globus-url-copy -sync -sync-level 3 -cred $HOME/gridcerts/dm-cli120_moderate.pem gsiftp://gridftp.ccs.ornl.gov/ccs/gridcerts/dm-cli120.pem  file:///$PEMS_DIR/dm-cli120_moderate.pem
set +x
rc=$?; rcall=$((rcall+rc)); rclist="$rclist:$rc"
if [[ $rc -ne 0 ]]; then msglist="$msglist: Failed to get dm-cli120_moderate.pem"; fi


if [[ $rcall -ne 0 ]]; then
   msg=$msglist
   rc=2
else
   msg="Success"
fi

datestr=$(date "+%Y%m%d-%H%M%S")
echo "$rc Globus-Certs-Update fail=$rclist [${datestr}] ${msg}" | tee  $STATFILE
