#!/usr/bin/env bash

PGM=$(basename $0)

usage() {
   >&2 echo "Usage:"
   >&2 echo "   $PGM [start_anl|start_ornl|stop|status|isdone|watch] -w <directory>"
   >&2 echo ""
}

do_stop() {
   rc=0
   while getopts ":w:" opt
   do
     case $opt in
        w|work) w=$OPTARG
          if [[ ! -d "$w" ]]; then
            >&2 echo "Syntax error - $w is not a directory."
            usage
            rc=5
          fi
          ;;
        ?) >&2 echo "Syntax error - unknown option." 
           usage
           rc=5
           ;;
        *) >&2 echo "Syntax error - invalid option '-$OPTARG'"
           usage
           rc=5
           ;; 
     esac
   done
   set -x
   w=$(readlink -f $w)
   if cd $w; then
      if [[ -s pid ]]; then
         pid=$(cat pid)
         session_pids=$(pgrep -s $pid)
         if [[ "$session_pids" != "" ]]; then
            echo "Session pids found..."
            echo "$session_pids"
            echo "Killing now..." 
            pkill -s $pid
            sleep 5
            echo "Done..."
            rc=0
          else
            echo "No session PIDS found, already down?"
            echo "Try to kill PID $pid by itself..."
            kill $pid
            sleep 5
            kill -9 $pid
            echo "Done."
            rc=0
          fi
       else
           >&2 echo "Error, PID file not found or unknown.  Nothing to do."
           rc=5
       fi
   else
      >&2 echo "Error: could not change to directory $1, exiting."
      rc=5
   fi
   set +x
   return $rc
}

start_ornl() {
   rc=0
   while getopts ":w:" opt
   do
     case $opt in
        w|work) w=$OPTARG
          if [[ ! -d "$w" ]]; then
            >&2 echo "Syntax error - $w is not a directory."
            usage
            rc=5
          fi
          ;;
        ?) >&2 echo "Syntax error - unknown option." 
           usage
           rc=5 
           ;;
        *) >&2 echo "Syntax error - invalid option '-$OPTARG'"
           usage
           rc=5
           ;; 
     esac
   done
   set -x
   w=$(readlink -f $w)
   if cd $w; then
      do_status -w $w > /dev/null
      if [[ rc -eq 0 ]]; then
         >&2 echo "Error, this job is already running.  It must be stopped first. Exiting."
         rc=5
      else
         rm pid rc log
         nohup setsid ioparchive.sh -w . -d ornl >> log 2>&1 &
         echo "Started. PID=$(cat pid)"
         rc=0
      fi 
   else
      >&2 echo "Error: could not change to directory $1, exiting."
      rc=5
   fi
   return $rc
}

start_anl() {
   rc=0
   while getopts ":w:" opt
   do
     case $opt in
        w|work) w=$OPTARG
          if [[ ! -d "$w" ]]; then
            >&2 echo "Syntax error - $w is not a directory."
            usage
            rc=5
          fi
          ;;
        ?) >&2 echo "Syntax error - unknown option." 
           usage
           rc=5 
           ;;
        *) >&2 echo "Syntax error - invalid option '-$OPTARG'"
           usage
           rc=5
           ;; 
     esac
   done
   set -x
   w=$(readlink -f $w)
   if cd $w; then
      do_status -w $w > /dev/null
      if [[ rc -eq 0 ]]; then
         >&2 echo "Error, this job is already running.  It must be stopped first. Exiting."
         rc=5
      else
         rm pid rc
         nohup setsid ioparchive.sh -w . -d anl >> log 2>&1 &
         echo "Started. PID=$(cat pid)"
         rc=0
      fi 
   else
      >&2 echo "Error: could not change to directory $1, exiting."
      rc=5
   fi
   return $rc
}

do_isdone() {
   # rc=0 if UP
   # rc=1 if DOWN
   # rc >=5  if other error
   rc=0
   while getopts ":w:" opt
   do
     case $opt in
        w|work) w=$OPTARG
          if [[ ! -d "$w" ]]; then
            >&2 echo "Syntax error - $w is not a directory."
            usage
            rc=5
          fi
          ;;
        ?) >&2 echo "Syntax error - unknown option." 
           usage
           rc=5
           ;;
        *) >&2 echo "Syntax error - invalid option '-$OPTARG'"
           usage
           rc=5
           ;; 
     esac
   done
   w=$(readlink -f $w)
   rc=1
   if cd $w; then
      if [[ -s ql  && -s qp ]]; then
         ql=$(cat ql)
         qp=$(cat qp)
         if [[ $qp -lt $ql ]]; then
            echo "job is in progress" 
         else
            echo "job is complete" 
            rc=0
         fi
      else
         echo "WARN: cannot determine state,  missing ql and/or qp files."
      fi
   fi
   return $rc
}

do_status() {
   # rc=0 if UP
   # rc=1 if DOWN
   # rc >=5  if other error
   rc=0
   while getopts ":w:" opt
   do
     case $opt in
        w|work) w=$OPTARG
          if [[ ! -d "$w" ]]; then
            >&2 echo "Syntax error - $w is not a directory."
            usage
            rc=5
          fi
          ;;
        ?) >&2 echo "Syntax error - unknown option." 
           usage
           rc=5
           ;;
        *) >&2 echo "Syntax error - invalid option '-$OPTARG'"
           usage
           rc=5
           ;; 
     esac
   done
   w=$(readlink -f $w)
   if cd $w; then
      if [[ -s pid ]]; then
          pid=$(cat pid)
          session_pids=$(pgrep -s $pid)
          if [[ "$session_pids" != "" ]]; then
             echo "$pid UP"
             rc=0 
          else
             echo "$pid DOWN"
             rc=1
          fi
      else
          >&2 echo "Missing pid file, cannot determine status."
          rc=5
       fi
   else
      >&2  echo "Error: could not change to directory $1, exiting."
      rc=5 
   fi
   return $rc
}
     
do_watch() {
   while getopts ":w:" opt
   do
     case $opt in
        w|work) w=$OPTARG
          if [[ ! -d "$w" ]]; then
            >&2 echo "Syntax error - $w is not a directory."
            usage
            rc=1
          fi
          ;;
        ?) >&2 echo "Syntax error - unknown option." 
           usage
           rc=1
           ;;
        *) >&2 echo "Syntax error - invalid option '-$OPTARG'"
           usage
           rc=1
           ;; 
     esac
   done
   set -x
   w=$(readlink -f $w)

   if cd $w; then
cat > $w/watch <<EOF
echo -n 'good count: '; wc -l $w/good 
echo -n 'bad  count: '; wc -l $w/bad
echo -n 'ql        : '; cat $w/ql
echo -n 'qp        : '; cat $w/qp
echo -n 'GBytes    : '; showgig $w/dusk
echo -n 'pid       : '; cat $w/pid
echo -n 'pstree    : '; pstree -asp $(cat $w/pid) | head -20
EOF
      watch -n 6 "bash $w/watch"
   else
      >&2 echo "Error: could not change to directory $1, exiting."
      rc=1
   fi
   set +x
}
     
   
rc=0
case "$1" in
   start_anl)
     shift
     start_anl $@
     ;;
   start_ornl)
     shift
     start_ornl $@
     ;;
   stop)
     shift
     do_stop $@
     ;;
   status)
     shift
     do_status $@
     ;;
   iscomplete|isfinished|isdone)
     shift
     do_isdone $@
     ;;
   watch)
     shift
     do_watch $@
     ;;
   *) >&2 echo "Syntax error - invalid option."
      usage
      exit 1
      ;;
esac 
exit $rc
