#!/usr/bin/env bash

PGM=$(basename $0)
PGM=${FUNCNAME[0]}
pid=$$

usage() {
  echo "Usage:"
  echo "  $PGM -w <work-directory> -d [anl|ornl]"
  echo ""
}

get_lock() {
   exec 200>lock
   flock 200 
}
free_lock() {
   flock -u 200 
   200>&-
}

log() {
  (
  while IFS= read -r line; do
     printf '%s %s\n' "$(date): $PGM/$TASK_ID $line";
  done
  )<<EOF
$@
EOF
  return 0
}

doit() {
   PGM=${FUNCNAME[0]}
   TASK_ID=$1 # where $1 is parallel's task  number {%} - by convention
   TASK_DATA=$2 # where $3 is data supplied to sub-process, {} - by convention 

   touch lock good bad skip
   rc=0
   log "Pushing dest=$dest: $TASK_DATA"

   if echo $TASK_DATA | egrep -q '^[/.]*iop\/'; then
      # input  data begins with "iop" - A-ok 
      case "$dest" in
         anl)   
            cmd="globus-url-copy -cd -cred $HOME/gridcerts/dm-cli120_moderate.pem"
            cmd="$cmd file:///data/$TASK_DATA"
            cmd="$cmd gsiftp://miradtn12.alcf.anl.gov:2813/projects/DOE_ARM_Archive/$TASK_DATA"
            log "cmd=$cmd"
            ;;
         ornl)
            # ensure the file's  directory is crated.... 
            dirname=$(dirname /f1/arm/$TASK_DATA)
            cmd="echo 'mkdir -p $dirname; cput /data/$TASK_DATA : /f1/arm/$TASK_DATA'" 
            cmd="$cmd | /usr/local/bin/hsi -l alcfmgr -A combo -q -P -A keytab -k /home/alcfmgr/etc/arm.johnny.keytab -l arm"
            log "cmd=$cmd"
            ;;
      esac
      size_now=$(du -sb /data/${TASK_DATA} | cut -f1)
      out=$(eval $cmd 2>&1)
      rc=$?
      log "rc=$rc, out=$out"
      get_lock
      if [[ $rc -eq 0 ]]; then
         log "good rc=$rc"
         log "out=$out"
         echo $TASK_DATA >> good
         size_before=$(cat dusk)
         size_after=$((size_before + size_now))
         echo $size_after > dusk
      else 
         log "bad rc=$rc"
         log "out=$out"
         echo $TASK_DATA >> bad 
      fi
      free_lock
   else
     get_lock
     log "Error: bad input format! Skipping.  $TASK_DATA"
     echo $TASK_DATA >> skip 
     free_lock
     rc=1
   fi 
   exit $rc 
}

export -f usage doit get_lock free_lock log
#
# Verify parms
#
workdir=""
rc=0
while getopts ":w:d:" opt
do
   case $opt in
      w) workdir=$OPTARG
         workdir=$(readlink -f $workdir)
         ;;
      d) dest=$OPTARG
         ;;
      ?) echo "Syntax error - invalid option '-$OPTARG'"
         rc=1
         usage
         exit 1 
         ;;
    esac
done

if [[ "$workdir" == "" ]]; then
   echo "Syntax error:  -w $workdir' is required."
   rc=1 
fi

if [[ ! -d $workdir ]]; then
   log "Syntax error - '-w $workdir' is not a valid directory."
   err=$((err+1))
fi
if [[ "$dest" != "anl" && "$dest" != "ornl" ]]; then
   log "Syntax error - '-d $dest' must be 'anl' or 'ornl'"
   err=$((err+1))
fi

if [[ $rc -gt 0 ]]; then
   usage
   exit 1
fi

export workdir
export dest 

if cd $workdir; then
   if [[ -f pid ]]; then
      jobpid=$(cat pid)
      if [[ $jobpid != "" ]]; then
         if ps -p $jobpid >/dev/null 2>&1; then
            log "Error: $pgm (pid $pid) already running, exiting."
            exit 1
         fi
      fi
   fi
   if [[ -f dusk && -s dusk ]]; then
      :
   else
      rm dusk >/dev/null 2>&1
      echo "0" > dusk
   fi
   if [[ -f rc ]]; then
      jobrc=$(cat rc) 
      if [[ $jobrc != ""  ]]; then
        log "Error: $pgm already completed $rc=$jobrc, exiting."
        exit 1
      fi
   fi
   echo $pid > pid
   > rc
   j=100
   if [[ ! -f qp ]]; then
      echo 0 > qp 
   fi
   if [[ ! -f ql ]]; then
      wc -l master | awk '{print $1}' > ql
   fi 
  
   ql=$(cat ql) # get total length of queue
   qp=$(cat qp) # get current queue pointer
   qp=$((qp-$j)) # rewind queue pointer by j
   # if queue pointer (qp) goes negatrive, make it zero)
   qp=$((qp>0 ? qp : 0))
   n=$((ql-qp))
   log "##############################################################################"
   log "INFO: STARTING AT QP=$qp QL=$ql"
   log "##############################################################################"
   if [[ $qp -lt $ql ]]; then
       tail -$n master |  linecounter qp | parallel  -j $j doit {%} {}
       echo $? > $workdir/rc
   else
       log "INFO: All Done! The queue pointer ($qp) has reached the end of the queue ($ql)."
       log "INFO: Done."
   fi
else
   log "Error: Cannot change to directory $workdir. Exiting."
   rc =5
fi

exit $rc
