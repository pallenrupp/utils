#!/usr/bin/env python3
###############################################################################
# Name: checknic
# Desc: Display's OS NIC, and any teaming/bonding/vlan information in a
#       succinct, and easy-to-view way.
# Notes:
#   1. determine if a NIC is a container type (vlan, bond, bond_slave, team or
#      team_slave?
#      $ ip link show type [vlan|bond|bond_slave|team|team_slave]
#      
#      $ ip link show master team0
#        3: enp0s8: <BROADCAST,MULTICAST,PROMISC,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast master team0 state UP mode DEFAULT group default qlen 1000
#        link/ether 08:00:27:4f:51:a2 brd ff:ff:ff:ff:ff:ff
#        4: enp0s9: <BROADCAST,MULTICAST,PROMISC,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast master team0 state UP mode DEFAULT group default qlen 1000
#        link/ether 08:00:27:4f:51:a2 brd ff:ff:ff:ff:ff:ff

#      $ ip link show master team0 
#        ip link show type bond_slave | grep master
#        3: enp0s8: <BROADCAST,MULTICAST,PROMISC,SLAVE,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast master bond0 state UP mode DEFAULT group default qlen 1000
#        4: enp0s9: <BROADCAST,MULTICAST,PROMISC,SLAVE,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast master bond0 state UP mode DEFAULT group default qlen 1000

#   2. Bonding notes...
#      a.  /proc/net/bonding/bond<N>.  The only way single-source way
#          to determine all bonding details. 
#      b. /sys/class/net/bonding_masters <-- current active bond interfaces
#         (add or remove by 'echo "+<name>" or "-<name>" >> bonding_masters)
#      c. /sys/class/net/bond<N>/bonding/* various config files that can be
#                                          modified to change characteristics
#      The only easy way to get all bond details is the file
#      /proc/net/bonding/bond<N> file.  Outside of this, you need to parse
#      /etc/sysconfig/network-scripts/ifcfg* for
#                         "BONDING_OPTS" or "ETHTOOL_OPTS"
#      d.  Can get original MAC addresses
#          by parsing /proc/net/bonding/<bondN> grep for "Permanent HW addr"
#          or...
#          # ethtool -P <nic>
#  3.  Teaming notes.
#      a.  Get teaming information by parsing output of 
#          # teamdctl <teamN> state [dump]
#          "dump" provids json output, which may be easier to parse
#      b.  Get port information by running
#          # teamnl <teamN> ports
#      c.  Can only get original MAC addresses using 
#          # ethtool -P <nic>
#
###############################################################################
   
import argparse
import os
import platform
import pwd 
import re
import re
import socket
import string
import subprocess
import sys
import time

PGM=os.path.basename(sys.argv[0])
USER=pwd.getpwuid(os.getuid()).pw_name

class shell():
   """ 
   shell is a simple class to make it easy to execute a shell command
   and get the output and return-code.
   """

   def  __init__(self,cmd=None,rc=0,stdout="",stderr=""):
      self.rc=rc
      self.cmd=cmd
      self.stdout=stdout
      self.stderr=stderr

   def __repr__(self):
      work="cmd='%s',rc=%s,stdout(%s bytes),stderr(%s bytes)"%(str(self.cmd),str(self.rc),str(len(self.stdout)),str(len(self.stderr)))
      return work
  
   def __str__(self):
      work="cmd='%s',rc=%s,stdout(%s bytes),stderr(%s bytes)"%(str(self.cmd),str(self.rc),str(len(self.stdout)),str(len(self.stderr)))
      return work

   def getrc(self):
      return self.rc

   def getcmd(self):
      return self.cmd

   def setcmd(self,cmd):
      self.cmd=cmd
      self.rc=0
      self.stdout=""
      self.stderr=""
      self.grepout=""

   def getstdout(self):
      return self.stdout

   def getstderr(self):
      return self.stderr

   def grep(self,needle='',ignorecase=False):
      self.grepout=""
      haystack=self.stdout
      p=re.compile(needle)
      if ignorecase:
         p=re.compile(needle,re.IGNORECASE)
      result=[]
      for i in str(haystack).splitlines():
         if p.search(i):
            result.append(i)
         else:
            pass
      results='\n'.join(result)
      self.grepout=results
      return results

   def run(self):
      self.rc=0
      self.stdout=''
      self.stderr=''

      if self.cmd !=None: 
         proc=subprocess.Popen(self.cmd, shell=True, stderr=subprocess.PIPE, stdout=subprocess.PIPE, universal_newlines=True)
         self.stdout=proc.stdout.read()
         self.stderr=proc.stderr.read()
         self.rc=proc.wait()
      return self

class tree:
  """
  a tree used to represent system NIC devices
  primarily used to display hierarchies of nic devices
  such as 'team0.100' --> team0 --> enp0s8/enp0s9
  in a nice way.
  """

  level=0
  def __init__(self):
     self.nodelist={"root":link("root",data=None)}
 
  def add_link(self,name,link):
     self.nodelist['root'].add_child(name)
     self.nodelist[name]=link
     self.nodelist[name].add_parent('root')
        
  def del_link(self,name):
     self.nodelist['root'].del_child(name)
     del self.nodelist[name]
     
  def relate(self,pname,cname):
     rootlink=self.nodelist["root"]
     self.nodelist[pname].add_child(cname)
     self.nodelist[cname].add_parent(pname)
     # Remove child's link from root, because somebody
     # else is now pointing to the child.
     self.nodelist["root"].del_child(cname) 

     #print("after add_parent", pname, cname)
     #print("cname.parents=", self.nodelist[cname].parents)
     #print("cname.children=", self.nodelist[cname].children)
     #print("pname.parents=", self.nodelist[pname].parents)
     #print("pname.children=", self.nodelist[pname].children)
     return self

  def get(self,name):
     # returns a link object if name in the tree
     if self.nodelist.has_key(name):
        return self.nodelist[name]
     else:
        print("Error: tree.get(%s) does not exist."%(name))

  def walk(self,name="root"):
     link=self.nodelist[name]
     nav="|_"
     spaces=' '*2*tree.level
     print(spaces+nav+link.name)
     print("link.data=",link.data)
     print("link.children=",link.children)
     print("link.parents=",link.parents)

     tree.level=tree.level+1
     for child in link.children:
        self.walk(name=child)
     tree.level=tree.level-1

class link:
  """ a link in a linked list, used as elements for the nictree class """
  
  def __init__(self,name=None,data=None):
    self.name=name
    self.data=data
    self.children=[]
    self.parents=[]

  def add_child(self,name):
     if (self.children.count(name) < 1):
        self.children.append(name) 
 
  def add_parent(self,name):
     if (self.parents.count(name) < 1):
        self.parents.append(name) 

  def del_child(self,name):
     while name in self.children: self.children.remove(name)

  def del_parent(self,name):
     while name in self.parents: self.parents.remove(name)

def chunk(stuff,length=60):
   """
   splits a string into an array fixed-sized chunks of
   a given length
   """
   stuff=str(stuff)
   stuff_l=len(stuff)
   i=0
   j=length
   out=[]
   while True:
      if i > stuff_l:
         break
      else:
         out.append(stuff[i:j])
      i+=length
      j+=length
   return out

def parse_ip_entry(stuff):
   """ 
   parses a NIC entry created via 'ip link show' 
   then returns a namespace with each parsed field.
   """ 

   # Initialize returned variables
   nic_name=""
   nic_sname=""
   nic_ip=""
   nic_mask=""
   nic_mac=""
   nic_state=""
   nic_flags=""
   nic_mtu=""
   nic_link=""
   nic_speed=""
   nic_duplex=""
   nic_autoneg=""
   nic_type=""
   nic_data=""
   nic_children=""

   # Get name and short=name 
   nic_name=nic_sname=""
   elist=stuff.split()
   
   try:
     nic_name=elist[1].rstrip(":")
     nic_sname=nic_name.split('@')[0]
   except Exception as e:
      print >> sys.stderr,"Warning: %s nic_name may be blank due to exception: %s"%(nic_sname,e)

   # Get flags
   nic_flags=""
   try:
      work=[e for e in elist if  e.startswith('<') and e.endswith('>')]
      if work:
         nic_flags=work[0]
   except Exception as e:
      print >> sys.stderr,"Warning: %s nic_flags may be blank due to exception: %s"%(nic_sname,e)

   # Get mtu
   nic_mtu=""
   try:
      if elist.count('mtu') > 0:
         index=elist.index('mtu') 
         nic_mtu=elist[index+1]

   except Exception as e:
      print >> sys.stderr,"Warning: %s nic_mtu may be blank due to exception: %s"%(nic_sname,e)

   # Get state  (via IP command)
   nic_state="" 
   try:
      if elist.count('state') > 0:
         index=elist.index('state') 
         nic_state=elist[index+1]
   except Exception as e:
      print >> sys.stderr,"Warning: $s nic_state may be blank due to exception: %s"%(nic_sname,e)
   
   # Get IP address
   nic_ip=""
   try:
      work=[ elist.index(item) for item in elist if item.find(' inet ') >= 0 ]
      if work:
         nic_ip=elist[work[0]+1]
   except Exception as e:
      print >> sys.stderr,"Warning: %s nic_ip may be blank due to exception: %s"%(nic_sname,e)
   
   # Get mac 
   nic_mac=""
   try:
      work=[ elist.index(item) for item in elist if item.find('link/') >= 0 ]
      if work:
         nic_mac=elist[work[0]+1]
   except Exception as e:
      print >> sys.stderr,"Warning: %s nic_mac may be blank due to exception: %s"%(nic_sname,e)
   
   # Get speed, duplex, autoneg, link (via ethtool)
   nic_speed=""
   nic_duplex=""
   nic_link=""
   nic_autoneg=""
   try: 
      o=shell("ethtool %s"%(nic_sname)).run()
      nic_speed=o.grep(needle="Speed:",ignorecase=True).strip().split()[1]
      nic_duplex=o.grep(needle="Duplex:",ignorecase=True).strip().split()[1]
      nic_autoneg=o.grep(needle="\sAuto-negotiation:",ignorecase=False).strip().split()[1]
      nic_link=o.grep(needle="Link detect",ignorecase=True).strip().split()[2]
   except Exception as e:
      print >> sys.stderr,"Warning: %s ethtool fields may be blank due to exception: %s"%(nic_sname,e)
  
   # Get nic type (vlan, team, bond, etc) 
   # First, create the type-to-nic, and nic-to-type lookup tables.
   lookup_by_name,lookup_by_type=get_lookup_tables()

   nic_type=""
   try:
      if nic_name in lookup_by_name:
         nic_type=lookup_by_name[nic_name][0]
   except Exception as e:
      print >> sys.stderr,"Warning: %s nic_type may be blank due to exception: %s"%(nic_sname,e)
   
   # Get link data specific to vlan/bond/link 
   # This means:
   #  1.  bond/team data (IEEE 802.3ad - link aggregation)
   #      and vlan data (IEEE 802.1q - vlan tagging) 
   #  2.  child device names
   #
   nic_data=""
   nic_child=""
   nic_children=[]

   if nic_type == "vlan":
      # NOTE WELL - IEEE 802.1q - vlan tagging....
      # vlan driver live data in /proc/net/vlan/<device>
      # e.g, $ cat /proc/net/vlan/vlan.100
      # team0.100  VID: 100      REORDER_HDR: 1  dev->priv_flags: 1
      # total frames received           0
      # total bytes received            0
      # Broadcast/Multicast Rcvd        0
      # total frames transmitted        11
      # total bytes transmitted         782
      # Device: team0
      # INGRESS priority mappings: 0:0  1:0  2:0  3:0  4:0  5:0  6:0 7:0
      # EGRESS priority mappings:
      #
      nic_vid=""
      nic_tfr=""
      nic_tbr=""
      nic_tft=""
      nic_tbt=""
      o=shell("cat /proc/net/vlan/%s"%(nic_sname)).run()
      wlist=o.grep(needle="VID:",ignorecase=False).strip().split()
      try:
         if wlist.count('VID:') > 0:
            index=wlist.index('VID:') 
            nic_vid=wlist[index+1]

         nic_tfr=o.grep(needle="total frames received",ignorecase=False).strip().split()[3]
         nic_tbr=o.grep(needle="total bytes received",ignorecase=False).strip().split()[3]
         nic_tft=o.grep(needle="total frames trans",ignorecase=False).strip().split()[3]
         nic_tbt=o.grep(needle="total bytes trans",ignorecase=False).strip().split()[3]
         nic_child=o.grep(needle="Device:",ignorecase=False).strip().split()[1]
      except Exception as e:
         print >> sys.stderr,"Warning: %s vlan data may be blank due to exception: %s"%(nic_sname,e)
      
      nic_data="%s,vid=%s,child=%s,tfr=%s,tbr=%s,tft=%s,tbt=%s"%\
      ("IEEE 802.1q vlan", nic_vid,nic_child,nic_tfr,nic_tbr,nic_tft,nic_tbt)
      nic_children.append(nic_child)

   if nic_type == "team":
      # retrieve team-specific attributes using methods below... 
      #   $ teamdctl -v <dev> state view'  # real-time info (may be less than configuration
      #   $ teamdctl -v <dev> config dump' # as configured info (may be different than realtime
      #   (caveat? have to run as 'root')
      # or...query the process table, gives "as configured" info in process args field 
      #   $ ps -o args <PID>
      #   does NOT need root, pid in /var/run/teamd/<teamX>.pid
      nic_data=""
      nic_children=[]
      nic_data=shell("ps -o args $(cat /var/run/teamd/%s.pid)"%(nic_sname)).run().getstdout().strip()

      # get children
      try:
         nic_children=[]
         o=shell("ip link show master %s"%(nic_sname)).run()
         for line in o.grep(needle='^[0-9]+\:').splitlines():
            nic_children.append(line.split()[1].strip(":"))
      except Exception as e:
         print >> sys.stderr,"Warning: %s team children may be blank due to exception: %s"%(nic_sname,e)

   if nic_type == "bond":
      # retrieve real-time bond info from /proc/net/bonding/bond<N> file  
      # very difficult to get "as configured" data...have to parse network start scripts
      # 
      nic_data=""
      o=shell("cat /proc/net/bonding/%s"%(nic_sname)).run()
      bond_driver=o.grep(needle="^Ethernet Channel",ignorecase=True).strip("\n") 
      bond_mode=o.grep(needle="^Bonding Mode",ignorecase=True).strip("\n") 
      bond_slaves=o.grep(needle="^Slave interface",ignorecase=True).strip("\n") 
      nic_data=bond_driver+","+bond_mode+","+bond_slaves
      # get children
      try:
         nic_children=[]
         o=shell("ip link show master %s"%(nic_sname)).run()
         for line in o.grep(needle='^[0-9]+\:').splitlines():
            nic_children.append(line.split()[1].strip(":"))
      except Exception as e:
         print >> sys.stderr,"Warning:  %s bond children may be blank due to exception: %s"%(nic_sname,e)

   mynic=nic()
   mynic.nic_name=nic_name   
   mynic.nic_sname=nic_sname   
   mynic.nic_ip=nic_ip  
   mynic.nic_state=nic_state
   mynic.nic_flags=nic_flags
   mynic.nic_mtu=nic_mtu
   mynic.nic_mac=nic_mac
   mynic.nic_link=nic_link
   mynic.nic_speed=nic_speed
   mynic.nic_duplex=nic_duplex
   mynic.nic_autoneg=nic_autoneg
   mynic.nic_type=nic_type
   mynic.nic_data=nic_data
   mynic.nic_children=nic_children

   return mynic


class nic():
   """" simple class to hold NIC attributes """
   def __init__(self):
      pass

   def __str__(self):
      return str(vars(self))

   def __repr__(self):
      return str(vars(self))

   
def get_lookup_tables():
   # Create nic type lookup tables...
   # lookup_by_name:
   #    is a dict where keys are nic device names,and
   #    values are a list of assigned types belonging to the NIC name.
   # lookup_by_type:
   #    is a dict  where keys are nic types, and values
   #    are a list of NIC device names belonging to the type. 
   #
   """ This program returns two dictionaries, used to lookup
       1.  What types are assigned to a given NIC? 
      2.  What NICs are assigned a given type? 
   """
   # TYPES are extracted from the IP command man page
   TYPES="bridge bond can dummy hsr ifb ipoib macvlan macvtap vcan \
          veth vlan vxlan ip6tnl ipip sit gre gretap ip6gre ip6gretap \
          vti nlmon ipvlan lowpan geneve vrf macsec  bridge_slave \
          bond_slave team bond team_slave"
   #
   lookup_by_name={}
   lookup_by_type={}


   for nic_type in TYPES.split():
      o=shell("ip link show type %s"%(nic_type)).run()
      namelist=[ s.split()[1].strip(":") for s in o.grep(needle='^[0-9]+\:').splitlines() ] 
      lookup_by_type[nic_type]=namelist

      if len(namelist) > 0:
         for nic_name in namelist:
            if nic_name in lookup_by_name:
               tlist=lookup_by_name[nic_name]
            else:
               tlist=[nic_type]
            lookup_by_name[nic_name]=tlist
      else:
         pass
   return (lookup_by_name,lookup_by_type) 

def getNicInfo():

   # Example of an IP entry
   # 3: enp0s8: <BROADCAST,MULTICAST,PROMISC,UP,LOWER_UP> mtu 1500 qdisc \
   #    pfifo_fast master team0 state UP mode DEFAULT group default qlen 1000
   #  link/ether 08:00:27:4f:51:a2 brd ff:ff:ff:ff:ff:ff
 
   nic_tab={}
   olist=shell("ip link show").run().getstdout().splitlines()
   if len(olist) > 0:
      i=0;j=len(olist)
      while i < j:
         mynic=nic()
         ent=olist[i] + olist[i+1]
         nic_inst=parse_ip_entry(ent)
         nic_tab[nic_inst.nic_name]=nic_inst
         i+=2
      #end_while
      i=i+2
   return nic_tab

def showNicInfo(nic_table):
   print(nic_table)
   t=tree()
   for i in nic_table.keys():
      t.add_link(i,link(i,nic_table[i]))

   for i in nic_table.keys():
      for j in nic_table[i].nic_children:
         t.relate(i,j)

   t.walk()
   sys.exit(0)
 
def main():
   PGM=os.path.basename(sys.argv[0])
   HOSTNAME=socket.gethostname()
   USERNAME=pwd.getpwuid(os.getuid()).pw_name 

   nic_table=getNicInfo()
   showNicInfo(nic_table)

if (__name__=="__main__"):
   try:
      main()
   except KeyboardInterrupt as e:
      log("INFO","Aborted...")
      sys.exit(1)
