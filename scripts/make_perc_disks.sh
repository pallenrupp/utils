#!/bin/bash


vgremove appvg

/opt/opensystems/bin/perccli64 /c0/v1 del
/opt/opensystems/bin/perccli64 /c0/v2 del
/opt/opensystems/bin/perccli64 /c0/v3 del

for i in /sys/class/scsi_disk/*; do    echo "- - -" $i/scan; done

/opt/opensystems/bin/perccli64 /c0 add vd type=raid10 drives=32:0-3 pdperarray=2 WB RA
/opt/opensystems/bin/perccli64 /c0 add vd type=raid10 drives=32:4-7 pdperarray=2 WB RA
/opt/opensystems/bin/perccli64 /c0 add vd type=raid10 drives=32:8-11 pdperarray=2 WB RA

echo "Sleeping for a few seconds..."
sleep 10 

for i in /sys/class/scsi_disk/*; do    echo "- - -" $i/scan; done

pvcreate /dev/sdb /dev/sdc /dev/sdd
vgcreate -s32m appvg /dev/sdb /dev/sdc /dev/sdd
