#!/bin/bash
#
#  checknic - tries to determmine the switch/port behind
#           a network interface.  
#
#  Details: This script uses tcpdump to sniff for Cisco Discovery Protocol
#           (CDP) packets, or Link Layer Discovery Protocol (LLDP) packets.
#           to determine information about the link partner.  The script
#           on native, bond, team and vlan interfaces.
#
#
#  

basic() {
  LIVE_NICS=""
  export PATH=$PATH:/bin:/usr/bin:/sbin:/usr/sbin:.

  # Get NIC devices. 
  if [[ $# -gt 0 ]]; then
     NICS=$@
  else
     WORK=$(ip link | grep ^[0-9]*: | awk -F: '{print $2}' | awk -F@ '{print $1}' | egrep -v lo | sort)
  
     NICS="" 
     for i in $WORK
     do
        NICS="$NICS $i"
     done
  fi

  printf "%-10s %-4s %-16s %-25s %-4s %-17s\n" "NIC" "Type" "Related_To" "speed,duplex,neg" "Link" "MAC (Permanent)"
  printf "%-10s %-4s %-16s %-25s %-4s %-17s\n" "---------" "----" "----------------" "-------------------------" "----" "-----------------"
  
  for i in $NICS
  do
    
    # Get current settings of the interface (which may be altered later
    # depending on the type of interface (see below) 
  
    DEPS=""
    TYPE="raw"
    SPEED=$(ethtool $i | grep Speed | awk '{print $2}')
    DUPLEX=$(ethtool $i | grep Duplex | awk '{print $2}')
    AUTONEG=$(ethtool $i | grep Auto- | awk '{print $2}')
    SPECS="$SPEED,$DUPLEX,$AUTONEG"
    LINK=$(ethtool ${i}| grep detected |awk '{print $3}')
      MAC=$(ip addr | awk -v needle=$i '$0 ~ needle {getline;print $2;exit}')

    # Next, we alter the above settings slightly, depending if the interface
    # is a bond, team, or vlan
    # If a bond...
    #    1. We set TYPE to "raw"
    #    2. We any dependent devices, then place into DEPS variable
    # If a vlan...
    #    1. We set TYPE to "vlan"
    #    2. We find any dependent device(s), then place into DEPS variable
    #
    # If part of a bond...
    #    1. We find the real MAC address, then place into the MAC variable
    #    2. We find parent bond name, then place into DEPS variable
    # 
    
    #
    # IS NIC  a vlan?
    #
    WORK=$(ip link show $i |grep '^[0-9]*:' | awk -F: '{print $2}' | awk -F@ '{print $2}')
    if [[ $WORK != "" ]]; then
       TYPE="vlan"
       for j in $WORK; do if [ $DEPS ];then DEPS="$DEPS,$j"; else DEPS=$j;fi;done
    fi  
  
    #
    # Is NIC a bond?
    #
    BONDFILE=/proc/net/bonding/$i
    if [[ -f $BONDFILE ]]; then
       WORK=$(grep 'Slave Inter' $BONDFILE | awk -F: '{print $2}')
       if [[ $WORK != "" ]]; then
          TYPE="bond"
          for j in $WORK; do if [ $DEPS ];then DEPS="$DEPS,$j"; else DEPS=$j;fi;done
       fi 
    fi
     
    # 
    # Is NIC part of a bond?
    # 
    if [[ -d /proc/net/bonding ]]; then
       WORK=$(cd /proc/net/bonding;grep -l $i *| head -1)
       if [ $WORK ]; then
          MAC=$(awk  'BEGIN { RS = ""; FS= "\n" }/Slave Interface/{print  $1, $6}' /proc/net/bonding/*  | awk '{print $3, $7}'| grep $i | awk '{print $2}')
          DEPS=$WORK
       fi
    fi
    
    printf "%-10s %-4s %-16s %-25s %-4s %-17s\n" "$i" "$TYPE" "$DEPS" "$SPECS" "$LINK" "$MAC"
    
    if [[ $LINK == "yes" && $TYPE == "raw" ]]; then
       LIVE_NICS="$LIVE_NICS $i"
    fi
  done
}
  
lldp() {
  
   echo "CDP/LLCP discovery of NICs --> ${LIVE_NICS}..."
   PROCS=""
   NICLIST=$(ip link  | sed 'N;s/\n/ /' | egrep -v "@|bond|lo"|awk -F: '{print $2}')
   LIVE_NICS=""
   for i in $NICLIST
   do
      LINK=$(ethtool ${i}| grep detected |awk '{print $3}')
      if [[ $LINK == "yes" ]]; then
         LIVE_NICS="$LIVE_NICS $i"
      fi
   done
   LIVE_NICS=$(echo $LIVE_NICS|xargs -n1|sort -u|xargs)

   for i in $LIVE_NICS
   do
      # Cisco Discovery Protocol (CDP)
      OUT="/tmp/ethmap_$i.out"
      
      # LLDP #tcpdump -i $i -s  1500 -v -c 1 'ether proto 0x88cc' > $LLCPOUT 2>/dev/null & 
      # CDP  #tcpdump -i $i -v -s 1500 -c 1 'ether[20:2] == ' > $LLCPOUT & 

      tcpdump -i $i -v -s 1500 -c 1 '(ether[20:2] == 0x2000 or proto 0x88cc)' > $OUT 2>/dev/null & 
      PROCS="$PROCS $!" 
   done
   sleep 60 
   kill -SIGINT $PROCS >/dev/null 2>&1

   printf "%-10s %-30s %-20s\n" "NIC" "Switch" "Port"
   printf "%-10s %-30s %-20s\n" "---------" "------------------------------" "--------------------"
   for i in $LIVE_NICS 
   do 
      OUT="/tmp/ethmap_$i.out"
      NIC=$i
      SWITCH=$(awk '$0 ~ "System Name" {print $NF}' $OUT)
      if [[ $SWITCH == "" ]]; then SWITCH="UNKNOWN"; fi
      PORT=$(awk '$0 ~ "Port-ID" {print $NF}' $OUT)
      if [[ $PORT == "" ]]; then PORT="UNKNOWN"; fi
      printf "%-10s %-30s %-20s\n" "$NIC" "$SWITCH" "$PORT"
   done
}

basic
