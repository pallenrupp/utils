from fabric.api import *
import fabric.api 
import exceptions
import getpass
import re
import sys

env.key_filename = "~/.ssh/id_rsa"
#env.password = getpass.getpass()


def nocomments(file):
  out=[]
  try:
     f=open(file,'r')
  except Exception, e:
     print "error: file", file, "cannot be opened.",e
     sys.exit(1)

  line=f.readline()
  while (line != ""): 
     line=line.strip()
     match=re.search("^\b*\#|^$|^\b*$",line)
     if match:
 	pass 
     else:
        print ">",line,"<"
        work=line.split()[0].strip()  # Use only the first word on the line
        out.append(work)
     line=f.readline()
  f.close()
  return out
   

@parallel(pool_size=5)
def hostfile(file='./hosts'):
    env.hosts=nocomments(file)
    print ""
    print "hosts_file=", env.hosts
    print ""

def push_ssh_keys():
    put("/u/prupp/.ssh_clients","/u/prupp/.")
    run("cd /u/prupp; rm -rf .ssh; mv .ssh_clients .ssh;chmod 700 .ssh")
    
def pull_ssh_keys():
    run("if cd /u/prupp; then rm -rf .ssh/authorized_keys;fi")

@parallel(pool_size=5)
def pushtmp(file=""):
  put(file,"/tmp/.")
  run("ls -ld /tmp/"+file)

def push(source="",target=""):
  put(source,target)

def pull(source="",target=""):
  put(source,target)

def run(cmd):
    env.warn_only=True
    fabric.api.run(cmd)

def sudo(cmd):
    env.warn_only=True
    fabric.api.sudo(cmd)
     

@parallel(pool_size=5)
def hello():
  sudo("uname -a")

def fixlink():
    env.warn_only=True
    sudo("cd /opt/java;rm jdk8;ln -s jdk1.8.0_161 jdk8")

def invoke_puppet():
    env.warn_only=True
    sudo("puppet agent --enable;nohup /bin/nohup puppet agent -t &")

def checklink():
    env.warn_only=True
    sudo("cd /opt/java;pwd;ls -ld *")


def ebureau_install():
    env.warn_only=True
    sudo("yum clean all; yum -y -q install rh-nodejs8 rh-nginx18 --enablerepo=*")

def ebureau_remove():
    env.warn_only=True
    sudo("yum clean all; yum -y -q remove rh-nodejs8* rh-nginx* --enablerepo=*")

def ebureau_verify():
    env.warn_only=True
    run("pwd;rpm -qa rh-nodejs* rh-nginx*")
   
def buy_beer():
  put('./beer', './beer')

def set_tsm_password():
  put_file = "/home/namradi/DTS_DELL_ESX/tsm_set_password.sh"
  put(put_file, "/home/namradi/tsm_set_password.sh")
  sudo("bash /home/namradi/tsm_set_password.sh")

def disable_puppet():
  sudo("puppet agent --disable")

@parallel(pool_size=10)
def enable_puppet():
  sudo("puppet agent --enable")

def run_puppet():
  sudo("/opt/puppetlabs/bin/puppet agent -t; true", shell=False)

def run_puppet_noop():
  sudo("/opt/puppetlabs/bin/puppet agent -t --noop; true")

def serial_run_puppet_noop():
  sudo("/opt/puppetlabs/bin/puppet agent -t --noop; true")

def test_me():
  run("whoami")

def test_me_root():
  sudo("whoami", shell=False)

def check_ossec():
  sudo("grep ossec_v2 /opt/ossec/etc/ossec.conf")

def check_pam():
  run("egrep -c 'crack|quality'  /etc/pam.d/system-auth-ac")

def shut_trip():
  sudo("/opt/tripwire/bin/twdaemon stop ; /opt/tripwire/bin/twrtmd stop")

def check_trip2():
  sudo("ls -l /etc/init.d/tw* ; true", shell=False)

def check_maxstartups():
  sudo("grep -i maxstartup /etc/ssh/sshd_config ; true", shell=False)

def check_ciphers():
  sudo("grep Ciphers /etc/ssh/sshd_config ; true", shell=False)

def check_systemauth():
  sudo("md5sum /etc/pam.d/system-auth-ac ; true", shell=False)


def qacheck():
  put_file = "/home/mssiddi/repos/opensystems/bin/qachecks.sh"
  put(put_file, "/home/mssiddi/qachecks.sh")
  sudo("bash /home/mssiddi/qachecks.sh")

def ortcheck():
  put_file = "/home/mssiddi/repos/opensystems/bin/ortcheck.sh"
  put(put_file, "/home/mssiddi/ortcheck.sh")
  sudo("bash /home/mssiddi/ortcheck.sh")

def check_df():
  run("df -hP")

def check_tsm_rpm():
  run("rpm -q TIVsm-BA TIVsm-API64")

def check_tanium_date():
  run("rpm -q TaniumClients --qf '%{INSTALLTIME:date}\n'")

def check_pvs():
  sudo("/sbin/pvs ; true", shell=False)

def check_lvm_conf():
  sudo("ls -l /etc/lvm/lvm.conf ; true", shell=False)

def check_sudoers():
  sudo("md5sum /etc/sudoers ; true", shell=False)

def install_tanium():
  sudo("yum -y install TaniumClient ; true", shell=False)

def multipath_list():
  sudo("/sbin/multipath -ll ; true", shell=False)

def check_serial():
  sudo("facter dmi.product.serial_number; true", shell=False)
