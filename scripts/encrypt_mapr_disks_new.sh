#!/bin/bash
###
### This file is managed by Puppet. Any modifications will be overwritten
### Module  : disk_encrypt_tools
### SrcFile : filerepo/disk_encrypt_tools/encrypt_mapr_disks_nas.sh
###

# GUIDE: https://opsys-wiki.corp.transunion.com/dokuwiki/doku.php?id=wiki:hadoop:encrypted_hadoop_cluster

export PATH="/bin:/usr/bin:/sbin:/usr/sbin:$PATH"
D="$(date +%Y-%m-%d.%H:%M:%S)"
#LUKS_KEY_SOURCE="/root/encrypt_mapr/maprdisks-luks.key"
#Declare an Array for UNUSED DISKS to be populated
declare -a UNUSED_DISKS
#Define NAS share and mount point
NASMNTPNT="/shared/opensystems_luks"
LUKS_KEY="${NASMNTPNT}/maprdisks-luks.key"
#Get Hadoop Cluster name. Need this to mount the relevant NAS Share as the Keys are now stored on seperate NAS Share per cluster Env.
#For Stg we are just using one share for now accross all Stage Clusters.
ENCRYPTED_CLUSTER=$(/usr/local/bin/facter tu_encrypted_cluster)

#set hadoop group id and NAS Share based on env - stage/prod
HOST=$(hostname -s)
ENV=${HOST:5:1}
if [ ${ENV} -eq 5 ]; then
    HDPGROUP="hdp5adms"
    NASSHARE="CHCLSP5EMC01-dynamic.custstage.transunion.com:/ifs/cust_stage/shared/opensystems_analytics"
    #NASSHARE="nfscuststgsb.transunion.com:/ifs/cust_stage/shared/opensystems_analytics"
elif [ ${ENV} -eq 9 ]; then
    HDPGROUP="hdp9adms"
    case "${ENCRYPTED_CLUSTER}" in
        CORE)
            NASSHARE="nfscustsb.transunion.com:/ifs/data/shared/opensystems_analytics_core"
            ;;
        SHAPE)
            NASSHARE="nfscustsb.transunion.com:/ifs/data/shared/opensystems_analytics_shape"
            ;;
        HEALTHCARE) 
            NASSHARE="nfscustsb.transunion.com:/ifs/data/shared/opensystems_analytics_healthcare"
            ;;
        PRAMA) 
            NASSHARE="nfscustsb.transunion.com:/ifs/data/shared/opensystems_analytics_prama"
            ;;
        *)
            echo "PROBLEM: Could not determine which hadoop ENCRYPTED_CLUSTER this is"
            echo "INFO: Run facter tu_encrypted_cluster. This is set per hadoop cluster."
            echo "INFO: If this is brand new encrypted cluster. lease follow procedure from below link"
            echo "https://opsys-wiki.corp.transunion.com/dokuwiki/doku.php?id=wiki:hadoop:encrypted_hadoop_cluster"
            exit
            ;;
    esac
else
    echo "PROBLEM: Unable to determine environment(stg/prod) based on hostname"
    exit
fi

#Some utilities like lsscsi,lsblk are required so check if they are installed.
if [ ! -x /usr/bin/lsscsi ] || [ ! -x /bin/lsblk ] || [ ! -x /sbin/cryptsetup ]; then
    echo "PROBLEM: Pre-Requisites Failed. Please check if /usr/bin/lsscsi, /bin/lsblk and /sbin/cryptsetup are installed"
    exit 3
fi

checkIfEncrypted()
{
    if [ $(/bin/lsblk --fs $1 | grep -c crypto) -gt 0 ]; then
        return 1
    else
        return 0
    fi
}

isDiskUnused()
{
    dsk="$1"
    #Do as many checks possible to rule out any used disks.
    ls ${dsk}[0-9] >/dev/null 2>&1
    if [ $? -eq 0 ]; then
	    return 1 
    else
	if [ $(blkid | egrep -wc "${dsk}|${dsk}[0-9]}") -gt 0 ]; then
	    return 1
	else
	    return 0
	fi
    fi
}

mountNASShare()
{
    if [ "${NASSHARE}" != "" ]; then
        #check 1st if already mounted
        if [[ $(df -Ph | grep -c "${NASSHARE}") -gt 0 ]] || [[ $(df -Ph | grep -c "${NASMNTPNT}") -gt 0 ]]; then
            echo "PROBLEM: Something already mounted using ${NASSHARE} OR ${NASMNTPNT} - Please check"
            exit
        else
            mount ${NASSHARE} ${NASMNTPNT}
            if [ $? -gt 0 ]; then
                echo "PROBLEM: Mounting of ${NASSHARE} on ${NASMNTPNT} FAILED. Please check"
                exit
            fi
        fi
    else
        echo "PROBLEM: NO NAS SHARE Detected Please check"
        echo "INFO: New Clusters need a new NAS Share Setup"
        echo "INFO: https://opsys-wiki.corp.transunion.com/dokuwiki/doku.php?id=wiki:hadoop:encrypted_hadoop_cluster"
        exit
    fi
}

encryptDisk()
{
    dsk="$1"
    #****AT THIS POINT ALL CHECKS ARE DONE. THIS FUNCTION SHOULD ONLY BE CALLED FROM ANOTHER FUNCTION THAT DOES ALL CHECKS****
    #echo "Encrypting disk: ${dsk}"
    #On a 2nd thought. Do a double check if this disk has FS
    isDiskUnused ${dsk};_result=$?
    if [ ${_result} -gt 0 ]; then
        echo "EXITING: ${dsk} seems to have a filesystem or label or partition on it -- Please check"
        exit
    fi 
    #Create a dir for header backups
    #[ -d /opt/opensystems/backups/luks ] || mkdir -p /opt/opensystems/backups/luks
    [ -d ${NASMNTPNT}/backups/luks ] || mkdir -p ${NASMNTPNT}/backups/luks
    #Set LUKS Device numbers based on SCSI Target number as that will be unique and same across reboots and no need to depend on sdX names.
    HCTLID=$(/usr/bin/lsscsi | grep -w ${dsk} | awk '{print $1}' |sed 's/\[//g' | sed 's/\]//g' | awk -F: '{print $3}')
    #echo "HCTLID of ${dsk}: ${HCTLID}"
    echo "===ALL Looks OK, proceeding with encryption of ${dsk}==="
    /sbin/cryptsetup --batch-mode --use-random luksFormat "${dsk}" "${LUKS_KEY}"
    if [ $? -gt 0 ]; then
        echo "PROBLEM: luksFormat failed on ${dsk}. Please check before proceeding further"
        exit 11
    fi
    /sbin/cryptsetup luksOpen "${dsk}" MAPRLUKSDISK"${HCTLID}" --key-file "${LUKS_KEY}"
    if [ $? -gt 0 ]; then
        echo "PROBLEM: luksOpen failed on ${dsk}. Please check before proceeding further"
        exit 13
    else
        #Take a Backup of LUKS header just in case if ever needed. Offcourse if a passphrase is changed or drive replaced a new one need to be created.
        #So adding backup section to both all disks and single disk with datetime stamp.
        /sbin/cryptsetup luksHeaderBackup --header-backup-file ${NASMNTPNT}/backups/luks/$(hostname -s)_luks_header_backup.disk${HCTLID}.${D} ${dsk}
        #Set Perms on actual dm device
        echo "Encryption of ${dsk} completed. Setting permissions..."
        chgrp ${HDPGROUP} $(readlink -f /dev/mapper/MAPRLUKSDISK${HCTLID})
        chmod 0660 $(readlink -f /dev/mapper/MAPRLUKSDISK${HCTLID})
        ls -lH /dev/mapper/MAPRLUKSDISK${HCTLID}
    fi
    sleep 5
}

encryptSingleDisk()
{
    dsk="$1"
    if [ "${dsk}" == "" ]; then
        echo "PROBLEM: No disk was provided, see below for Usage"
        echo "Usage: "
        echo "$0 -d /dev/sdN [Encrypt Single Disk: Replace sdN with actual device to encrypt.Typical use case would be drive replacement]"
        exit 1
    fi
    if [ -b ${dsk} ]; then
        #Mount NAS Share for LUKS_KEY
        echo "Mounting NAS Share for LUKS_KEY"
        mountNASShare

        echo "===Performing pre-checks prior to encryption...==="
        #Check if this Node is Encrypted Hadoop Node.
        #LUKS_KEY should exist and also some encrypted disks.
        if [ ! -s ${LUKS_KEY} ] || [ $(/bin/lsblk --fs | grep -c crypto) -eq 0 ]; then
            echo "PROBLEM: You may be executing this script on wrong Server."
            echo "Please ensure this is a Encrypted Hadoop Node"
            exit 17;
        fi
        #Virtual OR Physical
        if [ $(grep -ci VMware /proc/scsi/scsi) -eq 0 ]; then
            #Check if it is UCSC-MRAID12G. Otherwise exit as we dont want to encrypt any other drive.
            if [ $(/usr/bin/lsscsi | grep -w ${dsk} | egrep -c "(MR9271-8i|UCSC-MRAID12G|PERC)") -eq 0 ]; then
                echo "PROBLEM: ${dsk} is not a MR9271-8i or UCSC-MRAID12G or PERC \(DELL\) disk."
                echo "INFO: Script is intended to encrypt only local RAID disks of type MR9271-8i, UCSC-MRAID12G, or PERC \(DELL\)"
                echo "INFO: Create RAID0 disk if you doing this part of failed drive replacement procedure"
                echo "INFO: Identify newly created sd device and provide that to the script"
                exit 15
            fi
        else
	        #Server is a VM. Lets check to see if disk being encrypted is attached to SCSI Controller 1 (aka host bus adapter 3 in OS)
	        if [ $(/usr/bin/lsscsi | grep -w ${dsk} | grep -c '^\[3:0') -ne 1 ]; then
		        echo "PROBLEM: The disk ${dsk} asked to be encrypted is not attached to VM SCSI Controller 1 (aka host bus adapter 3 in OS)"
		        echo "Please check and add new disk to same Controller as other Hadoop Disks are added"
		        exit 15
	        fi
	    fi
        #Check if disk already. If so, dont touch it.
        checkIfEncrypted ${dsk};_result=$?
        if [ ${_result} -eq 0 ]; then
            #Check if any partitions, lables or FS exists on disk.
            isDiskUnused ${dsk};_result=$?
            if [ ${_result} -gt 0 ]; then
                echo "EXITING: ${dsk} seems to have a filesystem or label or partition on it -- Please check"
                exit
            fi
            #Set LUKS Device numbers based on SCSI Target number as that will be unique and same across reboots and no need to depend on sdX names.
            HCTLID=$(/usr/bin/lsscsi | egrep "(MR9271-8i|UCSC-MRAID12G|PERC)" | grep -w ${dsk} | awk '{print $1}' |sed 's/\[//g' | sed 's/\]//g' | awk -F: '{print $3}')
            #Need to check and Close any hanging LUKS device for the failed disk before encrypting as luksOpen may fail with device alredy exists.
            if [ -e /dev/mapper/MAPRLUKSDISK${HCTLID} ]; then
                echo "An existing LUKS device /dev/mapper/MAPRLUKSDISK${HCTLID} still exists so removing that before proceeding"
	            /sbin/cryptsetup luksClose MAPRLUKSDISK${HCTLID}
                if [ $? -gt 0 ]; then
                    echo "PROBLEM: luksClose on /dev/mapper/MAPRLUKSDISK${HCTLID} failed. Please save details and escalate to Engineering"
                    exit 19;
                fi
            fi
            #Call encryptDisk function here
            encryptDisk ${dsk}
            #Unmount NAS Share after encryption is completed
            echo "Unmounting NAS Share"
            umount ${NASMNTPNT}
        else
            echo "PROBLEM: ${dsk} is already encrypted so exiting - Please check and proceed accordingly. If needed escalate to Engineering team"
            exit 23
        fi
    else
        echo "PROBLEM: ${dsk} doesnt exist.  Please check and proceed accordingly"
        exit 25
    fi
}


encryptAllDisks()
{

    echo "Performing Discovery steps to identify disks"
    #Check if any encrypted disks exists. If so, exit out as script could be mistakenly executed. As this portion is only for new builds.
    if [ $(/bin/lsblk --fs | grep -c crypto) -gt 0 ]; then
        echo "PROBLEM: server already has encrypted disks. Encrypt all disks is only intended for build time."
        echo "NOTE: If you need encrypt a single disk that was added later on, then use -d option" 
        exit 5
    fi

    #Check if VM
    if [ $(grep -ci VMware /proc/scsi/scsi) -eq 0 ]; then
	    #Server is BareMetal. Lets limit encryption to local LSI RAID0 disks, exclude any disks under LVM or already have a file system on it.
        #If no RAID disks exist then exit with a message.
        if [ $(/usr/bin/lsscsi | egrep -c "(MR9271-8i|UCSC-MRAID12G|PERC)") -eq 0 ]; then 
            echo "PROBLEM: No local MR9271-8i, UCSC-MRAID12G, or PERC \(DELL\) disks present."
            echo "INFO: Script is intended for New builds so please check procedure at https://opsys-wiki.corp.transunion.com/Encrypted_Hadoop"
            exit 7
        else
            for dsk in $(/usr/bin/lsscsi | egrep "(MR9271-8i|UCSC-MRAID12G|PERC)" | grep sd[a-z] | awk '{print $NF}'); do
		        isDiskUnused ${dsk}
		        if [ $? -eq 0 ]; then
                    UNUSED_DISKS+=("${dsk}")
                fi 
            done
        fi
    else
	    #Server is Virtual. Lets limit encryption to local disks and exclude any disks under LVM or has a FS on it.
	    # Also limiting to host adapter 3 (scsi controller 1 on VM guest layer)
	    #Discover unused disks.
	    for dsk in $(lsscsi  | egrep '^\[(1|3):0' | grep sd[a-z] | awk '{print $NF}'); do 
		    isDiskUnused ${dsk}
		    if [ $? -eq 0 ]; then
                UNUSED_DISKS+=("${dsk}")
            fi 
        done
        #echo ${UNUSED_DISKS[@]}
    fi

    #Do the actual encryption here.
    if [ ${#UNUSED_DISKS[@]} -eq 0 ]; then
	echo "PROBLEM: Didnt find any disks that are unused. Please check."
	exit
    fi
    echo "===Here is a list of disks that are detected as unused based on some predefined checks==="
    #printf '%s\n' "${UNUSED_DISKS[@]}"
    echo "TOTAL DISKS THAT ARE DISCOVERED AS UNUSED: ${#UNUSED_DISKS[@]}"
    lsscsi | egrep -w "$(echo ${UNUSED_DISKS[@]} | tr ' ' '|')"
    echo "***Please double check each of the above listed devices are actually unused and are ok to be encrypted. If yes, please confirm below***"
    read -p "Please confirm if you would to like proceed with encrypting above listed disks:[y/N] " confirm_encrypt
    case "$confirm_encrypt" in
        [yY])
            echo "===Staging LUKS Master Key and some pre-setup steps==="
            #Mount NAS Share for LUKS_KEY
            echo "===Mounting NAS Share for LUKS_KEY==="
            mountNASShare
            #Check if LUKS_KEY exists
            if [ ! -s ${LUKS_KEY} ]; then
                echo "PROBLEM: ${LUKS_KEY} doesnt exist. Please check"
                echo "INFO: If this is a brand new cluster a new key should be created by following the procedure at below link"
                echo "GUIDE: https://opsys-wiki.corp.transunion.com/dokuwiki/doku.php?id=wiki:hadoop:encrypted_hadoop_cluster"
                exit
            fi
            echo "===Proceeding with encryption of below disks:==="
            printf '%s\n' "${UNUSED_DISKS[@]}"
            for dsk in "${UNUSED_DISKS[@]}"; do
                encryptDisk ${dsk}
            done
            echo "===Unmounting NAS Share==="
            umount ${NASMNTPNT}
            ;;
        *)
            echo "Exiting without encrypting any disks as per your request" 
            exit
            ;;
        esac

}

case "$1" in
  -all)
	    encryptAllDisks ;;
  -d)
	    encryptSingleDisk $2;;
  *)
        echo "Usage: "
        echo "$0 -all [CAUTION: Encrypts all non-SAN disks. Only for build time use]"
        echo "$0 -d /dev/sdN [Encrypt Single Disk: Replace sdN with actual device to encrypt.Typical use case would be drive replacement]"
        echo "GUIDE: https://opsys-wiki.corp.transunion.com/dokuwiki/doku.php?id=wiki:hadoop:encrypted_hadoop_cluster"
        exit 1
esac
