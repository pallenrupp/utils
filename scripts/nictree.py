#!/usr/bin/env python

import sys

class tree:
  level=0
  def __init__(self):
     self.nodelist={"root":link("root",data=None)}
 
  def add_link(self,name,link):
     self.nodelist['root'].add_child(name)
     self.nodelist[name]=link
     self.nodelist[name].add_parent('root')
        
  def del_link(self,name):
     self.nodelist['root'].del_child(name)
     del self.nodelist[name]
     
  def relate(self,pname,cname):
     rootlink=self.nodelist["root"]
     self.nodelist[pname].add_child(cname)
     self.nodelist[cname].add_parent(pname)
     # Remove child's link from root, because somebody
     # else is now pointing to the child.
     self.nodelist["root"].del_child(cname) 

     #print("after add_parent", pname, cname)
     #print("cname.parents=", self.nodelist[cname].parents)
     #print("cname.children=", self.nodelist[cname].children)
     #print("pname.parents=", self.nodelist[pname].parents)
     #print("pname.children=", self.nodelist[pname].children)
     return self

  def get(self,name):
     # returns a link object if name in the tree
     if self.nodelist.has_key(name):
        return self.nodelist[name]
     else:
        print("Error: tree.get(%s) does not exist."%(name))

  def walk(self,name="root"):
     link=self.nodelist[name]
     nav="|_"
     spaces=' '*2*tree.level
     print(spaces+nav+link.name)

     tree.level=tree.level+1
     for child in link.children:
        self.walk(name=child)
     tree.level=tree.level-1

class link:
  def __init__(self,name=None,data=None):
    self.name=name
    self.data=data
    self.children=[]
    self.parents=[]

  def add_child(self,name):
     if (self.children.count(name) < 1):
        self.children.append(name) 
 
  def add_parent(self,name):
     if (self.parents.count(name) < 1):
        self.parents.append(name) 

  def del_child(self,name):
     while name in self.children: self.children.remove(name)

  def del_parent(self,name):
     while name in self.parents: self.parents.remove(name)

  def get_level(self):
     level = 0
     p = self.parent
     while p:
        level += 1
        p = p.parent
     return level

  def print_tree(self):
     nav="|_"
     spaces=' '*2*self.get_level()
     print(spaces+nav+self.name)
     for child in self.children:
        child.print_tree()

if __name__ == '__main__':
   nics=("enp0s3", \
         "enp0s8", \
         "enp0s9", \
         "enp0s10", \
         "enp0s11", \
         "enp0s12", \
         "team0", \
         "team1", \
         "team0.100", \
            "team0.200", \
         "team1.200", \
         "team1.100")

   # Load the tree
   t=tree()
   for i in nics:
     t.add_link(i,link(i,None))
   # setup relationships
   
   t.relate('team1.100','team1')
   t.relate('team1.200','team1')
   t.relate('team0.100','team0')
   t.relate('team0.200','team0')
   t.relate('team0','enp0s8')
   t.relate('team0','enp0s9')
   t.relate('team1','enp0s10')
   t.relate('team1','enp0s11')
   t.walk()
   sys.exit(0)
