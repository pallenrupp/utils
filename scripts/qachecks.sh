#!/bin/bash
export PATH="/bin:/usr/bin:/sbin:/usr/sbin:$PATH"
display () {
    printf '%-22s %-2s %-50s\n' "$1" ":" "$2"
}
#Hardware
MODEL=$(/usr/sbin/dmidecode -t 1 | grep "Product Name:" | awk -F: '{print $NF}')
BIOS_VER=$(/usr/sbin/dmidecode -t 0 | grep "Version:" | awk -F: '{print $NF}')
BB_VER=$(/usr/sbin/dmidecode -t 2 | grep "Version:" | awk -F: '{print $NF}')
CHASSIS_VER=$(/usr/sbin/dmidecode -t 3 | grep "Version:" | awk -F: '{print $NF}')
CPUMODEL=$(grep "^model name" /proc/cpuinfo  | sort -u | awk -F: '{print $NF}')
CPUDIES=$(grep "^physical id" /proc/cpuinfo  | sort -u | wc -l)
CORES=$(grep "^cpu cores" /proc/cpuinfo  | sort -u | awk '{print $NF}')
SIBLINGS=$(grep "^siblings" /proc/cpuinfo  | sort -u | awk '{print $NF}')
VCPUS=$(grep -c "^processor" /proc/cpuinfo)
#MEMORY=$(free -g | grep "^Mem:" | awk '{print $2}')
NUMERATOR=$(free -m | grep "^Mem:" | awk '{print $2}');DENOMINATOR=1024;
MEMORY=`python -c "print (int(round(${NUMERATOR}.0 / ${DENOMINATOR}.0)))"`
KERNEL_VER=$(uname -r)

display "HOSTNAME" "$(hostname)"

if [ $(grep -c VMware /proc/scsi/scsi) -eq 0 ]; then
    display "HARDWARE MODEL" "${MODEL}"
    display "BIOS VERSION/FIRMWARE" "${BIOS_VER}"
    display "BASE BOARD VERSION" "${BB_VER}"
    display "CHASSIS VERSION" "${CHASSIS_VER}"
fi

display "CPU MODEL" "${CPUMODEL}"
if [ $(grep -c VMware /proc/scsi/scsi) -eq 0 ]; then
    display "CPU SOCKETS" "${CPUDIES}"
    display "CORES PER SOCKET " "${CORES}"
    display "SIBLINGS PER SOCKET" "${SIBLINGS}"
fi

display "TOTAL vCPUS" "${VCPUS}"
display "TOTAL MEMORY" "${MEMORY}G"


# Check that that disks support SATA 12Gb/s speeds if on physical servers
if [ $(grep -ci vmware /proc/scsi/scsi) -eq 0 ]; then
    if grep -q "PowerEdge" <<< "${MODEL}"; then
        lsi_binary="/opt/opensystems/bin/perccli64"
    else
        lsi_binary="/opt/opensystems/bin/storcli64"
    fi

    if [ `${lsi_binary} /c0/eall/sall show all | grep -ci "12.0Gb"` -ne 0 ]; then
        display "SATA/SAS Link/Device speeds" "12.0Gb/s"
    fi
    if [ `${lsi_binary} /c0/eall/sall show all | grep -ci "6.0Gb"` -ne 0 ]; then
        display "SATA/SAS Link/Device speeds" "6.0Gb/s"
    fi
fi

if [ ! -s /etc/sysconfig/iptables ]; then
    if [ `chkconfig --list | grep ip6tables |  grep "$(runlevel | cut -d' ' -f2):on" | wc -l` -eq 1 ]; then
        chkconfig iptables off
        chkconfig ip6tables off
    fi
fi

 
if grep -q VMware /proc/scsi/scsi; then
    if [ $(ps -ef | grep -c [v]mtools) -eq 0 ]; then
        echo "Fixing VM Tools"
        vmware-config-tools.pl -d > /dev/null 2>&1
    fi
fi

#Print /opt or appvg FS
df -Ph -t ext4 -t xfs -t nfs | egrep -v "rootvg|boot"
