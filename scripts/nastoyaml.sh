#!/bin/bash

############################################################################
# PUPPET CONTROLLED FILE - DO NOT MAKE CHANGES HERE - CHANGE MASTER IN CVS #
# THEN PUT UPDATED FILE HERE FOR REDEPLOY:                                 #
#                                                                          #
#     chmgl9cf01:/opt/puppet/filerepo/opsys_utils/opt/opensystems/bin      #
#                                                                          #
############################################################################

for dir in `grep nfs /etc/fstab | grep -v home | grep -v '^#' | awk '{print $2}'`; do 
	echo "  '${dir}':";
	echo "    dv      : '$(grep -w ${dir} /etc/fstab | grep -v '^#' | awk '{print $1}')'";
	#echo "    owner   : '$(ls -ld ${dir}| awk '{print $3}')'";
	#echo "    group   : '$(ls -ld ${dir} | awk '{print $4}')'";
	#echo "    mode    : '$(stat -c "%a" ${dir})'";
        echo "    options : '$(grep -w ${dir} /etc/fstab | grep -v '^#' | awk '{print $4}')'";
done 
