#!/usr/bin/env bash

# Get list of all SCSI disk WWNN's
# Initialize temp files

LSSCSI=/tmp/LSSCSI
if lsscsi | fmt -su > $LSSCSI
then
   :
else
   echo "Error:  lsssci command not found - required.  Exiting..."
   return 1
fi

header() {

   echo "This report lists all SCSI disk devices"
   echo "Legend:"
   echo "   \"scsi-dev\"   The SCSI block device name (i.e, /dev/sdc)"
   echo "   \"[H:C:T:L]\"  The SCSI driver assigned HOST, CONTROLLER, TARGET, and LUN" 
   echo "   \"scsi-wwid\"  The assigned SSA WWID, derived from the \"scsi_id -g\" command"
   echo "   \"mpath?\"     If the device is controlled vi the Linux native DM Multipath driver"
   echo "   \"make\"       The SCSI device vendor (derived from a scsi inquiry)"
   echo "   \"model\"      The SCSI device model (derived from a scsi inquiry)"
   echo "   \"hwpath\"     The Linux device path under /sys. Useful to determine if the"
   echo "                  path is on a different controller"
   echo ""
}
get_hwpath() {
# 
#where $1 is full path to a scsi/block device
# Two ways to get hardware path from  /sys/devices/..
# e.g,
# $ udevadm info --query=path --name=/dev/sda
# /devices/pci0000:00/0000:00:01.0/0000:02:00.0/host0/target0:2:0/0:2:0:0/block/sda
#
# or...
# $ cd /sys;find devices -name "sda"
# devices/pci0000:00/0000:00:01.0/0000:02:00.0/host0/target0:2:0/0:2:0:0/block/sda
#

   i=$1
   if `which udevadm > /dev/null  2>&1`
   then
      hwpath=$(udevadm info --query=path --name=$i)
   else
      sdN=$(basename $i) 
      hwpath=$(cd /sys;find devices -name $sdN)
   fi
   echo "$hwpath"
}

get_hctl() { 
   i=$1
   hctl=$(grep "${i}$" $LSSCSI|awk '{print $1}')
   echo "$hctl"
}
get_make() {
   i=$1
   make=$(grep "${i}$" $LSSCSI|awk '{print $3}')
   echo "$make"
}
get_size() {
   i=$1
   echo $(lsblk -n -d $i | awk '{print $4}') 
}


get_model() {
   i=$1
   work=$(grep "${i}$" $LSSCSI|awk '{out=""; for(i=4;i<=NF-2;i++){out=out" "$i}; print out}')
   work=${work##*()}
   model=$(echo $work|sed 's/ /-/g')
   echo "$model"
}

doit() {

  for i in $(cat $LSSCSI|grep /dev/sd| awk '{print $NF}')
  do
     # where $i is the full path to the scsi device.
     # e.g, /dev/sda

     sdN=`basename $i`

     # get scsi_id
     if `which scsi_id > /dev/null 2>&1`
     then
         wwn=$(scsi_id -g $i)
     else
         wwn=$(ls -ld /dev/disk/by-id/scsi*|grep "${sdN}$"|awk '{print $9}' | awk -Fscsi- '{print $2}')
     fi
     if [[ "$wwn" == "" ]]; then
        wwn="n/a"
     fi
     # get multipathing info
     if `which multipath > /dev/null  2>&1`
     then
         if multipath -ll $i >/dev/null 2>&1
         then
            mpath="yes"
         else
            mpath="no "
         fi
     else
         mpath="n/a"
     fi
     hctl=$(get_hctl $i)
     make=$(get_make $i)
     model=$(get_model $i)
     hwpath=$(get_hwpath $i)
     mysize=$(get_size $i)
     printf "%-10s %-10s %-35s %-7s %-10s %-15s %s\n" "$i" " $hctl" "$wwn" "$mysize" "$mpath" "$make" "$model" 
  done
}
header
printf "%-10s %-10s %-35s %-7s %-10s %-15s %s\n" "scsi-dev" "[H:C:T:L]" "scsi-wwid" "size" "mpath?" "make" "model"
doit | sort -k 3
