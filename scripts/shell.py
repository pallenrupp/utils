class shell():
   def  __init__(self,cmd=None,rc=0,stdout="",stderr=""):
      self.rc=rc
      self.cmd=cmd
      self.stdout=stdout
      self.stderr=stderr

   def __repr__(self):
      work="cmd='%s',rc=%s,stdout(%s bytes),stderr(%s bytes)"%(str(self.cmd),str(self.rc),str(len(self.stdout)),str(len(self.stderr)))
      return work
  
   def __str__(self):
      work="cmd='%s',rc=%s,stdout(%s bytes),stderr(%s bytes)"%(str(self.cmd),str(self.rc),str(len(self.stdout)),str(len(self.stderr)))
      return work

   def getrc(self):
      return self.rc

   def getcmd(self):
      return self.cmd

   def setcmd(self,cmd):
      self.cmd=cmd
      self.rc=0
      self.stdout=""
      self.stderr=""

   def getstdout(self):
      return self.stdout

   def getstderr(self):
      return self.stderr

   def run(self):
      self.rc=0
      self.stdout=''
      self.stderr=''

      if self.cmd !=None: 
         proc=subprocess.Popen(self.cmd, shell=True, stderr=subprocess.PIPE, stdout=subprocess.PIPE, universal_newlines=True)
         self.stdout=proc.stdout.read()
         self.stderr=proc.stderr.read()
         self.rc=proc.wait()
      return self
