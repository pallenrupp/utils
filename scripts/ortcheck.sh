#!/bin/bash

HOSTFQDN=$(hostname -f)
export PATH="/bin:/usr/bin:/sbin:/usr/sbin:$PATH"
DOMAIN="${HOSTFQDN#*.}"
D=$(date +%m%d%y_%H%M%S)
#CURPATCHCHANNEL="clone-20150901-rhel-x86_64-server-6"
#CURLATESTKERNEL="2.6.32-573.3.1.el6.x86_64"
RUN_USER=$(dirname "${BASH_SOURCE[0]}" | awk -F/ '{print $3}')
EMAIL="${RUN_USER}@transunion.com"
ORTLOG="/opt/opensystems/logs/ort.log.${D}"
KERNEL_VER=$(uname -r)
RELEASE=$(awk '{ print $7 }' /etc/redhat-release | sed 's/.[0-9]//')
SKIP_TSM="1"
if [ "$1" == "-sTSM" ]; then
	SKIP_TSM="yes"
fi
display ()
{
	printf '%-30s %-2s %-7s\n' "$1" ":" "$2"
}

logIt ()
{
	echo $1 >> ${ORTLOG}	
	cat $2 >> ${ORTLOG}
}


#Lets try to determine why type of host this is.
if grep -q VMware /proc/scsi/scsi; then
	if [ $(rpm -qa | grep -ci gpfs) -gt 0 ]; then
		display "HOST-INFO" "$(hostname)-$(uname -r)-VIRTUAL_GPFS"
	else
		display "HOST-INFO" "$(hostname)-$(uname -r)-VIRTUAL"
	fi
else
	if [ $(rpm -qa | grep -ci gpfs) -gt 0 ]; then
		display "HOST-INFO" "$(hostname)-$(uname -r)-PHYSICAL_GPFS"
	else
        display "HOST-INFO" "$(hostname)-$(uname -r)-PHYSICAL"
	    if [ $(ifconfig  | grep eth[0-9] | grep -v 'eth[0-9]:' | awk '{print $1}' | grep -c eth2) -eq 1 ]; then
            if [ `host $(ifconfig  eth2 | grep "inet addr:" | awk '{print $2}' | awk -F: '{print $2}') | grep -c "\-ic"` -eq 1 ] && [ $(hostname | grep -c db) -eq 1 ]; then
                HOSTTYPE="DBRAC"
            fi
    	fi
    fi
fi

if grep -q VMware /proc/scsi/scsi; then
	ps -ef | grep [v]mtools >> ${ORTLOG}
	if ps -ef | grep -q [v]mtools; then
		display "VMTools is running" "PASSED"
	else
		if [ -d /etc/vmware-tools ]; then
			display "VMTools is running" "FAILED"
		else
			display "VMTools is running" "NOT INSTALLED - If Spark based, it is not installed by design"
		fi
		#echo "running vmtools config - will take a few mins"
		#vmware-config-tools.pl -d > /dev/null 2>&1
	fi
fi


#if [ `spacewalk-channel -l | grep -c "${CURPATCHCHANNEL}"` -eq 0 ]; then
#	display "Associated Patch Channel" "FAILED"
#else
#	display "Associated Patch Channel" "PASSED"
#fi

#if [ "$(uname -r)" == "${CURLATESTKERNEL}" ]; then
#	display "Latest Kernel" "PASSED"
#else
#	display "Latest Kernel" "FAILED"
#fi

if [ `who -r | grep -c "run-level 3"` -eq 1 ]; then
	display "Run Level Check" "PASSED"
else
	display "Run Level Check" "FAILED"
fi
	

#NTP/chrony check
if [ ${RELEASE} == "6" ]; then 
    ntpq -4 -p > /tmp/ortcheck.tmp
    if [[ `grep -c "^*" /tmp/ortcheck.tmp` -eq 1 ]] && [[ `grep -c "^+" /tmp/ortcheck.tmp` -eq 1 ]]; then
    	display "NTP CHECK" "PASSED"
    else
    	display "NTP CHECK" "FAILED"
    fi
elif [ ${RELEASE} == "7" ]; then
    chronyc -4 sources > /tmp/ortcheck.tmp
    if [ $(awk -F"= " '/sources/{print $2}' /tmp/ortcheck.tmp) -eq 2 ]; then
        display "chrony CHECK" "PASSED"
    else
        display "chrony CHECK" "FAILED"
    fi
fi 
logIt "NTP/chrony Check" "/tmp/ortcheck.tmp"

#Mail Check
date | mailx -s "$(uname -a)" ${EMAIL}
sleep 2
if [ `tail -n20 /var/log/maillog | grep -ci refuse` -gt 0 ]; then
	display "MAIL CHECK" "FAILED"
else
	display "MAIL CHECK" "PASSED"
fi


#Logrorate Check
logrotate -d /etc/logrotate.conf > /tmp/ortcheck.tmp 2>&1

if [ ${RELEASE} == "6" ]; then 
   if [[ `grep -c '^error' /tmp/ortcheck.tmp` -eq 0 ]] && [[ `ls -l /var/lib/logrotate.status | grep -c "$(date +'%b %e')"` -eq 1 ]]; then
        display "LOGROTATE CHECKS" "PASSED"
else
        display "LOGROTATE CHECKS" "FAILED"
fi
   elif [ ${RELEASE} == "7" ]; then
if [[ `grep -c '^error' /tmp/ortcheck.tmp` -eq 0 ]] && [[ `ls -l /var/lib/logrotate/logrotate.status | grep -c "$(date +'%b %e')"` -eq 1 ]]; then
        display "LOGROTATE CHECKS" "PASSED"
else
        display "LOGROTATE CHECKS" "FAILED"
fi
fi
logIt "Logrotate Check" "/tmp/ortcheck.tmp"

# OSSEC check
if [[ $(rpm -qa | grep -ci wazuh-agent) -eq 1 ]]; then
    display "OSSEC Installed" "PASSED"
	systemctl status wazuh-agent.service > /dev/null 2>&1
    if [ $? -eq 0 ]; then
        display "OSSEC running" "PASSED"
    else
        display "OSSEC running" "FAILED"
    fi

else
    display "OSSEC Installed" "NOT INSTALLED"
fi


:>/tmp/ortcheck.tmp
if [ ${RELEASE} == "6" ]; then 
	for SVC in `chkconfig --list | grep "$(runlevel | cut -d' ' -f2):on" | awk '{print $1}' | sort -g` ; do echo ${SVC}-------------------------------- && /etc/init.d/${SVC} status; done > /tmp/ortcheck.tmp 2>&1
	if [ $(grep -ci "not running" /tmp/ortcheck.tmp) -gt 0 ]; then
		display "chkconfig service check" "FAILED"
		grep -i "not running" /tmp/ortcheck.tmp
	else
		display "chkconfig service check" "PASSED"
	fi
elif [ ${RELEASE} == "7" ]; then
#    systemctl list-units | grep "[^[:ascii:]] failed" | egrep -v '(rngd|twrtmd)' > /tmp/ortcheck.tmp 2>&1
#    systemctl list-units | grep "failed" | egrep -v '(rngd|twrtmd)' > /tmp/ortcheck.tmp 2>&1
    systemctl list-units | grep  "[[:alnum:]] failed [[:alnum:]]" | egrep -v '(rngd|twrtmd|rdma|mapr-warden|mapr-zookeeper)' > /tmp/ortcheck.tmp 2>&1
	if [ $(grep -ci "failed" /tmp/ortcheck.tmp) -gt 0 ]; then
		display "systemctl list-units check" "FAILED"
		grep -i "failed" /tmp/ortcheck.tmp
	else
		display "systemctl list-units check" "PASSED"
	fi
fi
logIt "Service Check" "/tmp/ortcheck.tmp"

# Only running FSCK check interval on RHEL6; RHEL7 uses xfs and does not run xfs_repair
# on boot time. xfs_repair will replay the journaling log at mount time to ensure
# clean mount and consistency.
# https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/6/html/Storage_Administration_Guide/xfsrepair.html
if [ ${RELEASE} == "6" ]; then 
	for fs in $(lvscan|grep -v swap|cut -f 2,2 -d\');do echo -n "$fs ";tune2fs -l $fs|grep "Check interval" | awk '{print $3" "$4}';done > /tmp/ortcheck.tmp

	if [ $(grep -cv "0 (<none>)" /tmp/ortcheck.tmp) -gt 0 ]; then
		display "FSCK check interval" "FAILED"
		grep -v "0 (<none>)" /tmp/ortcheck.tmp
	else
		display "FSCK check" "PASSED"
	fi
	logIt "FSCK Check interval" "/tmp/ortcheck.tmp"
fi

# Check Mounts
/opt/opensystems/bin/check_mounts.py
if [ $? -eq 0 ]; then
	display "CHECK MOUNTS" "PASSED"
else
	display "CHECK MOUNTS" "FAILED"
fi

# Check multipaths if they are present
rpm -qa | grep multipath
if [ $? -eq 0 ]; then
    if [ $(grep -ci vmware /proc/scsi/scsi) -eq 0 ]; then
        TOTALPATHS=$(/sbin/multipathd paths count | grep Path | awk '{print $NF}')
        TOTALLUNS=$(/sbin/multipathd show maps | grep -cv uuid)
        echo "TOTAL LUNS: ${TOTALLUNS}" > /tmp/ortcheck.tmp
        echo "TOTAL PATHS: ${TOTALPATHS}" >> /tmp/ortcheck.tmp
        if [ -s /opt/opensystems/bin/check_paths.sh ]; then
            /opt/opensystems/bin/check_paths.sh >> /tmp/ortcheck.tmp 2>&1
            if [ $? -eq 0 ]; then
                display "CHECK PATHS" "PASSED"
            else
                display "CHECK PATHS" "FAILED"
            fi
        else
            display "CHECK PATHS" "FAILED[Missing check_paths.sh please get puppet to push it]"
            echo "Missing check_paths.sh please get puppet to push it or copy manually to /opt/opensystems/bin/check_paths.sh" >> /tmp/ortcheck.tmp
        fi
    fi
elif [ $? -eq 1 ]; then
    display "CHECK PATHS - no multipaths found" "PASSED"
fi
logIt "Check Paths" "/tmp/ortcheck.tmp"

# Check rootvg and make sure it's a RAID1 mirror
if [ $(grep -ci vmware /proc/scsi/scsi) -eq 0 ]; then
    # Try running UCS 'storecli64' command first
    if [ `/opt/opensystems/bin/storcli64 /c0/v0 show | grep -ci RAID1` -eq 0 ]; then
       # Try running the DELL 'perccli64' command next 
       if [ `/opt/opensystems/bin/perccli64 /c0/v0 show | grep -ci RAID1` -eq 0 ]; then
          display "rootvg RAID1 CHECK" "FAILED"
       else
        display "rootvg DELL  RAID 1 CHECK" "PASSED"
       fi 
    else
        display "rootvg CISCO RAID 1 CHECK" "PASSED"
    fi
fi

# Check /etc/hosts
if [[ `grep -v '^#' /etc/hosts|grep -E "$(uname -n)|localhost" | grep -c "$(hostname -f)"`  -eq 1 ]] && [[ `grep -v '^#' /etc/hosts|grep -E "$(uname -n)|localhost" | grep -c "^127.0.0.1   localhost"` -eq 1 ]]; then
	display "ETC HOSTS CHECK" "PASSED"
else
	display "ETC HOSTS CHECK" "FAILED"
fi


# NIC Checks
for nic in `ifconfig  | grep eth[0-9] | grep -v 'eth[0-9]:' | awk '{print $1}'`; do
	/sbin/ethtool ${nic} > /tmp/ortcheck.tmp
	SPEED=$(grep "Speed:" /tmp/ortcheck.tmp | awk '{print $NF}')
	NICMTU=$(ifconfig ${nic} | grep -o "MTU:[0-9][0-9][0-9][0-9]")
	#Check MTU
	if [ "${nic}" == "eth0" ] ; then
		if [ "${NICMTU}" != "MTU:1500" ]; then
			NICMTU="${NICMTU} - WARNING!! check MTU"
		fi
	fi
	if [ "${nic}" == "eth1" ] ; then
		if [ "${NICMTU}" != "MTU:9000" ]; then
			NICMTU="${NICMTU} - WARNING!! check MTU"
		fi
	fi
	if [ "${nic}" == "eth2" ] ; then
		if [ `host $(ifconfig  eth2 | grep "inet addr:" | awk '{print $2}' | awk -F: '{print $2}') | grep -c gpfs` -eq 1 ]; then
			if [ "${NICMTU}" != "MTU:9000" ]; then
				NICMTU="${NICMTU} - WARNING!! check MTU - GPFS NICs should now be 9000 i think"
			fi
		#elif [ `host $(ifconfig  eth2 | grep "inet addr:" | awk '{print $2}' | awk -F: '{print $2}') | grep -c "\-ic"` -eq 1 ]; then
        elif [ "${HOSTTYPE}" == "DBRAC" ]; then
			if [ "${NICMTU}" != "MTU:9000" ]; then
				NICMTU="${NICMTU} - WARNING!! check MTU"
			fi
		else
				NICMTU="${NICMTU} - ??"
		fi
	fi
    if [ $(grep -ci vmware /proc/scsi/scsi) -gt 0 ]; then
	#if [[ `/sbin/ethtool ${nic} | grep -c "Link detected: yes"` -eq 1 ]] && [[ `/sbin/ethtool ${nic} | grep -c "Duplex: Full"` -eq 1 ]] && [[  `/sbin/ethtool ${nic} | egrep -c "Speed: 10000Mb/s"` -eq 1 ]]; then
        if [[ `grep -c "Link detected: yes" /tmp/ortcheck.tmp` -eq 1 ]] && [[ `grep -c "Duplex: Full" /tmp/ortcheck.tmp` -eq 1 ]] && [[  `egrep -c "Speed: 10000Mb/s" /tmp/ortcheck.tmp` -eq 1 ]]; then
            display "NICCHECKS-Link/Duplex-${nic}" "PASSED [SPEED:${SPEED} ${NICMTU}]"
        else
            display "NICCHECKS-Link/Duplex-${nic}" "FAILED [SPEED:${SPEED} ${NICMTU}]"
        fi
    else
	    if [[ `grep -c "Link detected: yes" /tmp/ortcheck.tmp` -eq 1 ]] && [[ `grep -c "Duplex: Full" /tmp/ortcheck.tmp` -eq 1 ]] && [[  `egrep -c "Speed: 10000Mb/s|Speed: 20000Mb/s|Speed: 40000Mb/s" /tmp/ortcheck.tmp` -eq 1 ]]; then
	#if [[ `/sbin/ethtool ${nic} | grep -c "Link detected: yes"` -eq 1 ]] && [[ `/sbin/ethtool ${nic} | grep -c "Duplex: Full"` -eq 1 ]] && [[  `/sbin/ethtool ${nic} | egrep -c "Speed: 10000Mb/s|Speed: 20000Mb/s|Speed: 40000Mb/s"` -eq 1 ]]; then
            display "NICCHECKS-Link/Duplex-${nic}" "PASSED [SPEED:${SPEED} ${NICMTU}]"
        else
            display "NICCHECKS-Link/Duplex-${nic}" "FAILED [SPEED:${SPEED} ${NICMTU}]"
        fi
    fi
    logIt "NIC Checks" "/tmp/ortcheck.tmp"
done

# Ping Gateways
PROBLEMCNT=0
for gw in $(netstat -rn|grep -v lo0|awk '/^[0-9]/ {print $2}'|sort -u|grep -v "^0"); do 
	ping -c 1 $gw > /dev/null 2>&1
	if [ $? -gt 0 ]; then
               	(( PROBLEMCNT=${PROBLEMCNT}+1 ))
	fi
done
if [ ${PROBLEMCNT} -eq 0 ]; then
	display "GATEWAY PING" "PASSED"
else
	display "GATEWAY PING" "FAILED"
fi


# Check that forward and Reverse DNS are the same 
dnscheck () {
	host $1 > /tmp/ortcheck.tmp
	if [[ $? -eq 0 ]] && [[ `host $(host "$1" | awk '{print $NF}') | grep -c  "$1.${DOMAIN}"` -eq 1 ]]; then
		display "DNS Checks $1" "PASSED"
	else
		display "DNS Checks $1" "FAILED"
	fi
logIt "DNS Check" "/tmp/ortcheck.tmp"
}

dnscheck "$(hostname)"
#if [ $(rpm -qa | grep -c gpfs) -gt 0 ]; then
#	DOMAIN="transunion.com"
#	dnscheck "$(hostname)-gpfs"
#fi
#if [ $(hostname | grep -c "-sb") -eq 1 ]; then
if [ $(hostname | grep -c "\-sb") -eq 1 ]; then
    dnscheck "$(hostname)-sb"
elif [ $(hostname | grep -c "db[0-9]") -eq 1 ]; then
	if [ $(ifconfig | grep -c eth2) -gt 0 ]; then
		dnscheck "$(hostname)-ic"
	fi
fi


#Storage mounted over multipath links servives a loss of path
if [ $(grep -ci vmware /proc/scsi/scsi) -eq 0 ]; then
#readarray -t DISKS <<< "`multipath -ll rootvg_vol  | grep -v \"dm-\" | grep sd[a-z] | awk '{print $3}'`"
#detemine root mpath alias
#root_mpath=`pvs | grep rootvg | head -1 | awk -F/ '{print $4}' | awk '{print $1}' | sed 's/p2//g'`

    rpm -qa | grep multipath
    if [ $? -eq 0 ]; then
        if [ $(multipath -ll | grep -c rootvg_vol) -eq 1 ]; then
        readarray -t DISKS <<< "`multipath -ll rootvg_vol  | grep -v \"dm-\" | grep sd[a-z] | awk '{print $3}'`"
            if [ $(echo ${DISKS[*]} | grep -c 'sd[a-z]') -gt 0 ]; then
                iostat -d ${DISKS[0]} ${DISKS[1]} 2 20 > /tmp/ortcheck.tmp 2>&1 &
                dd if=/dev/zero of=/tmp/zBIGFILE bs=1024 count=3128448 >/dev/null 2>&1
                rm -f /tmp/zBIGFILE

                PROBLEMCNT=0
                for dsk in "${DISKS[@]}"; do
                    if [ `grep ${dsk} /tmp/ortcheck.tmp  | awk '{ if ($4 > 10000) print $4}' | wc -l` -eq 0 ]; then
                                (( PROBLEMCNT=${PROBLEMCNT}+1 ))
                    fi
                done	
                if [ ${PROBLEMCNT} -eq 0 ]; then
                    display "DATA ON MPATH DISKS(${DISKS[0]} ${DISKS[1]})" "PASSED"
                else
                    display "DATA ON MPATH DISKS(${DISKS[0]} ${DISKS[1]})" "FAILED"
                fi
            else
                display "DATA ON MPATH DISKS(${DISKS[0]} ${DISKS[1]})" "FAILED - couldnt get underlying disks list for rootvg_vol mpath"
            fi
        else
            display "DATA ON MPATH DISKS(${DISKS[0]} ${DISKS[1]})" "FAILED [Did not find rootvg_vol mpath alias]"
        fi
    elif [ $? -eq 1 ]; then
        display "No multipaths found or installed" "PASSED"
    fi
fi
logIt "Data on MPATH DISKS Check" "/tmp/ortcheck.tmp"

#Check if TSM Client Backups are configured. For now doing only for physical hosts.
#As of 09/22 per Liam VMs also backed up by TSM client.
if [ -z "${SKIP_TSM}" ]; then
    if [ -f /usr/bin/dsmc ]; then
        if [ $(echo "quit" | /usr/bin/dsmc q sched | grep -c "Schedule Name:") -eq 0 ]; then
            display "TSM Client Backups Setup" "FAILED"
        else
            display "TSM Client Backups Setup" "PASSED"
        fi
    else
            display "TSM Client Backups Setup" "FAILED[TSM client not installed]"
    fi
fi


#Oracle RAC Node checks
#How do i determine if it is a ORACLE RAC Node.
if [ "${HOSTTYPE}" == "DBRAC" ]; then
    echo "DBRAC node checking few more things"
    #Check if Transparent HugePages is set to never
    if [ $(grep ${KERNEL_VER} /boot/grub/grub.conf | grep -c "transparent_hugepage=never") -eq 0 ]; then
        display "transparent_hugepage=never" "FAILED"
    else
        display "transparent_hugepage=never" "PASSED"
    fi
    if [ $(grep ${KERNEL_VER} /boot/grub/grub.conf | grep -c "elevator=deadline") -eq 0 ]; then
        display "elevator=deadline" "FAILED"
    else
        display "elevator=deadline" "PASSED"
    fi
    if [ $(grep -c "NOZEROCONF=yes" /etc/sysconfig/network) -eq 0 ]; then
        display "NOZEROCONF=yes" "FAILED"
    else
        display "NOZEROCONF=yes" "PASSED"
    fi
fi
