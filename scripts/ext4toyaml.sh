#!/bin/bash
#
# This file is managed by Puppet and any changes will be overwritten.
# Module: opsys_utils
#
####################################################################
#
# ext4toyaml.sh         Ver: 1.1.0
#
# Creates a puppet hash of the exiting non-rootvg ext4 filesysmtems
# currently on this server for use in adding to the node yaml,
# assuming the puppet role assigned supports FS management.
#
# 04/11/17	1.1.0 Correct for 'mapper' lines
# 01/29/16	1.0.0 Initial version (Rider)
#
####################################################################

echo "# -------------------------------------------"
echo "# Generated: $(date)"
echo "# via $(basename $0)"
echo "#"
echo "# Note that this lists out ALL non-rootvg ext4 filesystems"
echo "# in *appvg*, even those already managed by puppet elsewhere..."
echo "# Run this output past the puppet folks before actually using."
echo "#"
echo "# If not explicity set, the defaults are:"
echo "#   opts  : 'rw,strictatimerw'"
echo "#   dump  : '0'"
echo "#   pass  : '0'"
echo "#   owner : 'root'"
echo "#   group : 'root'"
echo "# -------------------------------------------"
echo "$(hostname)_filesystems:"
for line in $(grep ext4 /etc/fstab|grep -v '^#'|grep appvg| sed -e 's/[ ][ ]*/:/g' -e 's/[\t]/:/g');do
  # above replaces whitespace/tabs with colon for parsing
  line=$(echo "$line"|sed -e 's/:/ /g');set $line
  # parse it
  fqlv=$1;mp=$2;opts=$4;dump=$5;pass=$6
  name=$(echo ${mp}|sed -e 's#^/##' -e 's#/#_#g')
  vg="$(echo $fqlv|cut -f 3,3 -d\/)"
  lv="$(basename $fqlv)"
  sz=$(df -hP $fqlv|tail -1|awk '{print $2}')
  # correct for 'mapper'
  if [ $vg == 'mapper' ];then
    vg=$(echo $lv|cut -f 1,1 -d'-')
    lv=$(echo $lv|cut -f 2,2 -d'-')
  fi
  #
  echo "  ${name}:";
  echo "    vg      : '${vg}'"
  echo "    lv      : '${lv}'"
  echo "    sz      : '${sz}'"
  echo "    mp      : '${mp}'"
  echo "    ty      : 'ext4'"
  # --- get sticky bit too, as future puppet requires this
  echo "    md      : '$(stat ${mp}|awk '/^Access/ {print $2}'|head -1|cut -c 2-5)'"
  echo "    owner   : '$(ls -ld ${mp}| awk '{print $3}')'";
  echo "    group   : '$(ls -ld ${mp} | awk '{print $4}')'";
  echo "    #opts    : '${opts}'"
  echo "    #dump    : '${dump}'"
  echo "    #pass    : '${pass}'"
done 
