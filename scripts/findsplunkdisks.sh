#!/usr/bin/env /bin/bash
   
# Example output: "Disk /dev/sdb: 23040.6 GB...."
# Look for disks of a given size:
SIZE="23040.6 GB"

export PATH=$PATH:/usr/sbin:/usr/bin:/bin:/sbin
disks=$(fdisk -l 2>/dev/null | grep "^Disk./dev/s" | grep "$SIZE" | awk '{print $2}' | awk -F: '{print $1}')
if [[ $disks == "" ]]; then
   echo "No disks found." >&2
   exit 1
else
   echo $disks
fi
exit 0
