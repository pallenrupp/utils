#!/usr/bin/env bash

for env in stg uat prod
do    
  echo "env=$env"    
  while read -r line
  do  
    share=$(echo $line | awk '{print $1}')
    mount=$(echo $line | awk '{print $2}')
    echo "   share=$share"
    echo "   mount=$mount"
    for host in $(cat nfs_hosts.$env)
    do 
      echo "       host=$host"
    done
    echo ""
  done < nfs.$env
  echo ""
done
