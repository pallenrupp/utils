import sys
try:
    import argparse
    import csv
    import datetime
    import getpass
    import json
    import logging
    import re
    import requests
    import urllib3
    urllib3.disable_warnings()
except ImportError as e:
    print("Failed to import one or more required modules: {}".format(e))
    sys.exit(-1)


CVES_IN_SCOPE = [
    'cve-2015-2808',
    'cve-2013-2566',
    'cve-2011-3389',
    'cve-2014-3566',
    'cve-2014-0224',
    'cve-2010-5298',
    'cve-2015-0204',
    'cve-2012-4929',
    'cve-2016-0800',
    'cve-2015-2434',
    'cve-2015-2471',
    'cve-2015-6112',
    'cve-2015-1637',
    'cve-2015-7575',
    'cve-2015-4000',
    'cve-2014-0411',
    'cve-2014-2783',
    'cve-2014-8730',
    'cve-2015-4458',
    'cve-2013-0013',
    'cve-2005-2969',
    'cve-2013-4248',
    'cve-2009-3555',
    'cve-2008-1678',
    'cve-2016-0704',
    'cve-2014-3568',
    'cve-2014-3509',
    'cve-2015-1791',
    'cve-2015-3196',
    'cve-2016-0703',
    'cve-2014-3511',
    'cve-2015-0205',
    'cve-2014-3572',
    'cve-2013-6450',
    'cve-2014-8176',
    'cve-2013-6449',
    'cve-2013-4353',
    'cve-2016-0149',
]


def get_cache():
    cache_file = './rapid7_utils.cache'
    try:
        with open(cache_file, 'r') as fh:
            cache = json.load(fh)
    except FileNotFoundError:
        cache = {
            'vulnerability_attribute_mapping': {},
            'asset_id_mapping': {},
        }
    return cache


def update_cache(cache):
    cache_file = 'rapid7_utils.cache'
    with open(cache_file, 'w') as fh:
        json.dump(cache, fh)


def configure_logging(level):
    log_level = logging.getLevelName(level)
    logging.basicConfig(format='[%(levelname)s] %(message)s', level=log_level)


def get_args():
    description = ''
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument(
        '--log-level',
        required=False,
        dest='log_level',
        action='store',
        choices=[
            'DEBUG',
            'INFO',
            'WARNING',
            'ERROR',
            'CRITICAL',
        ],
        default='INFO',
        help='Set logging level.',
    )
    parser.add_argument(
        '--host',
        required=False,
        dest='host',
        action='store',
        nargs='+',
        help='',
    )
    parser.add_argument(
        '--show-severity',
        default=[],
        required=False,
        dest='show_severity',
        action='store',
        nargs='+',
        help='Only show CVEs with severity X. Supports multiple arguments',
    )
    parser.add_argument(
        '--filter-age',
        default=None,
        required=False,
        dest='filter_age',
        action='store',
        help='Only show CVEs older than X days.',
    )
    parser.add_argument(
        '--tls-only',
        required=False,
        dest='tls_only',
        action='store_true',
        help='',
    )
    parser.add_argument(
        '--outputtype',
        required=False,
        dest='output_type',
        action='store',
    )
    parser.add_argument(
        '--tofile',
        required=False,
        dest='tofile',
        action='store',
    )
    parser.add_argument(
        '--infile',
        required=False,
        dest='infile',
        action='store'
    )
    return parser.parse_args()


def get_url(endpoint):
    base_url = 'https://rapid7.transunion.com/api/3'
    return '{}/{}'.format(base_url, endpoint)


def get_headers():
    return {'content-type': 'application/json'}


def get_last_scan(
    hostname,
    asset_id,
    session,
):

    vulnerabilities = []
    params = {}
    endpoint = 'assets/{}'.format(asset_id)
    r = session.get(
        url=get_url(endpoint=endpoint),
        verify=False,
        params=params,
    )
    response = r.json()
    scans = []
    try:
        for event in response['history']:
            if event['type'] == 'SCAN':
                scans.append(event['date'])
    except KeyError:
        logging.warn(
            "No history found for host {}. HTTP status code: {}".format(
                hostname,
                r.status_code
            )
        )
        return None
    if not scans:
        logging.warn(
            "No scans found for host {}. HTTP status code: {}".format(
                hostname,
                r.status_code
            )
        )
    return scans[-1]


def get_asset_id(host, session):
    data = {
        'match': 'all',
        'filters': [
            {
                'field': 'host-name',
                'operator': 'is',
                'value': host,
            },
        ],
    }
    r = session.post(
        url=get_url(endpoint='assets/search'),
        data=json.dumps(data),
        verify=False,
    )
    try:
        response = r.json()
    except ValueError as err:
        if "No JSON object could be decoded" in err:
            logging.critical(
                "[{}] - is your Rapid7 account locked out?"
            ).format(err)
    else:
        try:
            if not response['resources']:
                return None
            elif len(response['resources']) > 1:
                msg = 'Using the first asset found for {}.'.format(
                    host
                )
                logging.warning(msg)
                return response['resources'][0]['id']
            else:
                return response['resources'][0]['id']
        except KeyError:
            return None


def get_vulnerabilities_by_asset(
    hostname,
    asset_id,
    session,
    size=100,
    page=0,
    tls_only=False,
    show_severity=[],
    filter_age=None,
):

    global cache, cache_hits, cache_misses

    vulnerabilities = []
    params = {
        'size': size,
        'page': page,
    }
    endpoint = 'assets/{}/vulnerabilities'.format(asset_id)
    r = session.get(
        url=get_url(endpoint=endpoint),
        verify=False,
        params=params,
    )
    response = r.json()
    try:
        resource_ids = [resource['id'] for resource in response['resources']]
    except KeyError:
        logging.warn(
            "No resources found for host {}. HTTP status code: {}".format(
                hostname,
                r.status_code
            )
        )
        return None
    for resource_id in resource_ids:
        for resource in response['resources']:
            if resource['id'] == resource_id:
                matches = 0
                for cve in CVES_IN_SCOPE:
                    if re.match('.*' + cve + '.*', resource_id):
                        matches += 1
                if matches > 0 or not tls_only:
                    for result in resource['results']:

                        severity, added = get_vulnerability_attributes(session, resource_id, ['severity', 'added'])

                        added_datetime_obj = datetime.datetime.strptime(added, '%Y-%m-%d')
                        added_age = datetime.datetime.now() - added_datetime_obj

                        if show_severity:
                            if severity.lower() not in show_severity:
                                continue

                        if filter_age:
                            if int(filter_age) > added_age.days:
                                continue

                        vuln_array = [
                            hostname,
                            severity,
                            added,
                            resource_id,
                            result['proof'].replace(
                                '<p>',
                                ''
                            ).replace(
                                '</p>',
                                ''
                            )
                        ]
                        vulnerabilities.append(
                            vuln_array
                        )
    page_info = response['page']
    last_page = int(page_info['totalPages']) - 1
    if page < last_page:
        next_page = page + 1
        vulnerabilities += get_vulnerabilities_by_asset(
                hostname=hostname,
                asset_id=asset_id,
                session=session,
                size=size,
                page=next_page,
                tls_only=tls_only,
                show_severity=show_severity,
                filter_age=filter_age,
            )
    return vulnerabilities


def get_vulnerabilities(session, size=500, page=0):
    vulnerabilities = []
    params = {
        'size': size,
        'page': page,
        'view': 'summary',
    }

    r = session.get(
        url=get_url(endpoint='vulnerabilities'),
        verify=False,
        params=params,
    )

    try:
        response = r.json()
    except ValueError as err:
        if "No JSON object could be decoded" in err:
            logging.critical("[{}] - Rapid7 account locked?").format(err)

    try:
        vulnerabilities = [
            resource['id'] for resource in response['resources']
        ]
    except KeyError:
        logging.critical("No resources found; nothing to do.")

    page_info = response['page']
    last_page = int(page_info['totalPages']) - 1
    if page < last_page:
        next_page = page + 1
        vulnerabilities += get_vulnerabilities(session, size=size, page=next_page)

    return vulnerabilities


def get_vulnerability_attributes(session, vulnerability_id, attributes):
    global cache, cache_hits, cache_misses

    missing_from_cache = []

    for attribute in attributes:
        try:
            cache_lookup = cache['vulnerability_attribute_mapping'][attribute]
        except KeyError:
            cache['vulnerability_attribute_mapping'][attribute] = {}
            cache_lookup = cache['vulnerability_attribute_mapping'][attribute]

        try:
            attr_value = cache_lookup[vulnerability_id]
            cache_hits += 1
        except KeyError:
            cache_misses += 1
            missing_from_cache.append(attribute)

    if missing_from_cache:
        cache_lookup = cache['vulnerability_attribute_mapping'][attribute]
        r = session.get(
            url=get_url(
                endpoint='vulnerabilities/{}'.format(vulnerability_id)
            ),
            verify=False,
        )
        try:
            response = r.json()
        except ValueError as err:
            if "No JSON object could be decoded" in err:
                logging.critical("[{}] - Rapid7 account locked out?").format(err)
                sys.exit(1)
        for attr in missing_from_cache:
            try:
                attr_value = response[attr]
            except KeyError:
                attr_value = None

            cache['vulnerability_attribute_mapping'][attr][vulnerability_id] = attr_value

    return [ cache['vulnerability_attribute_mapping'][attribute][vulnerability_id] for attribute in attributes ]


def main():
    if sys.version_info.major not in [3]:
        print("Error: This script must be run with Python 3.x")
        sys.exit(-1)

    global cache, cache_hits, cache_misses
    args = get_args()
    
    if args.host:
        pass
    elif args.infile:
        host = []
        with open(args.infile, 'r') as fh:
            # Strip newlines and split the strings as we read them from a file
            host = fh.read().strip().split('\n')
        # Set host to args.host to mantain compatibility in case were not using
        # input files
        args.host = host
    else:
        logging.critical("No hosts or input file specified.")
        sys.exit(1)

    if args.output_type:
        if not args.tofile:
            logging.critical("Output file missing.")
            sys.exit(1)
        if 'csv' in args.output_type:
            csv_file = args.tofile
        elif 'json' in args.output_type:
            json_file = args.tofile
        else:
            logging.critical("Unknown output type.")
            sys.exit(1)

    configure_logging(args.log_level)
    user = getpass.getuser()
    password = getpass.getpass()
    session = requests.Session()
    session.auth = (user, password)
    session.headers.update(get_headers())

    vuln_per_host = []
    HEADER_LIST=["SCAN_DATE","HOSTNAME","SEVERITY","DATE_PUBLISHED","CVE","PROOF"]
    vuln_per_host.append(HEADER_LIST)
    print(','.join(HEADER_LIST))
    for host in args.host:
        if host.isdigit():
            asset_id = host
        else:
            try:
                asset_id = cache['asset_id_mapping'][host]
                assert asset_id is not None
                cache_hits += 1
            except (KeyError, AssertionError):
                cache_misses += 1
                asset_id = get_asset_id(
                    host=host,
                    session=session,
                )
                if not asset_id:
                    logging.warn("Unable to find host {}.".format(
                        host
                    ))
                    continue
                cache['asset_id_mapping'][host] = asset_id
        scan_date = get_last_scan(hostname=host, asset_id=asset_id, session=session)
        scan_date_obj = datetime.datetime.strptime(scan_date,'%Y-%m-%dT%H:%M:%S.%fZ')
        formatted_scan_date = scan_date_obj.strftime('%Y-%m-%d %H:%M')
        if args.show_severity:
            vulnerabilities = get_vulnerabilities_by_asset(
                hostname=host,
                asset_id=asset_id,
                session=session,
                tls_only=args.tls_only,
                show_severity=[show_sev.lower() for show_sev in args.show_severity],
                filter_age=args.filter_age,
            )
        else:
            vulnerabilities = get_vulnerabilities_by_asset(
                hostname=host,
                asset_id=asset_id,
                session=session,
                tls_only=args.tls_only,
                filter_age=args.filter_age,
            )
        if vulnerabilities:
            for vulnerability in vulnerabilities:
                vulnerability.insert(0, formatted_scan_date)
                print(",".join(vulnerability))
                vuln_per_host.append(vulnerability)

    # Dump stuff into CSV
    if args.output_type:
        if 'csv' in args.output_type:
            myfile = open(csv_file, 'w')
            csv.register_dialect(
                'comma-delimited',
                delimiter=',',
                quoting=csv.QUOTE_NONE,
                escapechar='\\',
            )
            with myfile:
                writer = csv.writer(
                    myfile,
                    dialect='comma-delimited',
                    escapechar='\\',
                )
                writer.writerows(vuln_per_host)

    update_cache(cache)

    logging.debug("Cache hits: {}".format(str(cache_hits)))
    logging.debug("Cache misses: {}".format(str(cache_misses)))


cache = get_cache()
cache_hits = 0
cache_misses = 0
if __name__ == '__main__':
    main()
