#!/usr/bin/python
import argparse, crypt,os, socket, string, sys, time

PGM=sys.argv[0]
HOSTNAME=socket.gethostname()

def getparms(args=sys.argv):
  d="""
Generates a shadow password entry from <string>, and writes it to STDOUT.
"""

  p=argparse.ArgumentParser(description=d)
  p.add_argument("hash",metavar="<hash>",help="hash algorithm (SHA512,SHA256,MD5,CRYPT)")
  p.add_argument("astring",metavar="<string>",help="string to hash")
  c=p.parse_args(args[1:])
  # Additional checks, not easily handled by argparse()

  if (not c.hash.upper() in ("SHA512","SHA256","MD5","CRYPT")):
     p.error("Error: hash not valid.")

  return c 
   
def doit(c):
  
 
  #print "c=", c
  #print "c.hash=",c.hash
  #print "c.string=",c.astring
  if (c.hash.upper() == "SHA512"): method=crypt.METHOD_SHA512 
  if (c.hash.upper() == "SHA256"): method=crypt.METHOD_SHA256
  if (c.hash.upper() == "MD5"): method=crypt.METHOD_MD5
  if (c.hash.upper() == "CRYPT"): method=crypt.METHOD_CRYPT
  print crypt.crypt(c.astring, crypt.mksalt(method))
  sys.exit(0)

if (__name__=="__main__"):
  c=getparms()
  doit(c)
