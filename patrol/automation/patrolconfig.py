#!/usr/bin/python
##########################################################################################
# Name: patrolconfig.py
# Desc: 
#   This program allows you to update the BMC Agent installation configuration file 'install.ctl'
#   non-interactively by specifying what you want updated as a command line option.  Normally,
#   you update 'install.ctl' by running the interactive program 'ctltool.sh' and responding to
#   numerous prompts - manually. Instead, this program invokes 'ctltool.sh' on your behalf,
#   supplying the answers as if you had typed them in yourself! The ctltool.sh program can update
#   numerous configuration variables.  This program currently only supports a small subset of
#   configuration attributes, limited to any or all of the following:
#     1. Admin user (au)
#     2. Admin user password (ap)
#     3. Patrol user (pu)
#     4. Patrol user password (pp)
#   Located at the bottom of this program is a comment listing all the possible prompts
#   used by 'ctltool.sh', you may use them
# Input: 
#     1.  Needs path to BMC utility "ctltool.sh"
#     2.  Needs path to valid installation configuration file "install.ctl". 
#     3.  At least one of the configuration attrributes listed earlier 
#     All are provided via command-line options
# Output:
#     1.  Path to a new 'install.ctl file.  The new install.ctl file contains
#         the attributes you specified on the command line.
#
# Logic:
#     1. Validate command line arguments
#     2. Use "pexpect" module to invoke the "ctltool.sh" program.
#        a. Instruct pexpect to look for certain prompts (the four we are interested in)
#        b. Loop through the prompts.  If we recognize a prompt we are looking for,
#           provide the required answer. Otherwise, logically press <enter> to cause
#           ctltool.sh to give another prompt. When there is no more output (typically
#           when "ctltool.sh" exits).  We return zero (0) if there were no errors. 
#           otherwise, return one (1) if we discover ctltool.sh fails, or we never saw
#           the prompts for the atttributes we were looking for. 
#     Done 
#
# Change History:
#     3/17/2017 - P. Rupp - Initial Implementation
##########################################################################################

import argparse,pexpect,os,platform,socket,string,sys,time

PGM=os.path.basename(sys.argv[0])
HOSTNAME=socket.gethostname()


def getparms(args=sys.argv):
  

  d="""
This program provides an easy way to modify certain attributes within BMC patrol
agent install control file 'install.ctl' from the command line. Currently, the
program allows you to change the following attributes:
 - the admin user and/or password
 - the patrol user and/or password
(The program can be easily modified to allow other attributes)
"""
  p=argparse.ArgumentParser(description=d)
  p.add_argument('-ctltool',nargs=1,required=True,help="path to bmc utility ctltool.sh (in distribution)",metavar="<path>",dest="ctltool")
  p.add_argument('-ctlfile',nargs=1,required=True,help="path/to/install.ctl",metavar="<path>",dest="ctlfile")
  p.add_argument('-au',nargs=1,required=False,help="admin user",metavar="<admin user>",dest="au")
  p.add_argument('-ap',nargs=1,required=False,help="admin passwd",metavar="<admin passwd>",dest="ap")
  p.add_argument('-pu',nargs=1,required=False,help="patrol user",metavar="<patrol user>",dest="pu")
  p.add_argument('-pp',nargs=1,required=False,help="patrol passwd",metavar="<patrol passwd>",dest="pp")
  p.add_argument('-o',nargs=1,required=True,help="output file",metavar="<output file>",dest="o")
  #
  # create "c" as a "Namespace" Object.  Just a shell object to hold variables
  # i.e, "class c():

  c=p.parse_args(args[1:])

  # At least one -au, -ap, -pu, -pp options must be selected..
  if (c.au == None and c.ap == None and c.pu == None and c.pp == None):
     p.error('Must select at least one of -au, -ap, -pu -pp options.') 

  if (c.ctltool and not os.path.isfile(c.ctltool[0])):
      p.error("-d %s does not exist."%(c.ctltool[0])) 

  if (c.ctlfile and not os.path.isfile(c.ctlfile[0])):
      p.error("-d %s does not exist."%(c.ctlfile[0])) 

  if (os.path.abspath(c.ctlfile[0]) == os.path.abspath(c.o[0])):
      p.error("-ctlfile and -o cannot be the same.  Change -o <path> to something different.")

  #print "c.ctltool=", c.ctltool
  #print "c.ctlfile=", c.ctlfile
  #print "c.o=", c.o
  #print "c.au", c.au
  print "c.ap", c.ap
  #print "c.pu", c.pu
  print "c.pp", c.pp
 
  return c 
   
def log(fac="INFO",msg=""):
   TIMESTAMP=time.strftime("%Y.%m.%d.%H:%M:%S", time.localtime())
   msg=msg.strip()
   if msg == "": # don't print blank messages
      pass
   else:
      # where fac should be "INFO", "WARN", "FAIL", or "ABND"
      print "%s %s [%s] %s"%(TIMESTAMP, PGM, fac, msg)

def doit(c):

  CTLTOOLPATH=os.path.abspath(c.ctltool[0])
  CTLTOOLDIR=os.path.dirname(c.ctltool[0])
  CTLTOOLBASE=os.path.basename(c.ctltool[0])
  CTLFILEPATH=os.path.abspath(c.ctlfile[0])
  OPATH=os.path.abspath(c.o[0])
  sub="./%s %s %s"%(CTLTOOLBASE, CTLFILEPATH, OPATH)
  
  log(msg="[start]")
  log(msg="options:")
  log(msg="  c.ctltool=%s"%(c.ctltool))
  log(msg="  c.ctlfile=%s"%(c.ctlfile))
  log(msg="  c.au=%s"%(c.au))
  log(msg="  c.ap=%s"%(c.ap))
  log(msg="  c.pu=%s"%(c.pu))
  log(msg="  c.pp=%s"%(c.pp))
  log(msg="  c.o=%s"%(c.o))
  
  log(msg="Invoking->%s"%(sub))

  CWDSAVE=os.getcwd()
  os.chdir(CTLTOOLDIR)

  #
  # Build list with all possible prompts you may look for
  #
    
  haystack=[
    pexpect.EOF,
    pexpect.TIMEOUT,
    # Start of all possible prompts from subprogram (in order)
    # -pu
    '\[SETP\]\/ACCOUNT\/Quotedlogin',
    # -pp
    '\[SETP\]\/ACCOUNT\/passwd',
    '\[SETP\]\/BMC\/CONFIRMATION\/PASSWORD',
    # -au
    '\[SETP\]\/root\/login',
    # -ap
    '\[SETP\]\/root\/passwd',
    '\[SETP\]\/ROOT\/CONFIRMATION\/PASSWORD',
    # program failure - common errors messages..
    #  "Failed to create output file <bla>"
    #  "Failed to open input file <bla>"
    #  and so on...
    'Failed to',
    ]

  try:
    p=pexpect.spawn(sub,timeout=.05)
  except Exception,e:
    print "Exception!", e
    sys.exit(1)

  log(msg="Looking for prompts...")

  rc=0
  auc=0 # au count 
  apc=0 # ap count
  puc=0 # pu count
  ppc=0 # pp count

  while True: 
    needle=p.expect(haystack)
    # To print data emitted from sub program
    # use <expect object>.before

    if ( needle == 0 ):
      log("INFO","Found-> EOF! Program exited.")
      log("INFO","Response-> None. Exit.")
      rc=0
      break 
    elif ( needle == 1 ):
      p.sendline()
      continue
    elif ( needle == 2 ):
      if c.pu:
         work=c.pu[0]
         log("INFO","Found-> patrol user prompt/"+haystack[2])
         log("INFO","Response-> "+work)
         puc+=1
         p.sendline(work)
      else:
         pass # ignore
    elif ( needle == 3):
      if c.pp:
         work=c.pp[0]
         log("INFO","Found-> patrol user prompt/"+haystack[3])
         log("INFO","Response-> "+work)
         ppc+=1
         p.sendline(work)
      else:
         pass # ignore
    elif ( needle == 4):
      if c.pp:
         work=c.pp[0]
         log("INFO","Found-> patrol password prompt/"+haystack[4])
         log("INFO","Response-> "+work)
         ppc+=1
         p.sendline(work)
      else:
         pass # ignore
    elif ( needle == 5 ):
      if c.au:
         work=c.au[0]
         log("INFO","Found-> admin user prompt/"+haystack[5])
         log("INFO","Response-> "+work)
         auc+=1
         p.sendline(work)
      else:
         pass # ignore
    elif ( needle == 6):
      if c.ap:
         work=c.ap[0]
         log("INFO","Found-> admin password prompt/"+haystack[6])
         log("INFO","Response-> "+work)
         apc+=1
         p.sendline(work)
      else:
         pass # ignore
    elif ( needle == 7):
      if c.ap:
         work=c.ap[0]
         log("INFO","Found-> admin password pprompt/"+haystack[7])
         log("INFO","Response-> "+work)
         apc+=1
         p.sendline(work)
      else:
         pass # ignore
    elif ( needle == 8 ):
      log("FAIL","Found-> Subprogram Error Message (fail)/"+sub)
      log("INFO","Response-> None. Abort!")
      rc=1
      break 
    elif ( needle == 9 ):
      p.sendline() 
    else:
      pass

  if c.au:
     if auc != 1:
       log("ERROR","Unexpected count for AU! Expected 1, received "+str(auc)) 
       rc=1 
  if c.ap:
     if apc != 2:
       log("ERROR","Unexpected count for AP! Expected 2, received "+str(apc))
       rc=1 
  if c.pu:
     if puc != 1:
       log("ERROR","Unexpected count for PU! Expected 1, received "+str(puc))
       rc=1 
  if c.pp:
     if ppc != 2:
       log("ERROR","Unexpected count for PP! Expected 2, received "+str(ppc))
       rc=1 
     
  os.chdir(CWDSAVE)
  p.close()
  sys.exit(rc)

if (__name__=="__main__"):
   try:
      c=getparms()
      doit(c)
   except KeyboardInterrupt, e:
      log("INFO","Aborted...")
      sys.exit(1)


#
# Below 
"""

   [SETP]/BMC/PATROL/RELEASEVERSION = 7.4
   Enter new value of hit RETURN key to preserve old value :  
   [SETP]/BMC/PATROL7/DIR = Patrol7
   Enter new value of hit RETURN key to preserve old value :  
   [SETP]/NP/BMC/CI/INSTALL_MODE = EXPORT
   Enter new value of hit RETURN key to preserve old value :  
   [SETP]/BMC/PATROL/BEM/EXISTS = FALSE
   Enter new value of hit RETURN key to preserve old value :  
   [SETP]/BMC/Agent = true
   Enter new value of hit RETURN key to preserve old value :  
   [SETP]/BMC/PERFORM/UNIX_AGENT = Requested
   Enter new value of hit RETURN key to preserve old value :  
   [SETP]/BMC/PERFORM/LINKBEST1DEFAULT = y
   Enter new value of hit RETURN key to preserve old value :  
   [SETP]/BMC/COMMON/DIR = common
   Enter new value of hit RETURN key to preserve old value :  
   [SETP]/BMC/PERFORM/MONITORPORT = 6768
   Enter new value of hit RETURN key to preserve old value :  
   [SETP]/BMC/UNINSTALL/DIR = Uninstall
   Enter new value of hit RETURN key to preserve old value :  
   [SETP]/BMC/AGENT/STOP = TRUE
   Enter new value of hit RETURN key to preserve old value :  
   [SETP]/BMC/AGENT/PORT = 3181
   Enter new value of hit RETURN key to preserve old value :  
   [SETP]/prfinfo/dummy1 = 
   Enter new value of hit RETURN key to preserve old value :  
   [SETP]/prfinfo/dummy0 = 
   Enter new value of hit RETURN key to preserve old value :  
   [SETP]/BMC/PERFORM/STARTAGENT = y
   Enter new value of hit RETURN key to preserve old value :  
   [SETP]/prfinfo/dummy2 = 
   Enter new value of hit RETURN key to preserve old value :  
   [SETP]/BMC/PATROL/INTEGRATIONSERVICES/VARIABLE = tcp:170.118.32.243:3183,tcp:170.118.32.244:3183
   Enter new value of hit RETURN key to preserve old value :  
   [SETP]/BMC/KMDS = FALSE
   Enter new value of hit RETURN key to preserve old value :  
   [SETP]/BMC/CONFIRMATION/PASSWORD = $-2$-$61940599032502D41DFAD84602626696
   Enter new password (will be encrypted)
   or hit RETURN key to preserve old value :  
   [SETP]/NP/BMC/MAYBE = TRUE
   Enter new value of hit RETURN key to preserve old value :  
   [SETP]/BMC/SECURITY/NETWORKCONNECTION = TCP
   Enter new value of hit RETURN key to preserve old value :  
   [SETP]/BMC/PATROL3/INFO_2 = /opt/patrol
   Enter new value of hit RETURN key to preserve old value :  
   [SETP]/BMC/PATROL7/INFO_1 = 
   Enter new value of hit RETURN key to preserve old value :  
   [SETP]/BMC/PERFORM/INTEROP = Configure
   Enter new value of hit RETURN key to preserve old value :  
   [SETP]/NP/BMC/CI/INSTALL_TYPE = CUSTOM
   Enter new value of hit RETURN key to preserve old value :  
   [SETP]/BMC/BGSSD/PORT = 10128
   Enter new value of hit RETURN key to preserve old value :  
   [SETP]/ENCRYPTION/AES_ENABLED = TRUE
   Enter new value of hit RETURN key to preserve old value :  
   [SETP]/Blank/BMC/Password/Continue = FALSE
   Enter new value of hit RETURN key to preserve old value :  
   [SETP]/BMC/PATROL/CENTRALADMIN/TAG/CFG = Allow
   Enter new value of hit RETURN key to preserve old value :  
   [SETP]/BMC/PATROL/BEM/FORWARDEVENTTOIS = yes
   Enter new value of hit RETURN key to preserve old value :  
   [SETP]/BMC/PWC/DIR = webcentral
   Enter new value of hit RETURN key to preserve old value :  
   [SETP]/BMC/PERFORM/TOFFHIST = y
   Enter new value of hit RETURN key to preserve old value :  
   [SETP]/root/passwd = $-2$-$2A573280511F2EF2065613BE2C8650E1
   Enter new password (will be encrypted)
   or hit RETURN key to preserve old value :  
   [SETP]/BMC/PATROL/CENTRALADMIN/TAG = Universal_Linux:"Linux agent from universal package"
   Enter new value of hit RETURN key to preserve old value :  
   [SETP]/BMC/PERFORM/AGENTU = TRUE
   Enter new value of hit RETURN key to preserve old value :  
   [SETP]/BMC/INSTBASE/NOSLASH = /opt/patrol
   Enter new value of hit RETURN key to preserve old value :  
   [SETP]/BMC/BASE = /opt/patrol
   Enter new value of hit RETURN key to preserve old value :  
   [SETP]/BMC/PERFORM/UNIX_COMMANDLENGTH = 2048
   Enter new value of hit RETURN key to preserve old value :  
   [SETP]/Blank/Root/Password/Continue = TRUE
   Enter new value of hit RETURN key to preserve old value :  
   [SETP]/BMC/INSTBASE = /opt/patrol/
   Enter new value of hit RETURN key to preserve old value :  
   [SETP]/BMC/PATROL/RTSERVERS/EXISTS = FALSE
   Enter new value of hit RETURN key to preserve old value :  
   [SETP]/ACCOUNT/Quotedlogin = patrol
   Enter new value of hit RETURN key to preserve old value :  
   [SETP]/BMC/AGENT/START = TRUE
   Enter new value of hit RETURN key to preserve old value :  
   [SETP]/BMC/PERFORM/CONFIG = TRUE
   Enter new value of hit RETURN key to preserve old value :  
   [SETP]/BMC/PATROL/BEM/PRIMARY = 
   Enter new value of hit RETURN key to preserve old value :  
   [SETP]/BMC/SECURITY/ADVANCED = Basic
   Enter new value of hit RETURN key to preserve old value :  
   [SETP]/BMC/PERFORM/REMPERF = Uninstall
   Enter new value of hit RETURN key to preserve old value :  
   [SETP]/root/login = root
   Enter new value of hit RETURN key to preserve old value :  
   [SETP]/Repository_Tests/Front_End/Install_Dir_Flag = 1
   Enter new value of hit RETURN key to preserve old value :  
   [SETP]/BMC/SECURITY/LEVEL = 0
   Enter new value of hit RETURN key to preserve old value :  
   [SETP]/BMC/PERFORM/PAT_DIR = /opt/patrol/Patrol3
   Enter new value of hit RETURN key to preserve old value :  
   [SETP]/ACCOUNT/passwd = $-2$-$61940599032502D41DFAD84602626696
   Enter new password (will be encrypted)
   or hit RETURN key to preserve old value :  
   [SETP]/BMC/PERFORM/SDDIR = /etc/bgs/SD
   Enter new value of hit RETURN key to preserve old value :  
   [SETP]/BMC/PATROL/DIR = Patrol3
   Enter new value of hit RETURN key to preserve old value :  
   [SETP]/BMC/PATROL/INTEGRATIONSERVICES/EXISTS = TRUE
   Enter new value of hit RETURN key to preserve old value :  
   [SETP]/BMC/PATROL/RTSERVERS/VARIABLE = tcp:localhost:2059
   Enter new value of hit RETURN key to preserve old value :  
   [SETP]/BMC/PWC/MIGRATE = webcentral.bak
   Enter new value of hit RETURN key to preserve old value :  
   [SETP]/BMC/PERFORM/HISTORY = /opt/patrol/perform/history
   Enter new value of hit RETURN key to preserve old value :  
   [SETP]/BMC/SECURITY/OVERWRITE = TRUE
   Enter new value of hit RETURN key to preserve old value :  
   [SETP]/BMC/TEST/PASSWORD_GOOD = 1
   Enter new value of hit RETURN key to preserve old value :  
   [SETP]/ROOT/BLANK/PASSWORD_CHECK = 1
   Enter new value of hit RETURN key to preserve old value :  
   [SETP]/BMC/PATROL/CENTRALADMIN/TAG/EXISTS = TRUE
   Enter new value of hit RETURN key to preserve old value :  
   [SETP]/BMC/PERFORM/UNIX_SERVICESETUP = False
   Enter new value of hit RETURN key to preserve old value :  
   [SETP]/BMC/PERFORM/AGENTPORT = 6767
   Enter new value of hit RETURN key to preserve old value :  
   [SETP]/BMC/PERFORM/KDC = TRUE
   Enter new value of hit RETURN key to preserve old value :  
   [SETP]/ROOT/TEST/PASSWORD_GOOD = 1
   Enter new value of hit RETURN key to preserve old value :  
   [SETP]/BMC/PATROL/BEM/SHARED_KEY = mc
   Enter new value of hit RETURN key to preserve old value :  
   [SETP]/Repository_Tests/Front_End/Install_Dir = /opt/patrol
   Enter new value of hit RETURN key to preserve old value :  
   [SETP]/BMC/PERFORM/COLLECT = /opt/patrol/perform/collect
   Enter new value of hit RETURN key to preserve old value :  
   [SETP]/ROOT/CONFIRMATION/PASSWORD = $-2$-$2A573280511F2EF2065613BE2C8650E1
   Enter new password (will be encrypted)
   or hit RETURN key to preserve old value :  
   [SETP]/BMC/CIAgent = TRUE
   Enter new value of hit RETURN key to preserve old value :  
   [SETP]/BMC/PERFORM/MIGRATE = Migrate
   Enter new value of hit RETURN key to preserve old value :  
   [SETP]/BMC/PATROL/BEM/SECONDARY = 
   Enter new value of hit RETURN key to preserve old value :  
"""
