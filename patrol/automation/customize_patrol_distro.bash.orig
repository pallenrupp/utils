#!/usr/bin/bash

# Program to generate automation configuration files for
# Patrol installation via Ansible
#
# P. Rupp/15 Feb 2017 - Initial implementation
#
# Input(s):
#   1. secrets.csv - file containing installation root/patrol passwords
# Outputs:
#   1. install.ctl - A modified version of BMC's Agent 'install.ctl' file
#                    customized with customer's root/patrol passwords.   
#                    renamed as <CUST>_install.ctl
#   2. <cust>_shadowhash_[root|patrol] - An /etc/shadow password entry for
#                    the installation's root and patrol user accounts.

PGM=$(basename $0)
PATH=$PATH:`pwd`:/opt/depot/patrol/automation;export PATH

usage() {
   echo "Usage: $PGM -s <secrets> -d <patrol_distro> -o <output dir>"
   echo "       $PGM -h(elp)" 
}
help() {
   echo "$PGM customizes the *linux* BMC patrol agent distribution by adding additional"
   echo "'install.ctl' files, each customized with unique passwords used in the target"
   echo "environments.  Additionally, linux shadow file hashes are generated and included"
   echo "as a convenience to allow the administrator to set both the OS and patrol"
   echo "passwords to match those set in the custom 'install.ctl' file(s)."
   echo "To use this program, you provide 3 arguments - the path to a 'secrets.csv'"
   echo "file (An encrypted .csv file containing the OS and patrol administrative users and passwords),"
   echo "the path to patrol installation media, which may be a directory, or a tar, tar.gz or tar.bz2 file,"
   echo "and the path to the output directory."
   echo ""
   echo "Options:"
   echo " -s <secrets>: an encrypted csv file, each line contains a customer 'tag', and"
   echo "               unique root and patrol passwords.  This information is used to"
   echo "               customize the 'install.ctl' file per client. The file is encrypted "
   echo "               using 'ansible-vault encrypt'.  For more information, see README.secrets."  
   echo " -d <distro>:  The BMC patrol distribution as a file (tar, tar.bz2 or tar.gz), or a"
   echo "               directory"
   echo ""
   echo " -o <output>:  Customized BMC Patrol directory (to hold results.)"
   echo ""
}

log() {
  TIMESTAMP=$(date "+%Y.%d.%m.%T")
  if [[ $# -gt 0 ]]; then
     P="[INFO]"
     case $1 in
     info|INFO) P="[INFO]";shift;;
     warn|WARN) P="[WARN]";shift;;
     error|ERROR) P="[ERROR]";shift;;
     fail|FAIL) P="[FAIL]";shift;;
     ok|OK) P="[OK]";shift;;
     esac
     printf "%20s %s %-6s %s\n" ${TIMESTAMP} ${PGM} "$P $@"
  fi
}

confirm() {
   msg="$@"
   c=11
   l=10
   while true
   do
      if [[ $c -gt l ]]; then
          printf "$@"
          c=0
      fi
      c=$((c+1))
      printf "y|n|q?\n"
      read ans
      case $ans in
         y|Y)
            return 0
            ;;
         n|n)
            return 1
            ;;
         q|Q)
            echo "Exiting..."
            sync
            exit 1
            ;;
      esac
   done
}


#
# input parms
#
SECRETSFILE=""
DISTRO=""
OUTDIR=""
declare -a SECRETSARRAY
validate_parms() {

while getopts "hs:d:o:" myopt
do
   case "$myopt" in
   h) # help
      usage
      echo ""
      help
      exit 1
      ;;
   s) # secrets file
     SECRETSFILE=$(realpath $OPTARG);;
   d) # Distro file or directory
     DISTRO=$(realpath $OPTARG);;
   o) # Output dir 
     OUTDIR=$OPTARG;;
   [?])
     echo ""
     usage
     exit 1
     ;;
   esac
done

if [[ $SECRETSFILE == "" ]]; then
  echo "Error: -s <secrets> is required.";
  echo ""
  usage
  exit 1
elif [[ ! -f $SECRETSFILE ]]; then
  echo "Error: -s <secrets> is not a file."
  echo ""
  usage
  exit 1
fi
if [[ $DISTRO == "" ]]; then
   echo "Error: -d <distro> is required."
   echo ""
   usage
   exit 1
elif [[ -f $DISTRO ]];
then
   if tar -tf $DISTRO >/dev/null 2>&1 || tar -tzf $DISTRO >/dev/null 2>&1 || tar -tjf $DISTRO >/dev/null 2>&1
   then
       echo "INFO: distro file $DISTRO is a valid tar, tar.gz, or tar.bz2 file.  Continuing...."
   else
       echo "Error: distro file $DISTRO is not a valid tar, tar.gz, or tar.bz2 file.  Exiting."
       exit 1
   fi
elif [[ -d $DISTRO  ]]; then
  :
else
  echo "Error: distro file $DISTRO is not a file or directory.  Please check."
  exit 1
fi
if [[ $OUTDIR == "" ]]; then
   echo "Error: -o <output directory> is required."
   echo ""
   usage
   exit 1
fi


echo "OUTDIR=$OUTDIR"
}


prep_secrets() {
#
# Load and validate secrets into "SECRETSARRAY" bash array.
#

   log "Loading secrets file $SECRETSFILE..."
   j=0
   while read -r i
   do
      if [[ "$i" != "" ]]; then
         if ! a=$(echo "$i"|grep -q '^[[:space:]]*#'); then
            if [[ $(echo $i|awk -F, '{print NF}') -eq 5 ]]; then
               SECRETSARRAY[$j]=$i
               j=$((j+1))
            else
               log "Warning: invalid secret found ($i). Discarding."
            fi
         fi
      fi
   done < <(cat $SECRETSFILE|ansible-vault decrypt)
   if [[ $j -eq 0 ]]; then
      log  "error" "No secrets found. Exiting."
      exit 1
   else
      log "info" "Secrets file loaded Ok - ${#SECRETSARRAY[@]} elements loaded.",
   fi
 
}

prep_distro() {
   OUTDIR=$(realpath $OUTDIR)
   log "Preparing output directory $OUTDIR"
   if [[ -e $OUTDIR ]]; then
      if confirm $'Warn: Output directory $OUTDIR  exists!\n Do you want me to remove it?\n';then
         log "Removing $OUTDIR..."
         if rm -rf $OUTDIR; then
            echo "Remove successful!"
         else
            echo "Remove FAILED."
            echo "Ok, please back it up or remove it before running this program." 
            log "Exiting..."
            exit 1
         fi 
      else
         echo "Ok, please move or remove it before running this program."
         log "Exiting..."
         exit 1
      fi
   fi 
   log "Creating $OUTDIR"
   if mkdir -p $OUTDIR; then
      log "OK."
   else
      log "fail" "Could not create output dirrectory! Exiting.."
      exit 1
   fi
     
   log "Copying distro to output directory..."

   if [[ -d  $DISTRO ]]; then
      log "Copying $DISTRO to  $OUTDIR"
      if cp -pr $DISTRO $OUTDIR; then
          log "Copying is successful. Continuing."   
      else
         log error "Could not create output directory $OUTDIR. Exiting."
         exit 1
      fi
   elif [[ -f $DISTRO ]]; then
      log "Copying $DISTRO to $OUTDIR"
      cd $OUTDIR
      if tar -xf $DISTRO || tar -xzf $DISTRO || tar -xjf $DISTRO; then
         log "Copying is successful. Continuing."
      else 
         log error "Copy failed for $OUTDIR. Exiting."
         exit 1
      fi
   fi
}
  
update_distro () {
#
# handles one (1) tag line from the secrets.csv file 
#
# for each line in SECRETSARRAY, split apart and perform
# creation of shadowhash and customm install.ctl files
#

  log "Locate critical Patrol files..."
  INSTALL_CTL="install.ctl"
  log "locate $INSTALL_CTL"
  INSTALL_CTL_FOUND=$(find $OUTDIR -name $INSTALL_CTL | head -1)
  if [[ $INSTALL_CTL_FOUND == "" ]]; then
     log error "Could not locate $INSTALL_CTL in Patrol distro (FATAL). Existing."
     exit 1
  else
     log "OK.  Found $INSTALL_CTL_FOUND" 
  fi

  CTLTOOL_SH="ctltool.sh"
  log "Locate $CTLTOOL_SH"
  CTLTOOL_SH_FOUND=$(find $OUTDIR -name $CTLTOOL_SH | head -1)
  if [[ $CTLTOOL_SH_FOUND == "" ]]; then
     log fail "Could not locate $CTLTOOL_SH in Patrol distro (FATAL). Exiting."
     exit 1
  else 
     log "OK.  Found $CTLTOOL_SH_FOUND" 
  fi

  CTLDIR=$(dirname $INSTALL_CTL_FOUND)/custom
  log "Creating custom directory: $CTLDIR"
  mkdir -p $CTLDIR

  log "Create custom $INSTALL_CTL files..."
  j=0
  while [[ $j -lt ${#SECRETSARRAY[@]} ]];  
  do
     i=${SECRETSARRAY[$j]} 
     tag=$(echo $i | awk -F, '{print $1}') 
     au=$(echo $i | awk -F, '{print $2}') 
     ap=$(echo $i | awk -F, '{print $3}') 
     pu=$(echo $i | awk -F, '{print $4}') 
     pp=$(echo $i | awk -F, '{print $5}') 

     NEWCTLFILE="${CTLDIR}/install.ctl.${tag}"
     log "Generate install.ctl: $NEWCTLFILE" 
     cmd="patrolconfig.py -o ${NEWCTLFILE} -ctltool ${CTLTOOL_SH_FOUND} -ctlfile ${INSTALL_CTL_FOUND} -pu '$pu' -pp '$pp' -au '$au' -ap '$ap'"
     log "CMD=$cmd"
     if eval "$cmd" >/dev/null; then
        log "Ok. RC=$?"
     else
        log error "Fail.  RC=$?"
     fi
     j=$((j+1))
  done

  log "Create custom SHADOW files..."
  j=0
  while [[ $j -lt ${#SECRETSARRAY[@]} ]];  
  do
     i=${SECRETSARRAY[$j]} 
     tag=$(echo $i | awk -F, '{print $1}') 
     au=$(echo $i | awk -F, '{print $2}') 
     ap=$(echo $i | awk -F, '{print $3}') 
     pu=$(echo $i | awk -F, '{print $4}') 
     pp=$(echo $i | awk -F, '{print $5}') 

     
     for algo in SHA512 SHA256 MD5 CRYPT
     do
        # Create Admin User hash
        user=$au
        pass="'$ap'"
        NEWHASHFILE="${CTLDIR}/shadowhash.${tag}.${user}.${algo}"
        log "Creating HASH file: $NEWHASHFILE"
        cmd="shadowhash.py $algo $pass > ${NEWHASHFILE}"
        log "CMD=$cmd"
        eval "$cmd" > /dev/null  

        # Create Patrol User hash
        user=$pu
        pass="'$pp'"
        NEWHASHFILE="${CTLDIR}/shadowhash.${tag}.${user}.${algo}"
        log "Creating HASH file: $NEWHASHFILE"
        cmd="shadowhash.py $algo $pass > ${NEWHASHFILE}"
        log "CMD=$cmd"
        eval "$cmd" > /dev/null  

     done
     j=$((j+1))
  done
}

validate_parms "$@"
prep_secrets $SECRETSFILE $DISTRO $WORKDIR $PREFIX
prep_distro  $SECRETSFILE $DISTRO $WORKDIR $PREFIX
update_distro  $SECRETSFILE $DISTRO $WORKDIR $PREFIX
log "Done"
