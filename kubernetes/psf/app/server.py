#!/usr/bin/env python3

import fcntl
import os
import signal
import socket
import sys
import threading
import time

def exit_parent(signalnum, handler):
    print('Parent exiting...', os.getpid())
    sys.exit(0)

def child_main(mysocketob):

    print("Currently active threads=", threading.activeCount())

  # If signal interecepted within child, just exit
  # (Signals caught in parent differ by killing all children)

    mysocketob.send(b'What do you want?\n')
    while True:
       data=os.read(mysocketob.fileno(),4096).strip()
       datastr=data.decode('ascii')

       if len(data)==0:
          break
       print(threading.get_ident(),"<data>")
       print(data)
       print(threading.get_ident(), "</data>")

    mysocketob.close()
    print("exiting thread %s"%(threading.get_ident(),))


   def handle(self):
       print("GOT into handle()")
       PID = threading.current_thread()
       bytes=self.request.recv(4096).strip()
       chars=bytes.decode('ascii')

       #while (chars != ""):
       while True:
          print("Start of loop")
          ds=chars.split(None,1)
          print("ds=", ds)
          if (( len(ds) > 1 ) and (ds[0].lower() == "shell")):
             print("User wants to run shell...")
             s=petelib.shell(str(ds[1]).strip())
             s.run()
             print("PID=%s SHELL=%s"%(PID,ds[1]))
             print("rc=%s"%(s.rc,))
             print("<stdout>")
             print("%s"%(s.stdout,))
             print("</stdout>")
             print("<stderr>")
             print("%s"%(s.stderr,))
             print("</stderr>")

             out=s.stdout.encode()
             self.request.sendall(out)
          else:
             self.request.sendall(bytes)

          bytes=self.request.recv(4096).strip()
          chars=bytes.decode('ascii')



      
def listener_main(host,port):

    print("host=", host)
    print("port=", port)
    # Responsible for creating a listening socket, and invoking new threads
    # connecting to it.  I do this away from the parent because I want the
    # parent to be able to build other listenrs, and perform house-keeping
    # work while everything else is running :-) 
    # Future renditions will probably take a parm to indicate the port # and
    # host specs (obtained from a config file, or dynamically reconfigured
    # on request from a controlling console.

    try:
       # Create socket
       socketob=socket.socket(socket.AF_INET,socket.SOCK_STREAM)
       socketob.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
       socketob.bind((host, port))
       socketob.listen(10)
       while (1):
          mysocketob, saddr = socketob.accept() 
          mytuple=(mysocketob,)
          # Create a new thread to operate on newly accepted socket connection 
          mythread=threading.Thread(group=None, target=child_main, name="client-"+str(saddr), args=mytuple, kwargs={})
          mythread.start()
          print(os.getpid(), "Created child thread")

    except Exception as e:
       print(e)

def main(argv):

    # Setup signal handlers
    signal.signal(signal.SIGINT,exit_parent)
    signal.signal(signal.SIGTERM,exit_parent)

    host=None
    port=None
    
    if (len(sys.argv) == 2):
       host = ""
       port = sys.argv[1]
    elif (len(sys.argv) == 3):
       host = sys.argv[1]
       port = sys.argv[2]
    else:
       print("Usage: %s [ <host> ] <port#>")
       return  
    port = int(port)

    lt_tuple1=(host,port)
    lt1=threading.Thread(group=None, target=listener_main, name="listner_main", args=lt_tuple1, kwargs={})
    lt1.start()
    print("Bye!")

if (__name__ == '__main__'):
   try:
      sys.exit(main(sys.argv))
   except Exception as e:
      print(e)
