#!/usr/bin/env python
import sys, fcntl, os, time, signal, socket, select, string

lock_fd   =-1
lock_file ='/tmp/schedlock'+str(os.getpid())

def munge_data(data):

    header_len=10
    header=string.zfill(header_len+len(data),header_len) 
    return header+data

def main():

    host=None
    port=None
    
    if (len(sys.argv) < 4):
       print "Usage:", sys.argv[0], "<host> <port#> <data>"
       sys.exit(-1)

    host=sys.argv[1]
    port=sys.argv[2]
    data=string.join(sys.argv[3:])


    try:
       port=int(port)
    except:
       print "Error: <port#> is not an integer"
       print "Usage:", sys.argv[0], "<host> <port#> <data>"
       sys.exit(-1)

    # Create socket
    socketob=socket.socket(socket.AF_INET,socket.SOCK_STREAM)
    socketob.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    try:
       socketob.connect((host,port))
    except:
       print "Error: Cannot connect to PSF controller"
       print "PSF controller may be down, or your using the wrong host or port."
       sys.exit(-1)
     
    print "Sending data:", data
    socketob.send(data)
    socketob.shutdown(socket.SHUT_WR)
    #result=socketob.recv(4096)
    result=socketob.recv(1024)
    print "Receiving data:", result
    print "Closing connection......"

    del socketob

if (__name__ == '__main__'):
   main()
