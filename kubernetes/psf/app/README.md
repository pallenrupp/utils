
1. server.py  -  a simple multi-process "echo" server. It listens on a port
                 then pre-starts a fixed number of processes to handle incoming
                 connections.  

2. client.py - a simple client program to connect to server's host and socket
               and send some data

3. petelib.py  - a utility library of misc python classes and scripts to make
                 things easier.  Think "shell" class.
                

TL;DR

# server listens on all interfaces (0.0.0.0) on port 5662, and starts 10 processes 
python server.py 0.0.0.0 5662 10

# to send and echo data to server...
python client.py localhost 5662 "my data"

